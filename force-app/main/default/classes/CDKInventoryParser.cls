public without sharing class CDKInventoryParser {
    private static final CDK_Integration__c SETTINGS;
    private static final String EXTRACT_INVENTORY_NAMESPACE;
    private static final String DEFAULT_NODE_CHILD = 'InventoryVehicle';

    static {
        SETTINGS = CDK_Integration__c.getOrgDefaults();
        EXTRACT_INVENTORY_NAMESPACE = CDKSoapConstants.NAMESPACE.INVENTORY;
    }

    public static List<Inventory__c> parse(Dom.Document doc, String dealerId) {
        Map<String, Schema.SObjectField> inventoryFields = Schema.SObjectType.Inventory__c.fields.getMap();

        List<Inventory__c> inventories = new List<Inventory__c>();
        for (Dom.XmlNode child: doc.getRootElement().getChildElements()) {
            if (child.getNodeType() == DOM.XmlNodeType.ELEMENT && child.getName() == DEFAULT_NODE_CHILD) {
                Inventory__c inv = new Inventory__c();
                CDKParser.setFields(inv, inventoryFields, INVENTORY_PARSE_MAP, child, EXTRACT_INVENTORY_NAMESPACE);
                inv.DealerId__c = dealerId;
                inv.UniqueId__c = dealerId + inv.Stock_Number__c;
                if (String.isNotBlank(inv.Make__c)) {
                    if (INVENTORY_MAKE_MAP.containsKey(inv.Make__c)) {
                        inv.Make__c = INVENTORY_MAKE_MAP.get(inv.Make__c);
                    } else {
                        inv.Make__c = 'Custom';
                        inv.Model_Custom__c = inv.Model__c;
                    }
                }
                inventories.add(inv);
            }
        }

        if (inventories.isEmpty()) {
            return null;
        }

        return inventories;
    }

    public static final Map<String, String> INVENTORY_PARSE_MAP = new Map<String, String> {
        'AxleNo'                  => 'AxleNo__c',
        'BaseInvoicePrice'        => 'BaseInvoicePrice__c',
        'BaseRetailPrice'         => 'BaseRetailPrice__c',
        'BodyStyle'               => 'BodyStyle__c',
        'Color'                   => 'Color__c',
        'DealerDefined1'          => 'DealerDefined1__c',
        'DealerDefined2'          => 'DealerDefined2__c',
        'DealerDefined3'          => 'DealerDefined3__c',
        'DealerDefined4'          => 'DealerDefined4__c',
        'DealerDefined5'          => 'DealerDefined5__c',
        'DealerDefined6'          => 'DealerDefined6__c', 
        'DealerDefined7'          => 'DealerDefined7__c',
        'DealerDefined8'          => 'DealerDefined8__c',
        'DealNo'                  => 'DealNo__c',
        'FuelType'                => 'Fuel_Type__c',
        'FullDescription'         => 'FullDescription__c',
        'GVW'                     => 'GVWR__c',
        'InteriorColor'           => 'InteriorColor__c',
        'LotLocation'             => 'LotLocation__c',
        'Make'                    => 'Make__c', 
        'MakeName'                => 'MakeName__c',
        'Manufacturer'            => 'Manufacturer__c',
        'Mileage'                 => 'Mileage__c',
        'Model'                   => 'Model__c',
        'ModelName'               => 'ModelName__c',
        'ModelNo'                 => 'ModelNo__c',
        'ModelType'               => 'ModelType__c',
        'OrderDate'               => 'OrderDate__c',
        'OrderType'               => 'OrderType__c',
        'Price1'                  => 'List_Price__c',
        'Price2'                  => 'Price2__c',
        'ReceivedDate'            => 'ReceivedDate__c',
        'SoldDate'                => 'SoldDate__c',
        'Status'                  => 'Status__c', 
        'StickerPrice'            => 'StickerPrice__c',
        'StockNo'                 => 'Stock_Number__c',
        'StockType'               => 'StockType__c',
        'VehicleStyle'            => 'VehicleStyle__c',
        'VIN'                     => 'VIN__c',
        'WheelBase'               => 'WheelBase__c',
        'Wholesale'               => 'Wholesale__c',
        'Year'                    => 'Year__c'
    }; 

    public static final Map<String, String> INVENTORY_MAKE_MAP = new Map<String, String> {
        'COA' =>  'Coachmen',
        'FR' => 'Forest River',
        'FLE' => 'Fleetwood',
        'EE' => 'Eclipse',
        'ALLI' => 'Alliance',
        'HG' => 'Heartland',
        'WIN' => 'Winnebago',
        'THO' => 'Thor Motor Coach',
        'GEN' => 'Genesis Supreme',
        'KEY' => 'Keystone',
        'GRE' => 'Grech',
        'PW' => 'Pleasure-Way',
        'ENT' => 'Entegra',
        'JAY' => 'Jayco',
        'NEW' => 'Newmar',
        'MID' => 'Midwest',
        'TIF' => 'Tiffin',
        'HOL' => 'Holiday Rambler',
        'MON' => 'Monaco',
        'NEXU' => 'Nexus'
    };
}