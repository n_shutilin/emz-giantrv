global without sharing class CDKWeOweSchedulable implements Schedulable {
    private static final ApexLogger LOGGER;
    private static final CDK_Integration__c SETTINGS;

    static {
        LOGGER = ApexLogger.create(CDKSoapConstants.PROCESS.WE_OWE_EXTRACT);
        SETTINGS = CDK_Integration__c.getOrgDefaults();
    }

    global void execute(SchedulableContext sc) {
        syncWeOwe();
        this.rescheduleJob(sc);
    }

    @future(callout=true)
    global static void syncWeOwe() {
        List<CDK_We_Owe__c> weOwes = new List<CDK_We_Owe__c>();
        String deltaDate = Date.today().format();

        for (String dealerId : CDKSoapConstants.getListOfDealers()) {
            try {
                HttpResponse response = CDKApi.doPost(
                    SETTINGS.PIP_We_Owe_Extract__c, 
                    new Map<String, String>{
                        CDKSoapConstants.PARAM.DEALER_ID => dealerId, 
                        CDKSoapConstants.PARAM.QUERY_ID => CDKSoapConstants.QUERY.WE_OWE_DELTA_EXTRACT, 
                        CDKSoapConstants.PARAM.DELTA_DATE => deltaDate
                    }
                );
        
                if (response.getStatusCode() != HTTPConstants.Status.OK) {
                    LOGGER.addLog(String.format(
                        CDKSoapConstants.LOGGER_MSG_TEMPLATE.RESPONDED_WITH_STATUS,
                        new List<String>{ response.getStatus(), deltaDate, dealerId }
                    ));
                    continue;
                }
                
                List<CDK_We_Owe__c> weOwesToUpsert = CDKWeOweParser.parse(response.getBodyDocument(), dealerId);
                if (weOwesToUpsert == null || weOwesToUpsert.isEmpty()) {
                    LOGGER.addLog(String.format(
                        CDKSoapConstants.LOGGER_MSG_TEMPLATE.LIST_UPSERTED_EMPTY,
                        new List<String>{ dealerId }
                    ));
                    continue;
                }
                weOwes.addAll(weOwesToUpsert);
            } catch (Exception e) {
                LOGGER.addLog(e);
            }
        }
        upsertWeOwes(weOwes);
        LOGGER.save();
    }

    public static void upsertWeOwes(List<CDK_We_Owe__c> weOwesToUpsert) {
        if (weOwesToUpsert == null || weOwesToUpsert.isEmpty()) return;

        Database.UpsertResult[] results = Database.upsert(weOwesToUpsert, CDK_We_Owe__c.fields.UniqueId__c, false);
        LOGGER.addLog(String.format(
            CDKSoapConstants.LOGGER_MSG_TEMPLATE.INSERTED_UPDATED, 
            CDKSoapConstants.countUpsertResults(results)
        ));
    }

    global void rescheduleJob(SchedulableContext sc) {
        Datetime nextScheduleTime = System.now().addMinutes(15);
        String hour = nextScheduleTime.hour() <= 21 ? String.valueOf(nextScheduleTime.hour()) : '6';
        String minutes = String.valueOf(nextScheduleTime.minute());
        String cronValue = '0 ' + minutes + ' ' + hour + ' * * ?' ;
        String jobName = CDKSoapConstants.PROCESS.WE_OWE_EXTRACT + ': ' + nextScheduleTime.format('hh:mm');

        CDKWeOweSchedulable nextJob = new CDKWeOweSchedulable();
        System.schedule(jobName, cronValue , nextJob);

        System.abortJob(SC.getTriggerId());
    }
}