@IsTest
public without sharing class CDKHelpEmployeeSalesSchedulableTest {
    @TestSetup
    public static void makeData(){
        CDKTestDataFactory.initSettings();
        CDKTestDataFactory.abortJobs();
    }

    @IsTest
    public static void testSchedulable() {
        Test.setMock(HttpCalloutMock.class, CDKTestDataFactory.getApiMock(CDKTestDataFactory.MOCK.HELP_EMPLOYEE_SALES_EXTRACT));
        Test.startTest();
        System.schedule(CDKTestDataFactory.DUMMY_VALUE_TEST, CDKTestDataFactory.TEST_CRON, new CDKHelpEmployeeSalesSchedulable());
        Test.stopTest();
    }
    
    @IsTest
    public static void testSchedulableServerError() {
        Test.setMock(HttpCalloutMock.class, CDKTestDataFactory.getApiMock(CDKTestDataFactory.MOCK.SERVER_ERROR));
        Test.startTest();
        System.schedule(CDKTestDataFactory.DUMMY_VALUE_TEST, CDKTestDataFactory.TEST_CRON, new CDKHelpEmployeeSalesSchedulable());
        Test.stopTest();
    }
}