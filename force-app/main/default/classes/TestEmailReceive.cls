@IsTest
private class TestEmailReceive {

    @IsTest
    static void testEmailService() {
        EmailReceive emailService = new EmailReceive();
        
        String currTime = String.valueOf(Datetime.now().getTime());

        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        envelope.fromAddress = 'test@test.test';

        Messaging.InboundEmail email = new Messaging.InboundEmail();
        email.subject = 'Test';
        email.plainTextBody =
        '<? adf version="1.0" ?>' +
        '<adf>' +
        '<prospect status="new">' +
        '<requestdate>2021-05-02T15:36-08:00</requestdate>' +
        '<vehicle interest="buy" status="new">' +
        '<year>2021</year>' +
        '<make>Winnebago </make>' +
        '<model>HIKE H171DB </model>' +
        '<stock>WHK003 </stock>' +
        '</vehicle>' +
        '<vehicle interest="trade-in">' +
        '<year>2021</year>' +
        '<make>Winnebago </make>' +
        '<model>HIKE H171DB </model>' +
        '<stock>WHK003 </stock>' +
        '</vehicle>' +
        '<customer>' +
        '<contact>' +
        '<name part="full" type="individual">Krissy Gomez</name>' +
        '<email preferredcontact="0">Kgome004@yahoo.com</email>' +
        '<phone preferredcontact="1" type="voice">619-349-3907</phone>' +
        '<phone preferredcontact="0" type="voice" />' +
        '<address type="home">' +
        '<postalcode />' +
        '</address>' +
        '</contact>' +
        '</customer>' +
        '<vendor>' +
        '<vendorname>Giant RV Downey</vendorname>' +
        '<url>www.facebook.com</url>' +
        '<contact>' +
        '<name part="full" type="business">Giant RV Downey</name>' +
        '<email preferredcontact="0" />' +
        '<phone preferredcontact="0" type="voice">562-348-1060</phone>' +
        '<address type="home">' +
        '<postalcode />' +
        '</address>' +
        '</contact>' +
        '</vendor>' +
        '<provider>' +
        '<name part="full" type="business">RVChat</name>' +
        '<service>Sales Facebook - Dealer Chat</service>' +
        '</provider>' +
        '</prospect>' +
        '</adf>';

        Messaging.InboundEmailResult result = emailService.handleInboundEmail(email, envelope);
        System.assert( result.success );
    }
}