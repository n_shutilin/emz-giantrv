global without sharing class CDKDealSchedulable implements Schedulable {
    private static final ApexLogger LOGGER;
    private static final CDK_Integration__c SETTINGS;

    static {
        LOGGER = ApexLogger.create(CDKSoapConstants.PROCESS.DEAL_EXTRACT);
        SETTINGS = CDK_Integration__c.getOrgDefaults();
    }

    global void execute(SchedulableContext sc) {
        syncDeals();
        this.rescheduleJob(sc);
    }

    @future(callout=true)
    global static void syncDeals() {
        List<CDK_Deal__c> deals = new List<CDK_Deal__c>();
        String deltaDate = Date.today().format();

        for (String dealerId : CDKSoapConstants.getListOfDealers()) {
            try {
                HttpResponse response = CDKApi.doPost(
                    SETTINGS.PIP_FISales_Closed_Extract__c, 
                    new Map<String, String>{
                        CDKSoapConstants.PARAM.DEALER_ID => dealerId, 
                        CDKSoapConstants.PARAM.QUERY_ID => CDKSoapConstants.QUERY.FI_SALES_CLOSED_DELTA_EXTRACT, 
                        CDKSoapConstants.PARAM.DELTA_DATE => deltaDate
                    }
                );
        
                if (response.getStatusCode() != HTTPConstants.Status.OK) {
                    LOGGER.addLog(String.format(
                        CDKSoapConstants.LOGGER_MSG_TEMPLATE.RESPONDED_WITH_STATUS,
                        new List<String>{ response.getStatus(), deltaDate, dealerId }
                    ));
                    continue;
                }
                
                List<CDK_Deal__c> dealsToUpsert = CDKDealParser.parse(response.getBodyDocument(), dealerId);
                if (dealsToUpsert == null || dealsToUpsert.isEmpty()) {
                    LOGGER.addLog(String.format(
                        CDKSoapConstants.LOGGER_MSG_TEMPLATE.LIST_UPSERTED_EMPTY,
                        new List<String>{ dealerId }
                    ));
                    continue;
                }
                deals.addAll(dealsToUpsert);
            } catch (Exception e) {
                LOGGER.addLog(e);
            }
        }
        upsertDeals(deals);
        LOGGER.save();
    }

    public static void upsertDeals(List<CDK_Deal__c> dealsToUpsert) {
        if (dealsToUpsert == null || dealsToUpsert.isEmpty()) return;

        Database.UpsertResult[] results = Database.upsert(dealsToUpsert, CDK_Deal__c.fields.UniqueId__c, false);
        LOGGER.addLog(String.format(
            CDKSoapConstants.LOGGER_MSG_TEMPLATE.INSERTED_UPDATED, 
            CDKSoapConstants.countUpsertResults(results)
        ));
    }

    global void rescheduleJob(SchedulableContext sc) {
        Datetime nextScheduleTime = System.now().addMinutes(15);
        String hour = nextScheduleTime.hour() <= 21 ? String.valueOf(nextScheduleTime.hour()) : '6';
        String minutes = String.valueOf(nextScheduleTime.minute());
        String cronValue = '0 ' + minutes + ' ' + hour + ' * * ?' ;
        String jobName = CDKSoapConstants.PROCESS.DEAL_EXTRACT + ': ' + nextScheduleTime.format('hh:mm');

        CDKDealSchedulable nextJob = new CDKDealSchedulable();
        System.schedule(jobName, cronValue, nextJob);

        System.abortJob(sc.getTriggerId());
    }
}
//system.schedule('CDK Dela', '0 24  * * * ?', new CDKDealSchedulable());