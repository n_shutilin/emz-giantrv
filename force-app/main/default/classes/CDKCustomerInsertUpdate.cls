public without sharing class CDKCustomerInsertUpdate {
    private ApexLogger LOGGER;
    private static final CDK_Integration__c SETTINGS;

    static { SETTINGS = CDK_Integration__c.getOrgDefaults(); }

    public CDKCustomerInsertUpdate(ApexLogger loggerInstance) {
        this.LOGGER = loggerInstance;
    }
    
    public String getCustomerNumber(String dealerId) {
        Dom.Document doc = new Dom.Document();
        Dom.XmlNode envelope = doc.createRootElement(CDKSoapConstants.TAG.ENVELOPE, CDKSoapConstants.SOAP_NS, 'soapenv');
        envelope.setNamespace('save', CDKSoapConstants.XMLNS_SAVE);
        envelope.setNamespace('pip', CDKSoapConstants.PIP.CUSTOMER);

        Dom.XmlNode body = envelope.addChildElement(CDKSoapConstants.TAG.BODY, CDKSoapConstants.SOAP_NS, 's');
        Dom.XmlNode getNumberTag = body.addChildElement(CDKSoapConstants.TAG.CUSTOMER_GET_NUMBER, CDKSoapConstants.PIP.CUSTOMER, 'save');

        Dom.XmlNode arg0 = getNumberTag.addChildElement(CDKSoapConstants.TAG.ARGUMENT_0, null, null);
        arg0.addChildElement(CDKSoapConstants.TAG.PASSWORD, null, null).addTextNode(SETTINGS.Password__c);
        arg0.addChildElement(CDKSoapConstants.TAG.USERNAME, null, null).addTextNode(SETTINGS.UserID__c);

        Dom.XmlNode arg1 = getNumberTag.addChildElement(CDKSoapConstants.TAG.ARGUMENT_1, null, null);
        arg1.addChildElement(CDKSoapConstants.TAG.DEALER_ID, null, null).addTextNode(dealerId);

        Dom.XmlNode arg2 = getNumberTag.addChildElement(CDKSoapConstants.TAG.ARGUMENT_2, null, null);
        arg2.addChildElement(CDKSoapConstants.TAG.USER_ID, null, null).addTextNode('111');
        System.debug(doc.toXmlString());
        HttpResponse response = CDKApi.doPost(SETTINGS.PIP_Customer_Insert_Update__c, doc);

        Dom.Document docResult = response.getBodyDocument();
        Dom.XmlNode envelopeResult = docResult.getRootElement();
        Dom.XmlNode bodyResult = envelopeResult.getChildElement(CDKSoapConstants.TAG.BODY, CDKSoapConstants.SOAP_NS);
        Dom.XmlNode getCustomerNumberResponse = bodyResult.getChildElement(CDKSoapConstants.TAG.CUSTOMER_GET_NUMBER_REPSONSE, CDKSoapConstants.PIP.CUSTOMER);
        Dom.XmlNode returnResult = getCustomerNumberResponse.getChildElement('return', null);
        Dom.XmlNode code = returnResult.getChildElement('code', null);

        if (code != null && code.getText() == 'success') {
            Dom.XmlNode customerNumber = returnResult.getChildElement('customerNumber', null);
            return customerNumber.getText();
        }

        return null;
    }

    public CustomerInfo insertUpdateCustomer(String dealerId, String accountId) {
        Account customer = [
            SELECT Id, Phone, PersonMobilePhone, PersonHomePhone,
                PersonOtherPhone, FirstName, LastName, MiddleName,
                PersonMailingStreet, PersonMailingCity,
                PersonMailingCountry, PersonMailingPostalCode,
                PersonMailingState, PersonEmail, 
                PersonBirthdate
            FROM Account
            WHERE Id = :accountId
            LIMIT 1
        ];

        String newCustomerNumber = getCustomerNumber(dealerId);

        Dom.Document doc = new Dom.Document();
        Dom.XmlNode envelope = doc.createRootElement(CDKSoapConstants.TAG.ENVELOPE, CDKSoapConstants.SOAP_NS, 's');
        envelope.setNamespace('pip', CDKSoapConstants.PIP.CUSTOMER);

        Dom.XmlNode body = envelope.addChildElement(CDKSoapConstants.TAG.BODY, CDKSoapConstants.SOAP_NS, 's');
        Dom.XmlNode insertTag = body.addChildElement('insert', CDKSoapConstants.PIP.CUSTOMER, 'ns2');

        Dom.XmlNode arg1 = insertTag.addChildElement(CDKSoapConstants.TAG.ARGUMENT_1, null, null);
        arg1.addChildElement(CDKSoapConstants.TAG.DEALER_ID, null, null).addTextNode(dealerId);

        Dom.XmlNode arg0 = insertTag.addChildElement(CDKSoapConstants.TAG.ARGUMENT_0, null, null);
        arg0.addChildElement(CDKSoapConstants.TAG.PASSWORD, null, null).addTextNode(SETTINGS.Password__c);
        arg0.addChildElement(CDKSoapConstants.TAG.USERNAME, null, null).addTextNode(SETTINGS.UserID__c);

        Dom.XmlNode arg2 = insertTag.addChildElement(CDKSoapConstants.TAG.ARGUMENT_2, null, null);
        arg2.addChildElement(CDKSoapConstants.TAG.USER_ID, null, null).addTextNode('111');

        Dom.XmlNode arg3 = insertTag.addChildElement(CDKSoapConstants.TAG.ARGUMENT_3, null, null);

        Dom.XmlNode idTag = arg3.addChildElement(CDKSoapConstants.TAG.ID, null, null);
        idTag.addChildElement(CDKSoapConstants.TAG.VALUE, null, null).addTextNode(newCustomerNumber);

        Dom.XmlNode address = arg3.addChildElement('address', null, null);
        address.addChildElement('addressLine', null, null).addTextNode(customer.PersonMailingStreet == null ? '' : customer.PersonMailingStreet);
        address.addChildElement('city', null, null).addTextNode(customer.PersonMailingCity == null ? '' : customer.PersonMailingCity);
        address.addChildElement('country', null, null).addTextNode(customer.PersonMailingCountry == null ? '' : customer.PersonMailingCountry);
        address.addChildElement('county', null, null).addTextNode('');//---val
        address.addChildElement('postalCode', null, null).addTextNode(customer.PersonMailingPostalCode == null ? '' : customer.PersonMailingPostalCode);
        address.addChildElement('stateOrProvince', null, null).addTextNode(customer.PersonMailingState == null ? '' : customer.PersonMailingState);

        Dom.XmlNode arStatus = arg3.addChildElement('arStatus', null, null);
        arStatus.addChildElement('dealerField1', null, null).addTextNode(dealerId);

        Dom.XmlNode contactInfo = arg3.addChildElement('contactInfo', null, null);
        //is it required?
        // Dom.XmlNode contactTime = contactInfo.addChildElement('contactTime', null, null);
        // contactTime.addChildElement('description', null, null).addTextNode('');
        // contactTime.addChildElement('time', null, null).addTextNode('');

        if (String.isNotBlank(customer.PersonEmail)) {
            Dom.XmlNode emailTag1 = contactInfo.addChildElement('email', null, null);
            emailTag1.addChildElement('desc', null, null).addTextNode('Cellular');
            emailTag1.addChildElement('value', null, null).addTextNode(customer.PersonEmail);
        }

        // if (String.isNotBlank('---val')) {
        //     Dom.XmlNode emailTag2 = contactInfo.addChildElement('email', null, null);
        //     emailTag2.addChildElement('desc', null, null).addTextNode('Home');
        //     emailTag2.addChildElement('value', null, null).addTextNode('---val');
        // }

        // if (String.isNotBlank('---val')) {
        //     Dom.XmlNode emailTag3 = contactInfo.addChildElement('email', null, null);
        //     emailTag3.addChildElement('desc', null, null).addTextNode('Work');
        //     emailTag3.addChildElement('value', null, null).addTextNode('---val');
        // }
        //is it required?
        // contactInfo.addChildElement('preferredContactDay', null, null).addTextNode('');
        // contactInfo.addChildElement('preferredContactMethod', null, null).addTextNode('');
        // contactInfo.addChildElement('preferredLanguage', null, null).addTextNode('');

        Dom.XmlNode telephoneNumber1 = contactInfo.addChildElement('telephoneNumber', null, null);
        if (String.isNotBlank(customer.PersonMobilePhone)) {
            customer.PersonMobilePhone = customer.PersonMobilePhone.replaceAll('[^0-9]', '');
        }
        telephoneNumber1.addChildElement('desc', null, null).addTextNode(customer.PersonMobilePhone == null ? '' : 'Cellular');
        telephoneNumber1.addChildElement('exten', null, null).addTextNode('');
        telephoneNumber1.addChildElement('value', null, null).addTextNode(customer.PersonMobilePhone == null ? '' : customer.PersonMobilePhone);

        Dom.XmlNode telephoneNumber2 = contactInfo.addChildElement('telephoneNumber', null, null);
        if (String.isNotBlank(customer.PersonHomePhone)) {
            customer.PersonHomePhone = customer.PersonHomePhone.replaceAll('[^0-9]', '');
        }
        telephoneNumber2.addChildElement('desc', null, null).addTextNode(customer.PersonHomePhone == null ? '' : 'Home');
        telephoneNumber1.addChildElement('exten', null, null).addTextNode('');
        telephoneNumber2.addChildElement('value', null, null).addTextNode(customer.PersonHomePhone == null ? '' : customer.PersonHomePhone);

        Dom.XmlNode telephoneNumber3 = contactInfo.addChildElement('telephoneNumber', null, null);
        if(String.isNotBlank(customer.PersonOtherPhone)) {
            customer.PersonOtherPhone = customer.PersonOtherPhone.replaceAll('[^0-9]', '');
        }
        telephoneNumber3.addChildElement('desc', null, null).addTextNode(customer.PersonOtherPhone == null ? '' : 'Work');
        telephoneNumber1.addChildElement('exten', null, null).addTextNode('');
        telephoneNumber3.addChildElement('value', null, null).addTextNode(customer.PersonOtherPhone == null ? '' : customer.PersonOtherPhone);
        //is it required?
        // Dom.XmlNode telephoneNumber4 = contactInfo.addChildElement('telephoneNumber', null, null);
        // telephoneNumber4.addChildElement('desc', null, null).addTextNode('');
        // telephoneNumber1.addChildElement('exten', null, null).addTextNode('');
        // telephoneNumber4.addChildElement('main', null, null).addTextNode('');
        // telephoneNumber4.addChildElement('value', null, null).addTextNode('');

        // if (opp.Customer_Primary_Phone__c == 'Mobile') {
        //     telephoneNumber1.addChildElement('main', null, null).addTextNode('Y');
        //     telephoneNumber2.addChildElement('main', null, null).addTextNode('');
        //     telephoneNumber3.addChildElement('main', null, null).addTextNode('');
        // } else if (opp.Customer_Primary_Phone__c == 'Home') {
        //     telephoneNumber1.addChildElement('main', null, null).addTextNode('');
        //     telephoneNumber2.addChildElement('main', null, null).addTextNode('Y');
        //     telephoneNumber3.addChildElement('main', null, null).addTextNode('');
        // } else if (opp.Customer_Primary_Phone__c == 'Work') {
        //     telephoneNumber1.addChildElement('main', null, null).addTextNode('');
        //     telephoneNumber2.addChildElement('main', null, null).addTextNode('');
        //     telephoneNumber3.addChildElement('main', null, null).addTextNode('Y');
        // } else {
        //     telephoneNumber1.addChildElement('main', null, null).addTextNode('');
        //     telephoneNumber2.addChildElement('main', null, null).addTextNode('');
        //     telephoneNumber3.addChildElement('main', null, null).addTextNode('');
        // }

        Dom.XmlNode demographics = arg3.addChildElement('demographics', null, null);
        demographics.addChildElement('birthDate', null, null).addTextNode(customer.PersonBirthdate == null ? '' : customer.PersonBirthdate.format());

        // arg3.addChildElement('employerName', null, null).addTextNode(opp.Customer_Employer__c == null ? '' : opp.Customer_Employer__c);
        // arg3.addChildElement('dealerLoyaltyIndicator', null, null).addTextNode('');
        // arg3.addChildElement('delCdeServiceNames', null, null).addTextNode('');
        // arg3.addChildElement('deleteCode', null, null).addTextNode('');

        Dom.XmlNode name1 = arg3.addChildElement('name1', null, null);

        // if (opp.Customer_Type__c == 'Business') {
        //     name1.addChildElement('companyName', null, null).addTextNode(opp.Company_Name__c == null ? '' : opp.Company_Name__c);
        //     name1.addChildElement('nameType', null, null).addTextNode(opp.Customer_Type__c == null ? '' : opp.Customer_Type__c);
        // } else {
        //     name1.addChildElement('firstName', null, null).addTextNode(opp.Customer_First_Name__c == null ? '' : opp.Customer_First_Name__c);
        //     name1.addChildElement('lastName', null, null).addTextNode(opp.Customer_Last_Name__c == null ? '' : opp.Customer_Last_Name__c);
        //     name1.addChildElement('middleName', null, null).addTextNode(opp.Customer_Middle_Name__c == null ? '' : opp.Customer_Middle_Name__c);
        //     name1.addChildElement('fullName', null, null).addTextNode(opp.Customer_Full_Name__c == null ? '' : opp.Customer_Full_Name__c);
        //     name1.addChildElement('nameType', null, null).addTextNode('Person');
        //     name1.addChildElement('suffix', null, null).addTextNode('');
        //     name1.addChildElement('title', null, null).addTextNode('');
        // }

        name1.addChildElement('firstName', null, null).addTextNode(customer.FirstName == null ? '' : customer.FirstName);
        name1.addChildElement('lastName', null, null).addTextNode(customer.LastName == null ? '' : customer.LastName);
        name1.addChildElement('middleName', null, null).addTextNode(customer.MiddleName == null ? '' : customer.MiddleName);
        // name1.addChildElement('fullName', null, null).addTextNode(opp.Customer_Full_Name__c == null ? '' : opp.Customer_Full_Name__c);
        name1.addChildElement('nameType', null, null).addTextNode('Person');
        name1.addChildElement('suffix', null, null).addTextNode('');
        name1.addChildElement('title', null, null).addTextNode('');
        System.debug(doc.toXmlString());
        HttpResponse response = CDKApi.doPost(SETTINGS.PIP_Customer_Insert_Update__c, doc);

        LOGGER.addLog(String.format(
            CDKSoapConstants.LOGGER_MSG_TEMPLATE.RESPONDED_WITH_STATUS_ONLY, 
            new List<String>{ response.getStatus() }
        ), new List<Attachment> {
            new Attachment(
                Name = 'Request',
                ContentType = HTTPConstants.MediaType.TEXT_PLAIN_VALUE,
                Body = Blob.valueOf(doc.toXmlString()) 
            ),
            new Attachment(
                Name = 'Response',
                ContentType = HTTPConstants.MediaType.TEXT_PLAIN_VALUE,
                Body = Blob.valueOf(response.getBody()) 
            )
        });

        return parseInsertResponse(response.getBodyDocument());
    }

    public CustomerInfo parseInsertResponse(DOM.Document doc) {
        Dom.XmlNode envelopeResult = doc.getRootElement();
        Dom.XmlNode bodyResult = envelopeResult.getChildElement(CDKSoapConstants.TAG.BODY, CDKSoapConstants.SOAP_NS);
        Dom.XmlNode getCustomerNumberResponse = bodyResult.getChildElement('insertResponse', CDKSoapConstants.PIP.CUSTOMER);
        Dom.XmlNode returnResult = getCustomerNumberResponse.getChildElement('return', null);
        Dom.XmlNode code = returnResult.getChildElement('code', null);
        CustomerInfo custInfo = new CustomerInfo();
        
        if (code != null && code.getText() == 'success') {
            Dom.XmlNode customerParty = returnResult.getChildElement('customerParty', null);
            Dom.XmlNode checksum = customerParty.getChildElement('checksum', null);
            Dom.XmlNode custId = customerParty.getChildElement('id', null);
            Dom.XmlNode custIdValue = custId.getChildElement('value', null);
            custInfo.checksum = checksum.getText();
            custInfo.custId = custIdValue.getText();
            return custInfo;
        } else {
            Dom.XmlNode message = returnResult.getChildElement('message', null);
            custInfo.error = CDKSoapConstants.PROCESS.CUSTOMER_INSERT + ' error: ' + message.getText();
        }
        return custInfo;
    }

    public class CustomerInfo {
        public String custId;
        public String checksum;
        public String error;
        public ApexLogger logger;
    }
}