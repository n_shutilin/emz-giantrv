@IsTest
public class GrvNewOpportunityTest {
    private static final String TEST_VALUE = 'TestValue';
    private static final GrvNewOpportunity.DataWrapper TEST_WRAPPER;
    
    static {
        TEST_WRAPPER = new GrvNewOpportunity.DataWrapper();
        TEST_WRAPPER.firstName = 'Test';
        TEST_WRAPPER.lastName = 'Test';
        TEST_WRAPPER.email = 'test@test.test';
        TEST_WRAPPER.phone = '111111111';
        TEST_WRAPPER.condition = 'New';
        TEST_WRAPPER.rvFuelType = 'Gas';
        TEST_WRAPPER.rvPriceMin = 1000;
        TEST_WRAPPER.rvType = 'A';
        TEST_WRAPPER.rvMake = 'Alliance';
        TEST_WRAPPER.rvModel = 'Adrenaline';
        TEST_WRAPPER.rvFloorplan = 'BH - Bunkhouse';
    }

    @TestSetup
    static void makeData(){
        List<Inventory__c> inventories = new List<Inventory__c>();
        List<Opportunity> opportunities = new List<Opportunity>();
        List<Account> accounts = new List<Account>();
        List<Matching_Vehicles__c> matches = new List<Matching_Vehicles__c>();

        inventories.add(new Inventory__c(
            Stock_Number__c = TEST_VALUE
        ));
        
        inventories.add(new Inventory__c(
            Condition__c = TEST_WRAPPER.condition,
            Fuel_Type__c = TEST_WRAPPER.rvFuelType,
            List_Price__c = TEST_WRAPPER.rvPriceMin,
            Class__c = TEST_WRAPPER.rvType,
            Make__c =  TEST_WRAPPER.rvMake,
            Model__c = TEST_WRAPPER.rvModel,
            Floorplan__c = TEST_WRAPPER.rvFloorplan
        ));

        accounts.add(new Account(
            FirstName = 'Test',
            LastName = 'Test',
            PersonEmail = 'test@test.test',
            PersonMailingStreet = 'test',
            PersonMailingCity = 'test',
            PersonMailingCountry = 'test',
            PersonMailingState = 'test',
            PersonMailingPostalCode = '1111'
        ));
        insert accounts;

        opportunities.add(new Opportunity(
            Name = 'Test',
            StageName = 'Qualification',
            CloseDate = Date.today(),
            AccountId = accounts.get(0).Id
        ));

        insert inventories;
        insert opportunities;  
        
        matches.add(new Matching_Vehicles__c(
            Inventory__c = inventories.get(0).Id,
            Opportunity__c = opportunities.get(0).Id
        ));

        insert matches;
    }

    @IsTest
    static void testSearchInventoryByStockNumber() {
        Test.startTest();
        List<Inventory__c> inventories = GrvNewOpportunity.searchInventoryByStockNumber(TEST_VALUE);
        Test.stopTest();

        System.assert(inventories.size() > 0);
    }

    @IsTest
    static void testSearchInventoryByParams() {
        Test.startTest();
        List<Inventory__c> inventories = GrvNewOpportunity.searchInventoryByParams(JSON.serialize(TEST_WRAPPER));
        Test.stopTest();

        System.assert(inventories.size() > 0);
    }

    @IsTest
    static void testCreateRecords() {
        Test.startTest();
        String opportunityId = GrvNewOpportunity.createRecords(JSON.serialize(TEST_WRAPPER));
        Test.stopTest();

        System.assert(String.isNotBlank(opportunityId));
    }

    @IsTest
    static void testUpdateRecords() {
        Id opportunityId = [SELECT Id FROM Opportunity LIMIT 1].Id; 
        Test.startTest();
        String updatedOpportunityId = GrvNewOpportunity.updateRecords(JSON.serialize(TEST_WRAPPER), opportunityId);
        Test.stopTest();

        Opportunity updatedOpportunity = [SELECT Id, Condition__c FROM Opportunity LIMIT 1];
        System.assertEquals(TEST_WRAPPER.condition, updatedOpportunity.Condition__c);
    }

    @IsTest
    static void testCreateOpportunityInventories() {
        Id opportunityId = [SELECT Id FROM Opportunity LIMIT 1].Id; 
        String inventoryId = [SELECT Id FROM Inventory__c LIMIT 1].Id;
        String existingMatchId = [SELECT Id FROM Matching_Vehicles__c LIMIT 1].Id;
        Test.startTest();
        GrvNewOpportunity.createOpportunityInventories(opportunityId, new List<String> {inventoryId});
        Test.stopTest();

        Matching_Vehicles__c match = [SELECT Id, Opportunity__c, Inventory__c FROM Matching_Vehicles__c LIMIT 1];
        System.assertEquals(opportunityId, match.Opportunity__c);
        System.assertEquals(inventoryId, match.Inventory__c);
        System.assertNotEquals(existingMatchId, match.Id);
    }
    
    @IsTest
    static void testGetInfoByOpportunityId() {
        Id opportunityId = [SELECT Id FROM Opportunity LIMIT 1].Id; 
        Test.startTest();
        GrvNewOpportunity.DataWrapper data = GrvNewOpportunity.getInfoByOpportunityId(opportunityId);
        Test.stopTest();

        Account acc = [SELECT Id, FirstName FROM Account LIMIT 1];
        System.assertEquals(acc.FirstName, data.firstName);
    }
}