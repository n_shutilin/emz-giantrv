@IsTest
public without sharing class CDKDealInsertComponentTest {
    @TestSetup
    public static void makeData(){
        CDKTestDataFactory.initSettings();
    }

    @IsTest
    public static void testRetrieveDeal() {
        String accountId = CDKTestDataFactory.createPersonAccount();
        String opportunityId = CDKTestDataFactory.createOpportunity(accountId);
        Test.startTest();
        Opportunity opp = CDKDealInsertComponent.retrieveDeal(opportunityId);
        Test.stopTest();
        System.assert(opp != null);
    }

    @IsTest
    public static void testSendDealNotOpportunityAccount() {
        String accountId = CDKTestDataFactory.createPersonAccount();
        String opportunityId = CDKTestDataFactory.createOpportunity(null);
        Test.startTest();
        CDKDealInsertComponent.DealInsertResponseWrapper dealInsertResponse = CDKDealInsertComponent.sendDeal(opportunityId);
        Test.stopTest();
        System.assertEquals(false, dealInsertResponse.success);
        System.assertEquals(System.Label.No_Opportunity_Account, dealInsertResponse.message);
    }

    @IsTest
    public static void testSendDealEmptyDealerId() {
        String accountId = CDKTestDataFactory.createPersonAccount();
        String opportunityId = CDKTestDataFactory.createOpportunity(accountId);
        Test.startTest();
        CDKDealInsertComponent.DealInsertResponseWrapper dealInsertResponse = CDKDealInsertComponent.sendDeal(opportunityId);
        Test.stopTest();
        System.assertEquals(false, dealInsertResponse.success);
        System.assertEquals(System.Label.Dealer_Id_Not_Defined, dealInsertResponse.message);
    }
    
    @IsTest
    public static void testSendDealEmptyMatchingVehicle() {
        String accountId = CDKTestDataFactory.createPersonAccount();
        String opportunityId = CDKTestDataFactory.createOpportunity(accountId);
        Opportunity opp = [SELECT Id FROM Opportunity WHERE Id = :opportunityId];
        opp.Dealer__c = CDKSoapConstants.DEALER_COLTON;
        update opp;
        Test.startTest();
        CDKDealInsertComponent.DealInsertResponseWrapper dealInsertResponse = CDKDealInsertComponent.sendDeal(opportunityId);
        Test.stopTest();
        System.assertEquals(false, dealInsertResponse.success);
        System.assertEquals(System.Label.Vehicle_For_Sending_Not_Found, dealInsertResponse.message);
    }

    @IsTest
    public static void testSendDealEmptyCustomerNumber() {
        String accountId = CDKTestDataFactory.createPersonAccount();
        String opportunityId = CDKTestDataFactory.createOpportunity(accountId);
        Opportunity opp = [SELECT Id FROM Opportunity WHERE Id = :opportunityId];
        opp.Dealer__c = CDKSoapConstants.DEALER_COLTON;
        update opp;
        String inventoryId = CDKTestDataFactory.createInventory();
        CDKTestDataFactory.createMatchingVehicle(inventoryId, opportunityId);
        Test.setMock(HttpCalloutMock.class, CDKTestDataFactory.getApiMock(CDKTestDataFactory.MOCK.DEAL_INSERT_COMPONENT));
        Test.startTest();
        CDKDealInsertComponent.DealInsertResponseWrapper dealInsertResponse = CDKDealInsertComponent.sendDeal(opportunityId);
        Test.stopTest();
        System.assertEquals(true, dealInsertResponse.success);
        System.assertEquals(System.Label.Deal_Successfully_Created, dealInsertResponse.message);
    }

    @IsTest
    public static void testSendDealCustomerNotFound() {
        String accountId = CDKTestDataFactory.createPersonAccount();
        Account acc = [SELECT Id FROM Account WHERE Id = :accountId];
        acc.CDK_Customer_Colton__c = CDKTestDataFactory.DUMMY_VALUE_TEST;
        String opportunityId = CDKTestDataFactory.createOpportunity(accountId);
        Opportunity opp = [SELECT Id FROM Opportunity WHERE Id = :opportunityId];
        opp.Dealer__c = CDKSoapConstants.DEALER_COLTON;
        update opp;
        String inventoryId = CDKTestDataFactory.createInventory();
        CDKTestDataFactory.createMatchingVehicle(inventoryId, opportunityId);
        Test.setMock(HttpCalloutMock.class, CDKTestDataFactory.getApiMock(CDKTestDataFactory.MOCK.DEAL_INSERT_COMPONENT_NO_CUSTOMER));
        Test.startTest();
        CDKDealInsertComponent.DealInsertResponseWrapper dealInsertResponse = CDKDealInsertComponent.sendDeal(opportunityId);
        Test.stopTest();
        System.assertEquals(true, dealInsertResponse.success);
        System.assertEquals(System.Label.Deal_Successfully_Created, dealInsertResponse.message);
    }
}