global without sharing class CDKInventorySchedulable implements Schedulable {
    private static final ApexLogger LOGGER;
    private static final CDK_Integration__c SETTINGS;

    static {
        LOGGER = ApexLogger.create(CDKSoapConstants.PROCESS.INVENTORY_EXTRACT);
        SETTINGS = CDK_Integration__c.getOrgDefaults();
    }

    global void execute(SchedulableContext sc) {
        syncInventory();
        this.rescheduleJob(sc);
    }

    @future(callout=true)
    global static void syncInventory() {
        List<Inventory__c> inventories = new List<Inventory__c>();
        String deltaDate = '11/13/2021';

        for (String dealerId : CDKSoapConstants.getListOfDealers()) {
            try {
                HttpResponse response = CDKApi.doPost(
                    SETTINGS.PIP_Vehicle_Extract__c, 
                    new Map<String, String>{
                        CDKSoapConstants.PARAM.DEALER_ID => dealerId, 
                        CDKSoapConstants.PARAM.INV_COMPANY => CDKSoapConstants.getInvCompanyByDealerId(dealerId),
                        CDKSoapConstants.PARAM.QUERY_ID => CDKSoapConstants.QUERY.INVENTORY_DELTA_EXTRACT, 
                        CDKSoapConstants.PARAM.DELTA_DATE => deltaDate
                    }
                );
        
                if (response.getStatusCode() != HTTPConstants.Status.OK) {
                    LOGGER.addLog(String.format(
                        CDKSoapConstants.LOGGER_MSG_TEMPLATE.RESPONDED_WITH_STATUS,
                        new List<String>{ response.getStatus(), deltaDate, dealerId }
                    ));
                    continue;
                }
                
                List<Inventory__c> inventoriesToUpsert = CDKInventoryParser.parse(response.getBodyDocument(), dealerId);
                if (inventoriesToUpsert == null || inventoriesToUpsert.isEmpty()) {
                    LOGGER.addLog(String.format(
                        CDKSoapConstants.LOGGER_MSG_TEMPLATE.LIST_UPSERTED_EMPTY,
                        new List<String>{ dealerId }
                    ));
                    continue;
                }
                inventories.addAll(inventoriesToUpsert);
            } catch (Exception e) {
                LOGGER.addLog(e);
            }
        }
        upsertInventories(inventories);
        LOGGER.save();
    }

    public static void upsertInventories(List<Inventory__c> inventoriesToUpsert) {
        if (inventoriesToUpsert == null || inventoriesToUpsert.isEmpty()) return;

        Database.UpsertResult[] results = Database.upsert(inventoriesToUpsert, Inventory__c.fields.UniqueId__c, false);
        LOGGER.addLog(String.format(
            CDKSoapConstants.LOGGER_MSG_TEMPLATE.INSERTED_UPDATED, 
            CDKSoapConstants.countUpsertResults(results)
        ));
    }

    global void rescheduleJob(SchedulableContext sc) {
        Datetime nextScheduleTime = System.now().addMinutes(15);
        String hour = nextScheduleTime.hour() <= 21 ? String.valueOf(nextScheduleTime.hour()) : '6';
        String minutes = String.valueOf(nextScheduleTime.minute());
        String cronValue = '0 ' + minutes + ' ' + hour + ' * * ?' ;
        String jobName = CDKSoapConstants.PROCESS.INVENTORY_EXTRACT + ': ' + nextScheduleTime.format('hh:mm');

        CDKInventorySchedulable nextJob = new CDKInventorySchedulable();
        System.schedule(jobName, cronValue , nextJob);

        System.abortJob(SC.getTriggerId());
    }
}

//system.schedule('CDK Inv', '0 14  * * * ?', new CDKInventorySchedulable());