public without sharing class HTTPConstants {
    public static final MethodType Method;
    public static final MimeType MediaType;
    public static final StatusCode Status;
    public static final String CONTENT_TYPE;
    public static final String AUTHORIZATION;
    public static final Integer TIMEOUT_120000;
    
    static {
        MediaType = new MimeType();
        Method = new MethodType();
        Status = new StatusCode();
        CONTENT_TYPE = 'Content-Type';
        AUTHORIZATION = 'Authorization';
        TIMEOUT_120000 = 120000;
    }

    public class MethodType {
        public final String GET = 'GET';
        public final String POST = 'POST';
        public final String PUT = 'PUT';
    }

    public class MimeType {
        public final String APPLICATION_FORM_URLENCODED_VALUE = 'application/x-www-form-urlencoded';
        public final String APPLICATION_JSON_VALUE = 'application/json';
        public final String APPLICATION_XML_VALUE = 'application/xml';
        public final String TEXT_PLAIN_VALUE = 'text/plain';
        public final String TEXT_XML_VALUE = 'text/xml';
    }

    public class StatusCode {
        public final Integer OK = 200;
        public final Integer INTERNAL_SERVER_ERROR = 500;

    }
}