global class EmailReceive implements Messaging.InboundEmailHandler {
    public static final String DEALER_RECORD_TYPE = 'Dealer';

    public static final String KEY_COL = 'Giant RV Colton';
    public static final String KEY_COL2 = 'Giant RV - Colton';
    public static final String KEY_MON = 'Giant RV Montclair';
    public static final String KEY_MON2 = 'Giant RV - Montclair';
    public static final String KEY_MUR = 'Giant RV Murrieta';
    public static final String KEY_MUR2 = 'Giant RV - Murrieta';
    public static final String KEY_DOW = 'Giant RV Downey';
    public static final String KEY_DOW2 = 'Giant RV - Downey';
    public static final String KEY_STR = 'Storage';

    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail inboundEmail, Messaging.InboundEnvelope envelope) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
        String myPlainText = '';
        myPlainText = inboundEmail.plainTextBody;
        
        try {
            String leadInfo;

            if (String.isNotBlank(inboundEmail.htmlBody)) {
                leadInfo = inboundEmail.htmlBody.stripHtmlTags().trim().replaceAll('\u00A0', '');
            } else {
                leadInfo = inboundEmail.plainTextBody.trim().replaceAll('\u00A0', '');
            }

            leadInfo = leadInfo
                    .replaceAll('<\\? .*\\?>', '')
                    .replaceAll('<!\\[CDATA\\[', '')
                    .replaceAll('\\]\\]>', '')
                    .replaceAll('<br>',' ')
                    .replaceAll('&','&amp;');

            System.debug(leadInfo);

            Lead lead = new Lead();

            Dom.Document doc = new Dom.Document();
            doc.load(leadInfo);
            Dom.XmlNode adf = doc.getRootElement();

            Dom.XmlNode prospect = adf.getChildElement('prospect', null);

            if (prospect != null) {
                String status = prospect.getAttribute('status', null);
                Dom.XmlNode requestDate = prospect.getChildElement('requestdate', null);
                lead.Request_Date__c = (requestDate != null ? convertStringToDateTime(requestDate.getText()) : null);

                Dom.XmlNode[] prospectChilds = prospect.getChildren();
                if (prospectChilds != null) {
                    for (Dom.XmlNode vehicle : prospectChilds) {
                        if (vehicle.getName() == 'vehicle') {
                            String vehicleInterest = vehicle.getAttribute('interest', null);
                            if (vehicleInterest == 'trade-in') {
                                Dom.XmlNode year = vehicle.getChildElement('year', null);
                                lead.Trade_In_RV_Year__c = (year != null ? year.getText() : '');

                                Dom.XmlNode make = vehicle.getChildElement('make', null);
                                lead.Trade_In_RV_Manufacturer__c = (make != null ? make.getText() : '');

                                Dom.XmlNode model = vehicle.getChildElement('model', null);
                                lead.Trade_In_RV_Brand__c = (model != null ? model.getText() : '');
                                
                                Dom.XmlNode trim = vehicle.getChildElement('trim', null);
                                lead.Trade_In_RV_Model__c = (trim != null ? trim.getText() : '');
                                
                                Dom.XmlNode odometer = vehicle.getChildElement('odometer', null);
                                lead.Trade_In_RV_Mileage__c = (odometer != null ? odometer.getText() : '');
                               
                                Dom.XmlNode condition = vehicle.getChildElement('condition', null);
                                lead.Trade_In_RV_Condition__c = (condition != null ? condition.getText() : '');
                            } else {
                                String vehicleStatus = vehicle.getAttribute('status', null);

                                if (String.isNotBlank(vehicleInterest)) {
                                    setVehicleInterest(lead, vehicleInterest);
                                }

                                if (String.isNotBlank(vehicleStatus)) {
                                    setVehicleStatus(lead, vehicleStatus);
                                }
                                
                                Dom.XmlNode year = vehicle.getChildElement('year', null);
                                lead.Year__c = (year != null ? year.getText() : '');

                                Dom.XmlNode make = vehicle.getChildElement('make', null);
                                lead.Make__c = (make != null ? make.getText() : '');

                                Dom.XmlNode model = vehicle.getChildElement('model', null);
                                lead.Model__c = (model != null ? model.getText() : '');
                                
                                Dom.XmlNode modelCaps = vehicle.getChildElement('Model', null);
                                if (String.isBlank(lead.Model__c)) {
                                    lead.Model__c = (modelCaps != null ? modelCaps.getText() : '');
                                }

                                Dom.XmlNode vin = vehicle.getChildElement('vin', null);
                                lead.VIN__c = (vin != null ? vin.getText() : '');
                                
                                Dom.XmlNode vehicleComment = vehicle.getChildElement('comments', null);
                                lead.Description =  (vehicleComment != null ? vehicleComment.getText() : '');

                                Dom.XmlNode stock = vehicle.getChildElement('stock', null);
                                lead.Stock_Number__c = (stock != null ? stock.getText() : '');
                                
                                Dom.XmlNode bodyStyle = vehicle.getChildElement('bodystyle', null);
                                lead.Body_Style__c = (bodyStyle != null ? bodyStyle.getText() : '');

                                Dom.XmlNode price = vehicle.getChildElement('price', null);
                                lead.Price__c = ((price != null && !String.isBlank(price.getText())) ? Decimal.valueOf(price.getText().replace('$','').replaceAll(',', '')) : null);

                                Dom.XmlNode[] vehicleChilds = vehicle.getChildren();
                            }
                        }
                    }
                }
                Dom.XmlNode vehicle = prospect.getChildElement('vehicle', null);

                // if (vehicle != null) {
                //     String vehicleInterest = vehicle.getAttribute('interest', null);
                //     String vehicleStatus = vehicle.getAttribute('status', null);

                //     if (String.isNotBlank(vehicleInterest)) {
                //         setVehicleInterest(lead, vehicleInterest);
                //     }

                //     if (String.isNotBlank(vehicleStatus)) {
                //         setVehicleStatus(lead, vehicleStatus);
                //     }
                    
                //     Dom.XmlNode year = vehicle.getChildElement('year', null);
                //     lead.Year__c = (year != null ? year.getText() : '');

                //     Dom.XmlNode make = vehicle.getChildElement('make', null);
                //     lead.Make__c = (make != null ? make.getText() : '');

                //     Dom.XmlNode model = vehicle.getChildElement('model', null);
                //     lead.Model__c = (model != null ? model.getText() : '');

                //     Dom.XmlNode vin = vehicle.getChildElement('vin', null);
                //     lead.VIN__c = (vin != null ? vin.getText() : '');

                //     Dom.XmlNode stock = vehicle.getChildElement('stock', null);
                //     lead.Stock_Number__c = (stock != null ? stock.getText() : '');

                //     Dom.XmlNode bodyStyle = vehicle.getChildElement('bodystyle', null);
                //     lead.Body_Style__c = (bodyStyle != null ? bodyStyle.getText() : '');

                //     Dom.XmlNode price = vehicle.getChildElement('price', null);
                //     lead.Price__c = ((price != null && !String.isBlank(price.getText())) ? Decimal.valueOf(price.getText().replace('$','').replaceAll(',', '')) : null);

                //     Dom.XmlNode[] vehicleChilds = vehicle.getChildren();
                // }

                Dom.XmlNode customer = prospect.getChildElement('customer', null);

                if (customer != null) {
                    Dom.XmlNode contact = customer.getChildElement('contact', null);

                    if (contact != null) {
                        Dom.XmlNode[] contactChilds = contact.getChildren();
                        
                        if (contactChilds != null) {
                            Map<String,String> namesPartsMap = new Map<String,String>();
                            for (Dom.XmlNode child : contactChilds) {
                                if (child.getName() == 'name') {
                                    String partAttribute = child.getAttributeValue('part', null);
                                    if (partAttribute != null) {
                                        namesPartsMap.put(partAttribute,child.getText());
                                    }
                                }

                                if (child.getName() == 'phone') {
                                    List<String> notPhoneTypes = new List<String> {'cellphone', 'fax'}; 
                                    if (!notPhoneTypes.contains(child.getAttributeValue('type', null)) && String.isNotBlank(child.getText())) {
                                        lead.Phone = getFormattedPhoneNumber(child.getText());
                                    }

                                    if (child.getAttributeValue('type', null) == 'cellphone' && String.isNotBlank(child.getText())) {
                                        lead.MobilePhone = getFormattedPhoneNumber(child.getText());
                                    }

                                    if (child.getAttributeValue('type', null) == 'fax' && String.isNotBlank(child.getText())) {
                                        lead.Fax = getFormattedPhoneNumber(child.getText());
                                    }
                                }
                            }

                            lead.FirstName = (namesPartsMap.get('first') != null ? namesPartsMap.get('first') : '');
                            lead.LastName = (namesPartsMap.get('last') != null ? namesPartsMap.get('last') : namesPartsMap.get('full'));

                            if (String.isBlank(lead.LastName) && String.isNotBlank(lead.LastName)) {
                                lead.LastName = lead.FirstName;
                            } 
                        }
                        
                        Dom.XmlNode email = contact.getChildElement('email', null);
                        lead.Email = (email != null ? email.getText() : '');

                        Dom.XmlNode address = contact.getChildElement('address', null);

                        if (address != null) {
                            String street1 = '';
                            String street2 = '';

                            for (Dom.XmlNode child : address.getChildren()) {
                                if (child.getName() == 'street') {
                                    if (child.getAttributeValue('line', null) == '1') {
                                        street1 = (child != null ? child.getText() : '');
                                    }

                                    if (child.getAttributeValue('line', null) == '2') {
                                        street2 = (child != null ? child.getText() : '');
                                    }
                                }
                            }

                            lead.Street = street1 + '\n' + street2;

                            Dom.XmlNode city = address.getChildElement('city', null);
                            lead.City = (city != null ? city.getText() : '');

                            Dom.XmlNode regionCode = address.getChildElement('regioncode', null);
                            lead.State = (regionCode != null ? regionCode.getText() : '');

                            Dom.XmlNode postalCode = address.getChildElement('postalcode', null);
                            lead.PostalCode = (postalCode != null ? postalCode.getText() : '');

                            Dom.XmlNode country = address.getChildElement('country', null);
                            lead.Country = (country != null ? country.getText() : '');
                        }
                    }

                    Dom.XmlNode comments = customer.getChildElement('comments', null);
                    if (comments != null && String.isNotBlank(comments.getText()) && String.isBlank(lead.Description)) {
                        lead.Description = comments.getText();
                    }
                }

                Dom.XmlNode vendor = prospect.getChildElement('vendor', null);

                if (vendor != null) {
                    Dom.XmlNode vendorName = vendor.getChildElement('vendorname', null);

                    if (vendorName != null) {
                        Dom.XmlNode vendorname2 = vendorName.getChildElement('vendorname', null);
                        setDealer(lead, vendorName.getText());
                    }

                    Dom.XmlNode vendorContact = vendor.getChildElement('contact', null);

                    if (vendorContact != null) {
                        Dom.XmlNode email = vendorContact.getChildElement('email', null);
                        lead.Dealer_Email__c = (email != null ? email.getText() : '');
                    }
                }

                Dom.XmlNode provider = prospect.getChildElement('provider', null);

                if (provider != null) {
                    Dom.XmlNode providerName = provider.getChildElement('name', null);
                    lead.LeadSource = (providerName != null ? providerName.getText() : '');

                    Dom.XmlNode dealerService = provider.getChildElement('service', null);
                    lead.Dealer_Service__c = (dealerService != null ? dealerService.getText() : '');
                }
            }

            System.debug('lead');
            System.debug(lead);

            Database.SaveResult insertResult = Database.insert(lead, false);

            System.debug('insertResult');
            System.debug(insertResult);

            if (insertResult.isSuccess()) {
                result.success = true;

                Attachment attach = new Attachment(
                    ParentId = insertResult.getId(),
                    Name = 'Email',
                    ContentType = 'text/plain',
                    Body = Blob.valueOf(leadInfo)
                );

                insert attach;

            } else {
                System.debug(insertResult);
                result.success = false;
                result.message = 'ERROR ';
                for (Database.Error error : insertResult.getErrors()) {
                    result.message += ' ' + error.getMessage();
                }
            }
        } catch (Exception e) {
            result.success = false;
            result.message = 'ERROR ' + e.getMessage() + ' ' + e.getCause() + ' ' + e.getStackTraceString();
            System.debug(e.getMessage());
        }
        return result;
    }

    private void setDealer(Lead lead, String dealerName) {
        dealerName = dealerName.deleteWhitespace();
        if (dealerName.containsIgnoreCase(KEY_COL.deleteWhitespace()) || dealerName.containsIgnoreCase(KEY_COL2.deleteWhitespace())) {
            lead.Dealer__c = 'Giant RV - Colton';
        } else if (dealerName.containsIgnoreCase(KEY_DOW.deleteWhitespace()) || dealerName.containsIgnoreCase(KEY_DOW2.deleteWhitespace())) {
            lead.Dealer__c = 'Giant RV - Downey';
        } else if (dealerName.containsIgnoreCase(KEY_MON.deleteWhitespace()) || dealerName.containsIgnoreCase(KEY_MON2.deleteWhitespace())) {
            lead.Dealer__c = 'Giant RV - Montclair';
        } else if (dealerName.containsIgnoreCase(KEY_MUR.deleteWhitespace()) || dealerName.containsIgnoreCase(KEY_MUR2.deleteWhitespace())) {
            lead.Dealer__c = 'Giant RV - Murrieta';
        } else if (dealerName.containsIgnoreCase(KEY_STR)) {
            lead.Dealer__c = KEY_STR;
        } 
    }

    private String getFormattedPhoneNumber(String phoneNumber) {
        if (String.isBlank(phoneNumber)) return '';
        String nonDigits = '[^0-9]';
        String digits = phoneNumber.replaceAll(nondigits, '');
        if (digits.startsWith('1') && digits.length() == 11) {
            digits = digits.substring(1);
        }
        return '(' + digits.substring(0, 3) + ') ' + digits.substring(3, 6) + '-' + digits.substring(6);
    }


    private void setVehicleInterest(Lead lead, String interest) {
        if (interest.equalsIgnoreCase('buy')) {
            lead.Vehicle_Interest__c = 'Buy';
        } else if (interest.equalsIgnoreCase('trade-in')) {
            lead.Vehicle_Interest__c = 'Trade-in';
        }
    }

    private void setVehicleStatus(Lead lead, String status) {
        if (status.equalsIgnoreCase('new')) {
            lead.Vehicle_Status__c = 'New';
        } else if (status.equalsIgnoreCase('used')) {
            lead.Vehicle_Status__c = 'Used';
        }
    }

    private Datetime convertStringToDateTime(String dateTimeVal) {
        try {
            if (dateTimeVal.contains(',')) { 
                return DateTime.parse(dateTimeVal); 
            } else if (dateTimeVal.contains('AM') || dateTimeVal.contains('PM')){
                List<String> splittedDate = dateTimeVal.split(' ');
                String dateString = splittedDate[0];
                String timeString = splittedDate[1];
                String meridiemString;
                if (timeString.lastIndexOf(':') > 0) {
                    timeString = timeString.substring(0, timeString.lastIndexOf(':'));
                }
                if (splittedDate.size() > 2) {
                    meridiemString = splittedDate[2];
                } 
                String formattedDate = dateString + ', ' + timeString;
                if (String.isNotBlank(meridiemString)) {
                    formattedDate += ' ' + meridiemString;
                }
                System.debug(formattedDate);
                return DateTime.parse(formattedDate); 
            } else {
                return Datetime.valueOf(dateTimeVal.replace('T', ' '));
            }
        } catch (Exception e) {
            return Datetime.now();
        }
    }
}