public with sharing class CDKHelpEmployeeParser {
    private static final CDK_Integration__c SETTINGS;
    private static final String EXTRACT_HELP_EMPLOYEE_NAMESPACE;
    private static final String DEFAULT_NODE_CHILD = 'HelpEmployee';

    static {
        SETTINGS = CDK_Integration__c.getOrgDefaults();
        EXTRACT_HELP_EMPLOYEE_NAMESPACE = CDKSoapConstants.NAMESPACE.HELP_EMPLOYEE;
    }

    public static List<CDK_Employee__c> parse(Dom.Document doc, String dealerId, String type) {
        Map<String, Schema.SObjectField> employeeFields = Schema.SObjectType.CDK_Employee__c.fields.getMap();

        List<CDK_Employee__c> employees = new List<CDK_Employee__c>();
        for (Dom.XmlNode child: doc.getRootElement().getChildElements()) {
            if (child.getNodeType() == DOM.XmlNodeType.ELEMENT && child.getName() == DEFAULT_NODE_CHILD) {
                CDK_Employee__c empl = new CDK_Employee__c();
                CDKParser.setFields(empl, employeeFields, HELP_EMPLOYEE_PARSE_MAP, child, EXTRACT_HELP_EMPLOYEE_NAMESPACE);
                empl.DealerId__c = dealerId;
                empl.UniqueId__c = dealerId + empl.Id__c;
                empl.Type__c = type;
                employees.add(empl);
            }
        }

        if (employees.isEmpty()) {
            return null;
        }

        return employees;
    }

    public static final Map<String, String> HELP_EMPLOYEE_PARSE_MAP = new Map<String, String> {
        'Id' => 'Id__c',
        'Name' => 'Name__c'
    };
}