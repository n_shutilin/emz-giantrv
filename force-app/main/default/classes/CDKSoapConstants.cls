public without sharing class CDKSoapConstants {
    public final static CDK_Integration__c SETTINGS = CDK_Integration__c.getOrgDefaults();
    public final static Processes PROCESS = new Processes();
    public final static Queries QUERY = new Queries();
    public final static QParams PARAM = new QParams();
    public final static XMLTags TAG = new XMLTags();
    public final static Pips PIP = new Pips();
    public final static Namespaces NAMESPACE = new Namespaces();
    public final static LoggerMsgTemplates LOGGER_MSG_TEMPLATE = new LoggerMsgTemplates();

    public final static String SOAP_NS = 'http://schemas.xmlsoap.org/soap/envelope/';
    public final static String SOAP_W3 = 'http://www.w3.org/2003/05/soap-envelope';
    public final static String STAR_NS = 'http://www.starstandards.org/STAR';
    public final static String CONTENT_TYPE = 'Content-Type';
    public final static String CONTENT_TYPE_VALUE = 'text/xml';
    public final static String XMLNS_SAVE = 'http://save.customer.pip.adp3pa.dmotorworks.com/';
    public final static Integer TIMEOUT = 60000;
    public final static String PREFIX_ID = '4*';
    public final static String DELTA_NOT_SPECIFIED = 'N/A';

    public final static String DEALER_COLTON = 'Giant RV - Colton';
    public final static String DEALER_DOWNEY = 'Giant RV - Downey';
    public final static String DEALER_MONTCLAIR = 'Giant RV - Montclair';
    public final static String DEALER_MURRIETA = 'Giant RV - Murrieta';
    
    public final static String EMPLOYEE_TYPE_SALES = 'Sales';
    public final static String EMPLOYEE_TYPE_SERVICE = 'Service';
    
    public static String getDealerIdByDealer(String dealer) {
        Map<String, String> dealerNameToSettingId = new Map<String, String> {
            DEALER_COLTON => SETTINGS.DealerId_Colton__c,
            DEALER_DOWNEY => SETTINGS.DealerId_Downey__c,
            DEALER_MONTCLAIR => SETTINGS.DealerId_Montclair__c,
            DEALER_MURRIETA => SETTINGS.DealerId_Murrieta__c
        };
        return dealerNameToSettingId.get(dealer);
    }

    public static String getDealerNameByDealerId(String dealerId) {
        Map<String, String> dealerIdToDealerName = new Map<String, String> {
            SETTINGS.DealerId_Colton__c => DEALER_COLTON,
            SETTINGS.DealerId_Downey__c => DEALER_DOWNEY,
            SETTINGS.DealerId_Montclair__c => DEALER_MONTCLAIR,
            SETTINGS.DealerId_Murrieta__c => DEALER_MURRIETA
        };
        return dealerIdToDealerName.get(dealerId);
    }

    public static String getAccountFieldNameByDealer(String dealer) {
        Map<String, String> dealerNameToAccountFieldName = new Map<String, String> {
            DEALER_COLTON => 'CDK_Customer_Colton__c',
            DEALER_DOWNEY => 'CDK_Customer_Downey__c',
            DEALER_MONTCLAIR => 'CDK_Customer_Montclair__c',
            DEALER_MURRIETA => 'CDK_Customer_Murrieta__c'
        };
        return dealerNameToAccountFieldName.get(dealer);
    }

    public static List<String> getListOfDealers() {
        return new List<String> {
            SETTINGS.DealerId_Colton__c,
            SETTINGS.DealerId_Downey__c,
            SETTINGS.DealerId_Montclair__c,
            SETTINGS.DealerId_Murrieta__c
        };
    }

    public static String getInvCompanyByDealerId(String dealerId) {
        Map<String, String> dealerIdToInvCompany = new Map<String, String> {
            SETTINGS.DealerId_Colton__c => SETTINGS.InvCompany_Colton__c,
            SETTINGS.DealerId_Downey__c => SETTINGS.InvCompany_Downey__c,
            SETTINGS.DealerId_Montclair__c => SETTINGS.InvCompany_Montclair__c,
            SETTINGS.DealerId_Murrieta__c => SETTINGS.InvCompany_Murrieta__c
        };
        return dealerIdToInvCompany.containsKey(dealerId) ? dealerIdToInvCompany.get(dealerId) : null;
    }

    public static List<String> countUpsertResults(Database.UpsertResult[] results) {
        Integer numberOfInserted = 0;
        Integer numberOfUpdated = 0;

        for (Database.UpsertResult result : results) {
            if (!result.isSuccess()) {
                continue;
            }
            if (result.isCreated()) {
                numberOfInserted++;
            } else {
                numberOfUpdated++;
            }
        }

        return new List<String>{ String.valueOf(numberOfInserted),  String.valueOf(numberOfUpdated) };
    }

    public class Dealers {
        public final String COLTON = 'Giant RV - Colton';
        public final String DOWNEY = 'Giant RV - Downey';
        public final String MONTCLAIR = 'Giant RV - Montclair';
        public final String MURRIETA = 'Giant RV - Murrieta';
    }

    public class Processes {
        public final String CUSTOMER_EXTRACT = 'CDK [Customer Extract]';
        public final String CUSTOMER_SEARCH = 'CDK [Customer Search]';
        public final String CUSTOMER_INSERT = 'CDK [Customer Insert]';
        public final String DEAL_EXTRACT = 'CDK [Deal Extract]';
        public final String DEAL_INSERT = 'CDK [Deal Insert]';
        public final String REPAIR_ORDER_CLOSED_EXTRACT = 'CDK [Repair Order Closed Extract]';
        public final String REPAIR_ORDER_OPEN_EXTRACT = 'CDK [Repair Order Open Extract]';
        public final String INVENTORY_EXTRACT = 'CDK [Inventory Extract]';
        public final String HELP_EMPLOYEE_SALES_EXTRACT = 'CDK [Help Employee Sales Extract]';
        public final String HELP_EMPLOYEE_SERVICE_EXTRACT = 'CDK [Help Employee Service Extract]';
        public final String WE_OWE_EXTRACT = 'CDK [We Owe Extract]';
        public final String WEB_CREDIT_INSERT = 'CDK [Web Credit Insert]';
    }

    public class Queries {
        public final String CUSTOMER_BULK_EXTRACT = 'CUST_Bulk';
        public final String CUSTOMER_DELTA_EXTRACT = 'CUST_Delta';
        public final String FI_SALES_CLOSED_DELTA_EXTRACT = 'FISC_Delta';
        public final String SERVICE_RO_CLOSED_EXTRACT = 'SROD_Closed_DateRange';
        public final String SERVICE_RO_OPEN_EXTRACT = 'SROD_Open_WIP';
        public final String SERVICE_RO_WIP_EXTRACT = 'SROD_WIP';
        public final String INVENTORY_DELTA_EXTRACT = 'IVEH_Delta';
        public final String HELP_EMPLOYEE_DELTA_SALES_EXTRACT = 'HEMPL_Delta_Sales';
        public final String HELP_EMPLOYEE_DELTA_SERVICE_EXTRACT = 'HEMPL_Delta_Service';
        public final String WE_OWE_DELTA_EXTRACT = 'WO_Delta';
    }
   
    public class QParams {
        public final String START_DATE = 'qparamStartDate';
        public final String END_DATE = 'qparamEndDate';
        public final String DELTA_DATE = 'deltaDate';
        public final String DEALER_ID = 'dealerId';
        public final String QUERY_ID = 'queryId';
        public final String INV_COMPANY = 'qparamInvCompany';
    }

    public class XMLTags {
        public final String ENVELOPE = 'Envelope';
        public final String BODY = 'Body';
        public final String HEADER = 'Header';
        public final String USERNAME = 'username';
        public final String PASSWORD = 'password';
        public final String ID = 'id';
        public final String ARGUMENT_0 = 'arg0';
        public final String ARGUMENT_1 = 'arg1';
        public final String ARGUMENT_2 = 'arg2';
        public final String ARGUMENT_3 = 'arg3';
        public final String USER_ID = 'userId';
        public final String DEALER_ID = 'dealerId';
        public final String VALUE = 'value';
        public final String AUTH_TOKEN = 'authToken';
        public final String REQ = 'req';


        public final String CUSTOMER_SEARCH_BULK = 'executeSearchBulk';
        public final String CUSTOMER_SEARCH_BULK_RESPONSE = 'executeSearchBulkResponse';
        public final String CUSTOMER_GET_NUMBER = 'getCustomerNumber';
        public final String CUSTOMER_GET_NUMBER_REPSONSE = 'getCustomerNumberResponse';
        public final String DEAL_INSERT = 'insertDeal';
        public final String PROCESS_CREDIT_APP = 'processCreditApp';
        public final String PROCESS_CREDIT_APP_RESPONSE = 'processCreditAppResponse';
        public final String DEAL_INSERT_RESPONSE = 'insertDealResponse';
        public final String RO_CLOSED = 'service-repair-order-closed';
        public final String RO_OPEN = 'service-repair-order-open';
        public final String FAULT = 'Fault';
    }

    public class Pips {
        public final String DEAL = 'http://www.dmotorworks.com/pip-deal';
        public final String CUSTOMER = 'http://www.dmotorworks.com/pip-customer';
        public final String WEB_CREDIT = 'http://www.dmotorworks.com/pip-web-credit';
    }

    public class Namespaces {
        public final String CUSTOMER = 'http://www.dmotorworks.com/pip-extract-customer'; 
        public final String DEAL = 'http://www.dmotorworks.com/pip-extract-fisales-closed'; 
        public final String INVENTORY = 'http://www.dmotorworks.com/pip-extract-inventory-vehicle'; 
        public final String REPAIR_ORDER_CLOSED = 'http://www.dmotorworks.com/service-repair-order-closed'; 
        public final String REPAIR_ORDER_OPEN = 'http://www.dmotorworks.com/service-repair-order-open'; 
        public final String HELP_EMPLOYEE = 'http://www.dmotorworks.com/pip-extract-help-employee'; 
    }

    public class LoggerMsgTemplates {
        public final String RESPONDED_WITH_NULL = 'Responded with NULL - DELTA {0} [Dealer: {1}]';
        public final String RESPONDED_WITH_STATUS = 'Responded with {0} status - DELTA {1} [Dealer: {2}]';
        public final String RESPONDED_WITH_STATUS_ONLY = 'Responded with {0} status';
        public final String LIST_UPSERTED_EMPTY = 'List of records to be upserted is empty [Dealer: {0}]';
        public final String INSERTED_UPDATED = 'Inserted {0} records / Updated {1} records';   
    }
}