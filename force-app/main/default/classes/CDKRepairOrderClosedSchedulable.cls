global without sharing class CDKRepairOrderClosedSchedulable implements Schedulable {
    private static final ApexLogger LOGGER;
    private static final CDK_Integration__c SETTINGS;

    static {
        LOGGER = ApexLogger.create(CDKSoapConstants.PROCESS.REPAIR_ORDER_CLOSED_EXTRACT);
        SETTINGS = CDK_Integration__c.getOrgDefaults();
    }

    global void execute(SchedulableContext sc) {
        syncRepairOrders();
        this.rescheduleJob(sc);
    }

    @future(callout=true)
    global static void syncRepairOrders() {
        String pStartDate = Date.today().addDays(-3).format();
        String pEndDate = Date.today().format();
        String deltaDate = pStartDate + '-' + pEndDate;
        List<CDK_Repair_Order__c> orders = new List<CDK_Repair_Order__c>();
        List<CDK_Repair_Order_Detail__c> orderDetails = new List<CDK_Repair_Order_Detail__c>();
        List<CDK_Repair_Order_Part__c> orderParts = new List<CDK_Repair_Order_Part__c>();

        for (String dealerId : CDKSoapConstants.getListOfDealers()) {
            try {
                HttpResponse response = CDKApi.doPost(
                    SETTINGS.PIP_Service_RO_Closed__c, 
                    new Map<String, String>{
                        CDKSoapConstants.PARAM.DEALER_ID => dealerId, 
                        CDKSoapConstants.PARAM.QUERY_ID => CDKSoapConstants.QUERY.SERVICE_RO_CLOSED_EXTRACT,
                        CDKSoapConstants.PARAM.START_DATE => pStartDate,
                        CDKSoapConstants.PARAM.END_DATE => pEndDate 
                    }
                );
                LOGGER.debug(response.getBody());
                if (response == null) {
                    LOGGER.addLog(String.format(
                        CDKSoapConstants.LOGGER_MSG_TEMPLATE.RESPONDED_WITH_NULL, 
                        new List<String>{ deltaDate, dealerId }
                    ));
                    continue;
                }
        
                if (response.getStatusCode() != HTTPConstants.Status.OK) {
                    LOGGER.addLog(String.format(
                        CDKSoapConstants.LOGGER_MSG_TEMPLATE.RESPONDED_WITH_STATUS,
                        new List<String>{ response.getStatus(), deltaDate, dealerId }
                    ));
                    continue;
                }

                Map<String, List<SObject>> parseResult = CDKRepairOrderParser.parse(
                    response.getBodyDocument(), dealerId, CDKSoapConstants.NAMESPACE.REPAIR_ORDER_CLOSED, CDKSoapConstants.TAG.RO_CLOSED
                );
                if (parseResult == null) continue;

                List<CDK_Repair_Order__c> ordersToUpsert = parseResult.get('orders');
                List<CDK_Repair_Order_Detail__c> orderDetailsToUpsert = parseResult.get('orderDetails');
                List<CDK_Repair_Order_Part__c> orderPartsToUpsert = parseResult.get('orderParts');

                if (ordersToUpsert == null || ordersToUpsert.isEmpty()) {
                    LOGGER.addLog(String.format(
                        CDKSoapConstants.LOGGER_MSG_TEMPLATE.LIST_UPSERTED_EMPTY,
                        new List<String>{ dealerId }
                    ));
                    continue;
                }
                orders.addAll(ordersToUpsert);
                orderDetails.addAll(orderDetailsToUpsert);
                orderParts.addAll(orderPartsToUpsert);
            } catch (Exception e) {
                LOGGER.addLog(e);
            }
        }
        upsertRepairOrders(orders, orderDetails, orderParts);
        LOGGER.save();
    }

    public static void upsertRepairOrders(List<CDK_Repair_Order__c> ordersToUpsert, List<CDK_Repair_Order_Detail__c> orderDetailsToUpsert, List<CDK_Repair_Order_Part__c> orderPartsToUpsert) {
        if (ordersToUpsert == null || ordersToUpsert.isEmpty()) return;

        Database.UpsertResult[] results = Database.upsert(ordersToUpsert, CDK_Repair_Order__c.fields.UniqueId__c, false);
        LOGGER.addLog(String.format(
            CDKSoapConstants.LOGGER_MSG_TEMPLATE.INSERTED_UPDATED, 
            CDKSoapConstants.countUpsertResults(results)
        ));
        Database.upsert(orderDetailsToUpsert, CDK_Repair_Order_Detail__c.fields.UniqueId__c, false);
        Database.upsert(orderPartsToUpsert, CDK_Repair_Order_Part__c.fields.UniqueId__c, false);
    }

    global void rescheduleJob(SchedulableContext sc) {
        Datetime nextScheduleTime = Datetime.newInstance(
            Date.today().addDays(1).year(), 
            Date.today().addDays(1).month(), 
            Date.today().addDays(1).day(), 
            22, 5, 0
        );
        
        String hour = String.valueOf(nextScheduleTime.hour());
        String minutes = String.valueOf(nextScheduleTime.minute());
        String day = String.valueOf(nextScheduleTime.day());
        String cronValue = '0 ' + minutes + ' ' + hour + ' * * ?';
        String jobName = CDKSoapConstants.PROCESS.REPAIR_ORDER_CLOSED_EXTRACT + ': ' + nextScheduleTime.format('hh:mm');

        CDKRepairOrderClosedSchedulable nextJob = new CDKRepairOrderClosedSchedulable();
        System.schedule(jobName, cronValue, nextJob);

        System.abortJob(sc.getTriggerId());
    }
}

//system.schedule('CDK rep', '0 33  * * * ?', new CDKRepairOrderClosedSchedulable());