@IsTest
public without sharing class CDKCustomerInsertUpdateTest {
    private static final CDKCustomerInsertUpdate customerInsertUpdate = new CDKCustomerInsertUpdate(ApexLogger.create());
    @TestSetup
    public static void makeData(){
        CDKTestDataFactory.initSettings();
    }

    @IsTest
    public static void testGetCustomerNumber() {
        String expectedCustomerNumber = CDKTestDataFactory.CUSTOMER_NUMBER;
        Test.setMock(HttpCalloutMock.class, CDKTestDataFactory.getApiMock(CDKTestDataFactory.MOCK.CUSTOMER_GET_NUMBER));
        Test.startTest();
        String actualCustomerNumber = customerInsertUpdate.getCustomerNumber(CDKTestDataFactory.getDealerId());
        Test.stopTest();
        System.assertEquals(expectedCustomerNumber, actualCustomerNumber);
    }
    
    @IsTest
    public static void testInsertUpdateCustomer() {
        String accountId = CDKTestDataFactory.createPersonAccount();
        String expectedCustomerId = CDKTestDataFactory.CUSTOMER_NUMBER;
        Test.setMock(HttpCalloutMock.class, CDKTestDataFactory.getApiMock(CDKTestDataFactory.MOCK.CUSTOMER_INSERT_UPDATE));
        Test.startTest();
        CDKCustomerInsertUpdate.CustomerInfo custInfo = 
            customerInsertUpdate.insertUpdateCustomer(CDKTestDataFactory.getDealerId(), accountId);
        Test.stopTest();
        System.assertEquals(expectedCustomerId, custInfo.custId);
    }
    
    @IsTest
    public static void testParseInsertResponse() {
        String expectedCustomerId = CDKTestDataFactory.CUSTOMER_NUMBER;
        Dom.Document doc = new Dom.Document();
        doc.load(CDKTestDataFactory.BODY_CUSTOMER_INSERT_UPDATE);
        Test.startTest();
        CDKCustomerInsertUpdate.CustomerInfo custInfo = customerInsertUpdate.parseInsertResponse(doc);
        Test.stopTest();
        System.assertEquals(expectedCustomerId, custInfo.custId);
    }
}