@IsTest
public without sharing class CDKDealInsertUpdateTest {
    private static final CDKDealInsertUpdate dealInsertUpdate = new CDKDealInsertUpdate(ApexLogger.create());
    @TestSetup
    public static void makeData(){
        CDKTestDataFactory.initSettings();
        CDKTestDataFactory.createOpportunityWithMatchingVehicle();
    }
    
    @IsTest
    public static void testInsertDeal() {
        Opportunity opp = [
            SELECT Id, AccountId, CDK_Deal_Number__c, CDK_Deal_Inserted_Date__c, Dealer__c, CDK_Customer_Number__c
            FROM Opportunity
            LIMIT 1
        ];

        Matching_Vehicles__c cdkVehicle = [
            SELECT Id, Inventory__r.Stock_Number__c 
            FROM Matching_Vehicles__c
            WHERE Opportunity__c = :opp.Id
            LIMIT 1
        ];

        String expectedDealId = CDKTestDataFactory.DEAL_NUMBER;
        Test.setMock(HttpCalloutMock.class, CDKTestDataFactory.getApiMock(CDKTestDataFactory.MOCK.DEAL_INSERT_UPDATE));
        Test.startTest();
        List<CDKDealInsertUpdate.DealInfo> dealInfo = 
            dealInsertUpdate.insertDeal(opp, cdkVehicle, CDKTestDataFactory.CUSTOMER_NUMBER, CDKTestDataFactory.getDealerId());
        Test.stopTest();
        System.assert(dealInfo != null);
        System.assertEquals(expectedDealId, dealInfo.get(0).dealId);
    }
    
    @IsTest
    public static void testParseInsertResponse() {
        String expectedDealId = CDKTestDataFactory.DEAL_NUMBER;
        Dom.Document doc = new Dom.Document();
        doc.load(CDKTestDataFactory.BODY_DEAL_INSERT_UPDATE);
        Test.startTest();
        List<CDKDealInsertUpdate.DealInfo> dealInfo = dealInsertUpdate.parseInsertResponse(doc);
        Test.stopTest();
        System.assert(dealInfo != null);
        System.assertEquals(expectedDealId, dealInfo.get(0).dealId);
    }
}