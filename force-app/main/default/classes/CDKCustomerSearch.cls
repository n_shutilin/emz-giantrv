public without sharing class CDKCustomerSearch {
    private static final ApexLogger LOGGER;
    private static final CDK_Integration__c SETTINGS;

    static {
        LOGGER = ApexLogger.create(CDKSoapConstants.PROCESS.CUSTOMER_SEARCH);
        SETTINGS = CDK_Integration__c.getOrgDefaults();
    }

    public static List<String> executeSearchBulk(String dealerId, String searchName, String searchPhone) {
        DOM.Document requestBody = createSearchRequestBody(dealerId, searchName, searchPhone);
        System.debug(requestBody.toXmlString());
        HttpResponse response = CDKApi.doPost(SETTINGS.PIP_Customer_Search__c, requestBody);

        if (response.getStatusCode() != HTTPConstants.Status.OK) {
            return null;
        }

        Dom.Document docResult = response.getBodyDocument();
        Dom.XmlNode envelopeResult = docResult.getRootElement();
        Dom.XmlNode bodyResult = envelopeResult.getChildElement(CDKSoapConstants.TAG.BODY, CDKSoapConstants.SOAP_NS);
        Dom.XmlNode executeSearchResponse = bodyResult.getChildElement(CDKSoapConstants.TAG.CUSTOMER_SEARCH_BULK_RESPONSE, CDKSoapConstants.PIP.CUSTOMER);
        Dom.XmlNode returnResult = executeSearchResponse.getChildElement('return', null);

        List<String> results = new List<String>();

        if (returnResult != null) {
            for (Dom.XmlNode child : executeSearchResponse.getChildElements()) {
                Dom.XmlNode id = child.getChildElement('id', null);
                Dom.XmlNode value = id.getChildElement('value', null);

                results.add(value.getText().remove(CDKSoapConstants.PREFIX_ID));
            }

            return results;
        }

        return null;
    }

    private static DOM.Document createSearchRequestBody(String dealerId, String searchName, String searchPhone) {
        DOM.Document doc = new DOM.Document();
        dom.XmlNode envelope = doc.createRootElement(CDKSoapConstants.TAG.ENVELOPE, CDKSoapConstants.SOAP_NS, 'S');
        dom.XmlNode header = envelope.addChildElement(CDKSoapConstants.TAG.HEADER, CDKSoapConstants.SOAP_NS, 'S');
        dom.XmlNode body = envelope.addChildElement(CDKSoapConstants.TAG.BODY, CDKSoapConstants.SOAP_NS, 'S');
        dom.XmlNode executeSearch = body.addChildElement(CDKSoapConstants.TAG.CUSTOMER_SEARCH_BULK, CDKSoapConstants.PIP.CUSTOMER, 'ns2');

        dom.XmlNode arg0 = executeSearch.addChildElement(CDKSoapConstants.TAG.ARGUMENT_0, null, null);
        arg0.addChildElement(CDKSoapConstants.TAG.PASSWORD, null, null).addTextNode(SETTINGS.Password__c);
        arg0.addChildElement(CDKSoapConstants.TAG.USERNAME, null, null).addTextNode(SETTINGS.UserID__c);

        dom.XmlNode arg1 = executeSearch.addChildElement(CDKSoapConstants.TAG.ARGUMENT_1, null, null);
        arg1.addChildElement(CDKSoapConstants.TAG.DEALER_ID, null, null).addTextNode(dealerId);

        dom.XmlNode arg2 = executeSearch.addChildElement(CDKSoapConstants.TAG.ARGUMENT_2, null, null);
        arg2.addChildElement('key', null, null).addTextNode(searchName);
        arg2.addChildElement('verb', null, null).addTextNode('LIKE');

        if (String.isNotBlank(searchPhone)) {
            arg2.addChildElement('key', null, null).addTextNode(searchPhone);
            arg2.addChildElement('verb', null, null).addTextNode('PHONE');
        }
        
        return doc;
    }
}