public without sharing class GrvEmailManagement {
    private static final Integer RECORDS_ON_PAGE = 10;
    private static final String LIGHTNING_EMAIL_MESSAGE_URL = '/lightning/r/EmailMessage/';
    private static final String VIEW_URL_PARAM = '/view';
    private static final String EMAIL_OPTION_UNREAD = 'unread';
    private static final String EMAIL_OPTION_OUTGOING = 'outgoing';
    private static final String EMAIL_OPTION_INCOMING = 'incoming';

    @AuraEnabled(Cacheable=true)
    public static List<OptionsWrapper> getUserOptions() {
        List<OptionsWrapper> userOptions = new List<OptionsWrapper>();
        List<Email_Tracking_Member__c> emailTrackingMembers = [
            SELECT Member__c 
            FROM Email_Tracking_Member__c
            WHERE Manager__c = :UserInfo.getUserId()
            AND Member__c != null
        ];
        Set<Id> memberIds = new Set<Id>();
        for (Email_Tracking_Member__c member : emailTrackingMembers) {
            memberIds.add(member.Member__c);
        }

        List<User> users = [
            SELECT Name, Email
            FROM User
            WHERE Track_Email__c = true 
            AND (Id = :UserInfo.getUserId() OR Id IN :memberIds)
        ];

        for (User user : users) {
            userOptions.add(new OptionsWrapper(user.Name, user.Email));
        }

        return userOptions;
    }

    @AuraEnabled
    public static List<MessageWrapper> getMessages(FilterWrapper filter, Integer pageNum){
        Integer offset = (pageNum - 1) * RECORDS_ON_PAGE;

        String address = filter.address;
        String emailOption = filter.emailOption;
        Date startDate = filter.startDate != null ? Date.valueOf(filter.startDate) : null;
        Date endDate = filter.endDate != null ? Date.valueOf(filter.endDate) : null;

        Set<Id> emsIds = getEmsIds(filter.address);
        String query = getQuery(address, emailOption, startDate, endDate, emsIds);
        query += ' ORDER BY CreatedDate DESC LIMIT :RECORDS_ON_PAGE OFFSET :offset';

        List<EmailMessageRelation> messageRelations = Database.query(query);
        List<MessageWrapper> messages = new List<MessageWrapper>();
        
        if (!messageRelations.isEmpty()) {
            Set<Id> emailMessageIds = new Set<Id>();
            Map<Id, Id> emailMessageIdToAdditionalRelationIdMap = new Map<Id, Id>();
            Map<Id, String> emailMessageIdToAdditionalRelationNameMap = new Map<Id, String>();
            Map<Id, String> emailMessageIdToRecipientName = new Map<Id, String>();
            for (EmailMessageRelation relation : messageRelations) {
                emailMessageIds.add(relation.EmailMessageId);
            }
            List<EmailMessageRelation> additionalRelations = [
                SELECT Id, RelationId, EmailMessageId, Relation.Name, EmailMessage.ToAddress, RelationAddress
                FROM EmailMessageRelation
                WHERE RelationId != null
                AND EmailMessageId IN :emailMessageIds
            ];
            if (!additionalRelations.isEmpty()) {
                for (EmailMessageRelation addRel : additionalRelations) {
                    emailMessageIdToAdditionalRelationIdMap.put(addRel.EmailMessageId, addRel.RelationId);
                    emailMessageIdToAdditionalRelationNameMap.put(addRel.EmailMessageId, addRel.Relation.Name);
                    if (addRel.EmailMessage.ToAddress == addRel.RelationAddress && String.isNotBlank(addRel.Relation.Name)) {
                        emailMessageIdToRecipientName.put(addRel.EmailMessageId, addRel.Relation.Name);
                    }
                }
            }
            for (EmailMessageRelation relation : messageRelations) {
                MessageWrapper wrappedMsg;
                if (relation.RelationId == null && relation.EmailMessage.RelatedToId == null && emailMessageIdToAdditionalRelationIdMap.containsKey(relation.EmailMessageId)) {
                    relation.RelationId = emailMessageIdToAdditionalRelationIdMap.get(relation.EmailMessageId);
                    wrappedMsg = new MessageWrapper(relation, emsIds);
                    wrappedMsg.recordName = emailMessageIdToAdditionalRelationNameMap.get(relation.EmailMessageId);
                } else {
                    wrappedMsg = new MessageWrapper(relation, emsIds);
                }
                if (emailMessageIdToRecipientName.containsKey(relation.EmailMessageId)) {
                    wrappedMsg.toAddress = emailMessageIdToRecipientName.get(relation.EmailMessageId) + ' (' + wrappedMsg.toAddress + ')';
                }
                messages.add(wrappedMsg);
            }
        }

        return messages;
    }

    @AuraEnabled
    public static Double getNumberMessagesPages(FilterWrapper filter){
        String address = filter.address;
        String emailOption = filter.emailOption;
        Date startDate = filter.startDate != null ? Date.valueOf(filter.startDate) : null;
        Date endDate = filter.endDate != null ? Date.valueOf(filter.endDate) : null;

        Set<Id> emsIds = getEmsIds(filter.address);
        String query = getCountQuery(address, emailOption, startDate, endDate, emsIds);
        Integer numberOfMessages = Database.countQuery(query);

        return Math.ceil(Double.valueOf(numberOfMessages) / Double.valueOf(RECORDS_ON_PAGE));
    }

    @AuraEnabled
    public static String deleteOpener(Id recId) {
        User user = [
            SELECT Email
            FROM User
            WHERE Id = :UserInfo.getUserId()
            LIMIT 1
        ];

        List<Email_Message_Opener__c> openers = [
            SELECT Id
            FROM Email_Message_Opener__c
            WHERE Email_Message_Id__c = :recId
            AND Email__c = :user.Email
        ];

        if (!openers.isEmpty()) {
            delete openers;
            return recId;
        }
        return null;
    }

    @AuraEnabled
    public static void deleteMessages(List<String> messageIds) {
        List<EmailMessage> messagesToDelete = [
            SELECT Id 
            FROM EmailMessage
            WHERE Id IN :messageIds
        ];
        List<Email_Message_Opener__c> emsOpenersToDelete = [
            SELECT Id, Email_Message_Id__c
            FROM Email_Message_Opener__c
            WHERE Email_Message_Id__c IN :messageIds
        ];
        if (!messagesToDelete.isEmpty()) {
            delete messagesToDelete;
        }
        if (!emsOpenersToDelete.isEmpty()) {
            delete emsOpenersToDelete;
        }
    }

    @AuraEnabled
    public static EmailMessage getEmailMessageById(String messageId){
        List<EmailMessage> emailMessages = [
            SELECT Id, ToIds, Subject, HtmlBody, BccAddress, BccIds, CcAddress, CcIds, FromAddress,
                FromName, MessageDate, ToAddress, Status, CreatedById, CreatedBy.Name, CreatedDate
            FROM EmailMessage
            WHERE Id = :messageId
        ];

        if (emailMessages.isEmpty()) return null;
        return emailMessages.get(0);
    }
    
    @AuraEnabled
    public static List<FieldWrapper> getOpportunityFieldSet(){
        List<FieldWrapper> fields = new List<FieldWrapper>();
        for(Schema.FieldSetMember f : SObjectType.Opportunity.FieldSets.Email_Management_System.getFields()) {
            fields.add(new FieldWrapper(f));
        }
        return fields;
    }

    @AuraEnabled
    public static void sendEmail(SingleEmailMessageWrapper message){
        System.debug(message);
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
		email.setToAddresses(message.toAddresses);
		email.setCcAddresses(message.ccAddresses);
		email.setBccAddresses(message.bccAddresses);
        if (String.isNotBlank(message.whatId)) {
            email.setWhatId(message.whatId);
        }
        if (String.isNotBlank(message.templateId)) {
            email.setTreatBodiesAsTemplate(true);
        }
        if (message.documentIds != null && message.documentIds.size() > 0) {
            email.setEntityAttachments(message.documentIds);
        }
		email.setSubject(message.subject);
		email.setHtmlBody(message.body);
		Messaging.sendEmail(new List<Messaging.SingleEmailMessage> { email });
    }

    @AuraEnabled
    public static List<EmailTemplate> getEmailTemplates(String uiType){
        List<EmailTemplate> templates = [
           SELECT Id, Name, Subject, HtmlValue, TemplateType, FolderName, Description, Body, UIType
           FROM EmailTemplate
           WHERE UiType = :uiType 
           AND IsActive = true
        ];

        return templates;
    }

    private static Set<Id> getEmsIds(String email) {
        List<Email_Message_Opener__c> emsOpeners = [
            SELECT Email_Message_Id__c
            FROM Email_Message_Opener__c
            WHERE Email__c = :email
        ];

        Set<Id> emsIds = new Set<Id>();

        for (Email_Message_Opener__c emsOpener : emsOpeners) {
            emsIds.add(emsOpener.Email_Message_Id__c);
        }

        return emsIds;
    }

    private static String getQuery(String address, String emailOption, Date startDate, Date endDate, Set<Id> emsIds) {
        String query = 'SELECT EmailMessageId, EmailMessage.FromName, EmailMessage.FromAddress, EmailMessage.ToAddress,' +
            ' EmailMessage.Subject, EmailMessage.relatedToId, Relation.Name, EmailMessage.RelatedTo.Name,' +
            ' CreatedDate, RelationId, RelationObjectType, EmailMessage.IsOpened, EmailMessage.Incoming, RelationType, RelationAddress' +
            ' FROM EmailMessageRelation';
        
        query += getQueryFilters(address, emailOption, startDate, endDate, emsIds);
        return query;
    }

    private static String getCountQuery(String address, String emailOption, Date startDate, Date endDate, Set<Id> emsIds) {
        String query = 'SELECT COUNT() FROM EmailMessageRelation';
        
        query += getQueryFilters(address, emailOption, startDate, endDate, emsIds);
        return query;
    }

    private static String getQueryFilters(String address, String emailOption, Date startDate, Date endDate, Set<Id> emsIds) {
        String filters = ' WHERE RelationAddress = :address';

        if (startDate != null) {
            filters += ' AND DAY_ONLY(CreatedDate) >= :startDate';
        }

        if (endDate != null) {
            filters += ' AND DAY_ONLY(CreatedDate) <= :endDate';
        }

        if (emailOption != null) {
            if (emailOption == EMAIL_OPTION_INCOMING) {
                filters += ' AND (RelationType = \'ToAddress\' OR RelationType = \'CcAddress\' OR RelationType = \'BccAddress\')';
            } else if (emailOption == EMAIL_OPTION_OUTGOING) {
                filters += ' AND RelationType = \'FromAddress\'';
            } else if (emailOption == EMAIL_OPTION_UNREAD) {
                filters += ' AND EmailMessageId IN :emsIds' + ' AND (RelationType = \'ToAddress\' OR RelationType = \'CcAddress\' OR RelationType = \'BccAddress\')';
            }
        }
        return filters;
    }

    public class FieldWrapper {
        @AuraEnabled
        public String name { get; set; }
        @AuraEnabled
        public String label { get; set; }
        @AuraEnabled
        public String type { get; set; }
        @AuraEnabled
        public String value { get; set; }

        public FieldWrapper(Schema.FieldSetMember fieldSetMember) {
            this.name = fieldSetMember.fieldpath;
            this.type = String.valueOf(fieldSetMember.getType());
            this.label = fieldSetMember.getLabel();
        }
    }

    public class FilterWrapper {
        @AuraEnabled 
        public String address { get; set; }
        @AuraEnabled 
        public String emailOption { get; set; }
        @AuraEnabled 
        public String startDate { get; set; }
        @AuraEnabled 
        public String endDate { get; set; }
    }

    public class MessageWrapper {
        @AuraEnabled 
        public String id { get; set; }
        @AuraEnabled 
        public String relationId { get; set; }
        @AuraEnabled 
        public String fromAddress { get; set; }
        @AuraEnabled 
        public String toAddress { get; set; }
        @AuraEnabled 
        public String subject { get; set; }
        @AuraEnabled 
        public String messageLink { get; set; }
        @AuraEnabled 
        public String recordLink { get; set; }
        @AuraEnabled 
        public String recordName { get; set; }
        @AuraEnabled 
        public String recordId { get; set; }
        @AuraEnabled 
        public Boolean isOpportunityLinked { get; set; }
        @AuraEnabled 
        public String sendingDateTime { get; set; }
        @AuraEnabled 
        public Boolean isOpened { get; set; }
        @AuraEnabled 
        public Boolean isInbound { get; set; }

        public MessageWrapper(EmailMessageRelation relation, Set<Id> notOpenedMessageIdSet) {
            String orgUrl = Url.getSalesforceBaseUrl().toExternalForm();

            this.id = relation.EmailMessageId;
            this.relationId = relation.Id;
            this.fromAddress = relation.EmailMessage.FromName != null ? relation.EmailMessage.FromName + ' (' + relation.EmailMessage.FromAddress + ')' : relation.EmailMessage.FromAddress;
            this.toAddress = relation.EmailMessage.ToAddress;
            this.subject = String.isNotBlank(relation.EmailMessage.Subject) ? relation.EmailMessage.Subject : '[No Subject]';
            this.messageLink = orgUrl + LIGHTNING_EMAIL_MESSAGE_URL + relation.EmailMessage.Id + VIEW_URL_PARAM;

            if (relation.EmailMessage.RelatedToId != null) {
                this.recordLink = orgUrl + '/' + relation.EmailMessage.RelatedToId;
                this.recordId = relation.EmailMessage.RelatedToId;
                this.isOpportunityLinked = relation.EmailMessage.RelatedToId.getSobjectType() == Schema.Opportunity.SObjectType;
            } else if (relation.RelationId != null) {
                this.recordLink = orgUrl + '/' + relation.RelationId;
                this.recordId = relation.RelationId;
                this.isOpportunityLinked = relation.RelationId.getSobjectType() == Schema.Opportunity.SObjectType;
            } else {
                this.recordLink = '';
            }

            this.recordName = relation.EmailMessage.RelatedToId != null ? relation.EmailMessage.RelatedTo.Name : relation.Relation.Name;
            this.sendingDateTime = relation.CreatedDate.format('M/d/yyyy h:mm a');
            this.isOpened = !notOpenedMessageIdSet.contains(relation.EmailMessageId);
            this.isInbound = relation.RelationType == 'ToAddress' || relation.RelationType == 'CcAddress' || relation.RelationType == 'BccAddress';
        }
    }

    public class OptionsWrapper {
        @AuraEnabled 
        public String label { get; set; }
        @AuraEnabled 
        public String value { get; set; }

        public OptionsWrapper(String label, String value) {
            this.label = label;
            this.value = value;
        }
    }

    public class SingleEmailMessageWrapper {
        @AuraEnabled 
        public String fromAddress {get; set;}
        @AuraEnabled 
        public List<String> toAddresses {get; set;}
        @AuraEnabled 
        public List<String> ccAddresses {get; set;}
        @AuraEnabled 
        public List<String> bccAddresses {get; set;}
        @AuraEnabled 
        public String whatId {get; set;}
        @AuraEnabled 
        public String subject {get; set;}
        @AuraEnabled 
        public String body {get; set;}
        @AuraEnabled 
        public String templateId {get; set;}
        @AuraEnabled 
        public List<String> documentIds {get; set;}
    }
}