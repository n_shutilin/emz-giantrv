public without sharing class CDKCreditInsert {
    private ApexLogger LOGGER;
    private static final CDK_Integration__c SETTINGS;

    static { SETTINGS = CDK_Integration__c.getOrgDefaults(); }

    public CDKCreditInsert(ApexLogger loggerInstance) {
        this.LOGGER = loggerInstance;
    }

    public CreditInfo insertCreditApp(Credit_Application__c creditAppInfo, String dealerId) {
        Dom.Document doc = new Dom.Document();
        Dom.XmlNode envelope = doc.createRootElement(CDKSoapConstants.TAG.ENVELOPE, CDKSoapConstants.SOAP_NS, 'S');
        envelope.setNamespace('pip', CDKSoapConstants.PIP.WEB_CREDIT);

        Dom.XmlNode body = envelope.addChildElement(CDKSoapConstants.TAG.BODY, CDKSoapConstants.SOAP_NS, 'S');
        Dom.XmlNode processTag = body.addChildElement(CDKSoapConstants.TAG.PROCESS_CREDIT_APP, CDKSoapConstants.PIP.WEB_CREDIT, 'pip');

        Dom.XmlNode authTag = processTag.addChildElement(CDKSoapConstants.TAG.AUTH_TOKEN, null, null);
        authTag.addChildElement(CDKSoapConstants.TAG.PASSWORD, null, null)
            .addTextNode(SETTINGS.Password__c);
        authTag.addChildElement(CDKSoapConstants.TAG.USERNAME, null, null)
            .addTextNode(SETTINGS.UserID__c);

        Dom.XmlNode reqTag = processTag.addChildElement(CDKSoapConstants.TAG.REQ, null, null);
        reqTag.addChildElement('bureaus', null, null)
            .addTextNode('TU');
        
        Dom.XmlNode clientInfo = reqTag.addChildElement('clientInfo', null, null);
        clientInfo.addChildElement('dealerId', null, null)
            .addTextNode(dealerId);
        clientInfo.addChildElement('userId', null, null)
            .addTextNode(SETTINGS.UserID__c);
        
        Dom.XmlNode creditApp = reqTag.addChildElement('creditApp', null, null);
        Dom.XmlNode detail = creditApp.addChildElement('Detail', CDKSoapConstants.STAR_NS, 'star');
        Dom.XmlNode individualApplicant = detail.addChildElement('IndividualApplicant', CDKSoapConstants.STAR_NS, 'star');
       
        Dom.XmlNode personName = individualApplicant.addChildElement('PersonName', CDKSoapConstants.STAR_NS, 'star');
        personName.addChildElement('GivenName', CDKSoapConstants.STAR_NS, 'star')
            .addTextNode(creditAppInfo.First_Name__c);
        personName.addChildElement('FamilyName', CDKSoapConstants.STAR_NS, 'star')
            .addTextNode(creditAppInfo.Last_Name__c);
        
        Dom.XmlNode creditVehicle = individualApplicant.addChildElement('CreditVehicle', CDKSoapConstants.STAR_NS, 'star');
        creditVehicle.addChildElement('VehicleStock', CDKSoapConstants.STAR_NS, 'star')
            .addTextNode(creditAppInfo.Stock_Number__c);

        Dom.XmlNode address = individualApplicant.addChildElement('Address', CDKSoapConstants.STAR_NS, 'star');
        address.setAttribute('qualifier', 'HomeAddress');
        address.addChildElement('AddressLine', CDKSoapConstants.STAR_NS, 'star')
            .addTextNode(creditAppInfo.Street__c);
        address.addChildElement('City', CDKSoapConstants.STAR_NS, 'star')
            .addTextNode(creditAppInfo.City__c);
        address.addChildElement('StateOrProvince', CDKSoapConstants.STAR_NS, 'star')
            .addTextNode(creditAppInfo.State__c);
        address.addChildElement('Country', CDKSoapConstants.STAR_NS, 'star')
            .addTextNode('US');
        address.addChildElement('PostalCode', CDKSoapConstants.STAR_NS, 'star')
            .addTextNode(creditAppInfo.Zip__c);

        reqTag.addChildElement('entryPoint', null, null).addTextNode('APPLICATION');
        System.debug(doc.toXmlString());
        HttpResponse response = CDKApi.doPost(SETTINGS.API_URL_2__c, SETTINGS.PIP_Web_Credit__c, doc);

        LOGGER.addLog(String.format(
            CDKSoapConstants.LOGGER_MSG_TEMPLATE.RESPONDED_WITH_STATUS_ONLY, 
            new List<String>{ response.getStatus() }
        ), new List<Attachment> {
            new Attachment(
                Name = 'Request',
                ContentType = HTTPConstants.MediaType.TEXT_PLAIN_VALUE,
                Body = Blob.valueOf(doc.toXmlString()) 
            ),
            new Attachment(
                Name = 'Response',
                ContentType = HTTPConstants.MediaType.TEXT_PLAIN_VALUE,
                Body = Blob.valueOf(response.getBody()) 
            )
        });
        
        return parseInsertResponse(response.getBodyDocument());
    }

    public CreditInfo parseInsertResponse(Dom.Document doc) {
        CreditInfo cInfo = new CreditInfo();
        Dom.XmlNode envelopeResult = doc.getRootElement();
        Dom.XmlNode bodyResult = envelopeResult.getChildElement(CDKSoapConstants.TAG.BODY, CDKSoapConstants.SOAP_NS);
        Dom.XmlNode fault = bodyResult.getChildElement(CDKSoapConstants.TAG.FAULT, CDKSoapConstants.SOAP_NS);
        Dom.XmlNode response = bodyResult.getChildElement(CDKSoapConstants.TAG.PROCESS_CREDIT_APP_RESPONSE, CDKSoapConstants.PIP.WEB_CREDIT);
        
        //existing Fault tag means there was an exception
        if (fault != null) {
            cInfo.success = false;
            cInfo.faultcode = fault.getChildElement('faultcode', null).getText();
            cInfo.faultstring = fault.getChildElement('faultstring', null).getText();
            Dom.XmlNode detail = fault.getChildElement('detail', null);
            Dom.XmlNode creditPipException = detail.getChildElement('WebCreditPIPException', CDKSoapConstants.PIP.WEB_CREDIT);
            cInfo.message = creditPipException.getChildElement('message', null).getText();
            return cInfo;
        }

        Dom.XmlNode returnTag = response.getChildElement('return', null);
        Dom.XmlNode typeTag= returnTag.getChildElement('type', null);
        if (typeTag.getText().equals('error')) {
            cInfo.success = false;
            cInfo.message = returnTag.getChildElement('message', null).getText();
            return cInfo;
        } 
        Dom.XmlNode creditCheck = returnTag.getChildElement('creditCheck', null);
        cInfo.creditCheckEqScore = creditCheck.getChildElement('eqScore', null).getText();
        cInfo.creditCheckExScore = creditCheck.getChildElement('exScore', null).getText();
        cInfo.creditCheckReportURL = creditCheck.getChildElement('reportURL', null).getText();
        cInfo.creditCheckStatus = creditCheck.getChildElement('status', null).getText();
        cInfo.creditCheckTuScore = creditCheck.getChildElement('tuScore', null).getText();
        cInfo.message = returnTag.getChildElement('message', null).getText();
        cInfo.status = returnTag.getChildElement('status', null).getText();
        cInfo.type = returnTag.getChildElement('type', null).getText();
        cInfo.wcURL = returnTag.getChildElement('wcURL', null).getText();
        cInfo.success = cInfo.type == 'success';

        return cInfo;
    }

    public class CreditInfo {
        public Boolean success;
        public String faultcode;
        public String faultstring;
        public String message;
        public String creditCheckEqScore;
        public String creditCheckExScore;
        public String creditCheckTuScore;
        public String creditCheckReportURL;
        public String creditCheckStatus;
        public String status;
        public String type;
        public String wcURL;
    }
}
