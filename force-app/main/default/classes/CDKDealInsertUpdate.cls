public without sharing class CDKDealInsertUpdate {
    private ApexLogger LOGGER;
    private static final CDK_Integration__c SETTINGS;

    static { SETTINGS = CDK_Integration__c.getOrgDefaults(); }

    public CDKDealInsertUpdate(ApexLogger loggerInstance) {
        this.LOGGER = loggerInstance;
    }

    public List<DealInfo> insertDeal(Opportunity opp, Matching_Vehicles__c cdkVehicle, String customerNumber, String dealerId) {
        Dom.Document doc = new Dom.Document();
        Dom.XmlNode envelope = doc.createRootElement(CDKSoapConstants.TAG.ENVELOPE, CDKSoapConstants.SOAP_NS, 'S');
        envelope.setNamespace('pip', CDKSoapConstants.PIP.DEAL);

        Dom.XmlNode body = envelope.addChildElement(CDKSoapConstants.TAG.BODY, CDKSoapConstants.SOAP_NS, 'S');
        Dom.XmlNode insertTag = body.addChildElement(CDKSoapConstants.TAG.DEAL_INSERT, CDKSoapConstants.PIP.DEAL, 'ns2');

        Dom.XmlNode arg0 = insertTag.addChildElement(CDKSoapConstants.TAG.ARGUMENT_0, null, null);
        arg0.addChildElement(CDKSoapConstants.TAG.PASSWORD, null, null)
            .addTextNode(SETTINGS.Password__c);
        arg0.addChildElement(CDKSoapConstants.TAG.USERNAME, null, null)
            .addTextNode(SETTINGS.UserID__c);

        Dom.XmlNode arg1 = insertTag.addChildElement(CDKSoapConstants.TAG.ARGUMENT_1, null, null);
        arg1.addChildElement(CDKSoapConstants.TAG.ID, null, null)
            .addTextNode(dealerId);

        Dom.XmlNode arg2 = insertTag.addChildElement(CDKSoapConstants.TAG.ARGUMENT_2, null, null);
        arg2.addChildElement('dealType', null, null)
            .addTextNode('Deal'); //---val

        Dom.XmlNode customers1 = arg2.addChildElement('customers', null, null);
        customers1.addChildElement('cobuyer', null, null).addTextNode('false');

        Dom.XmlNode partyId1 = customers1.addChildElement('partyId', null, null);
        partyId1.addChildElement('value', null, null)
                .addTextNode(customerNumber);

        Dom.XmlNode financing = arg2.addChildElement('financing', null, null);
        financing.addChildElement('financeType', null, null)
            .addTextNode('P');//opp.Type == 'Lease' ? 'L' : 'P' ---val

        Dom.XmlNode vehicle = arg2.addChildElement('vehicle', null, null);
        if (String.isNotBlank(cdkVehicle.Inventory__r.Stock_Number__c)) {
            vehicle.addChildElement('vehicleStock', null, null).addTextNode(cdkVehicle.Inventory__r.Stock_Number__c);
        } else {
            vehicle.addChildElement('vehicleStock', null, null).addTextNode('');
        }

        HttpResponse response = CDKApi.doPost(SETTINGS.PIP_Deal_Insert_Update__c, doc);

        LOGGER.addLog(String.format(
            CDKSoapConstants.LOGGER_MSG_TEMPLATE.RESPONDED_WITH_STATUS_ONLY, 
            new List<String>{ response.getStatus() }
        ), new List<Attachment> {
            new Attachment(
                Name = 'Request',
                ContentType = HTTPConstants.MediaType.TEXT_PLAIN_VALUE,
                Body = Blob.valueOf(doc.toXmlString()) 
            ),
            new Attachment(
                Name = 'Response',
                ContentType = HTTPConstants.MediaType.TEXT_PLAIN_VALUE,
                Body = Blob.valueOf(response.getBody()) 
            )
        });
        return parseInsertResponse(response.getBodyDocument());
    }

    public List<DealInfo> parseInsertResponse(Dom.Document doc) {
        Dom.XmlNode envelopeResult = doc.getRootElement();
        Dom.XmlNode bodyResult = envelopeResult.getChildElement(CDKSoapConstants.TAG.BODY, CDKSoapConstants.SOAP_NS);
        Dom.XmlNode Response = bodyResult.getChildElement(CDKSoapConstants.TAG.DEAL_INSERT_RESPONSE, CDKSoapConstants.PIP.DEAL);
        Dom.XmlNode returnResult = Response.getChildElement('return', null);
        Dom.XmlNode code = returnResult.getChildElement('code', null);

        List<DealInfo> dealInfos = new List<DealInfo>();
        if (code != null && code.getText() == 'success') {
            Dom.XmlNode deal = returnResult.getChildElement('deal', null);
            Dom.XmlNode documentId = deal.getChildElement('documentId', null);
            Dom.XmlNode checksum = deal.getChildElement('checksum', null);
            for (Dom.XmlNode child : deal.getChildElements()) {
                if (child.getName() == 'customers') {
                    Dom.XmlNode cobuyer = child.getChildElement('cobuyer', null);
                    DealInfo dInfo = new DealInfo();

                    Dom.XmlNode address = child.getChildElement('address', null);
                    Dom.XmlNode addressBase = address.getChildElement('addressBase', null);
                    Dom.XmlNode addressLines = addressBase.getChildElement('addressLines', null);
                    Dom.XmlNode city = addressBase.getChildElement('city', null);
                    Dom.XmlNode county = addressBase.getChildElement('county', null);
                    Dom.XmlNode postalCode = addressBase.getChildElement('postalCode', null);
                    Dom.XmlNode stateOrProvince = addressBase.getChildElement('stateOrProvince', null);
                    Dom.XmlNode contact = child.getChildElement('contact', null);
                    Dom.XmlNode telephoneNumber = contact.getChildElement('telephoneNumber', null);
                    Dom.XmlNode email = contact.getChildElement('email', null);
                    Dom.XmlNode name = child.getChildElement('name', null);
                    Dom.XmlNode firstName = name.getChildElement('firstName', null);
                    Dom.XmlNode fullName = name.getChildElement('fullname', null);
                    Dom.XmlNode lastName = name.getChildElement('lastName', null);
                    Dom.XmlNode nameType = name.getChildElement('nameType', null);
                    Dom.XmlNode middleName = name.getChildElement('middleName', null);
                    dInfo.checksum = checksum.getText();
                    dInfo.dealId = documentId.getText();
                    if (addressBase != null) {
                        dInfo.addressLines = (addressLines != null ? addressLines.getText() : '');
                        dInfo.city = (city != null ? city.getText() : '');
                        dInfo.stateOrProvince = (stateOrProvince != null ? stateOrProvince.getText() : '');
                        dInfo.postalCode = (postalCode != null ? postalCode.getText() : '');
                        dInfo.county = (county != null ? county.getText() : '');
                    }
                    if (name != null) {
                        dInfo.firstName = (firstName != null ? firstName.getText() : '');
                        dInfo.fullname = (fullName != null ? fullName.getText() : '');
                        dInfo.lastName = (lastName != null ? lastName.getText() : '');
                        dInfo.nameType = (nameType != null ? nameType.getText() : '');
                        if (dInfo.nameType != '' && dInfo.nameType == 'PR') {
                            dInfo.nameType = 'Person';
                        } else if (dInfo.nameType != '' && dInfo.nameType == 'BS') {
                            dInfo.nameType = 'Business';
                        }
                        dInfo.middleName = (middleName != null ? middleName.getText() : '');
                    }
                    dInfo.cobuyer = (cobuyer != null ? cobuyer.getText() : '');
                    if (contact != null) {
                        for (Dom.XmlNode childContact : contact.getChildElements()) {
                            Dom.XmlNode contactDesc = childContact.getChildElement('desc', null);
                            Dom.XmlNode contactValue = childContact.getChildElement('value', null);
                            if (childContact.getName() == 'email') {
                                if (contactDesc.getText() == 'Cellular') {
                                    dInfo.primaryEmail = contactValue.getText();
                                } else if (contactDesc.getText() == 'Home') {
                                    dInfo.homeEmail = contactValue.getText();
                                } else {
                                    dInfo.workEmail = contactValue.getText();
                                }
                            } else if (childContact.getName() == 'telephoneNumber') {
                                if (contactDesc.getText() == 'Cellular') {
                                    dInfo.primaryPhone = contactValue.getText();
                                } else if (contactDesc.getText() == 'Home') {
                                    dInfo.homePhone = contactValue.getText();
                                } else {
                                    dInfo.workPhone = contactValue.getText();
                                }
                            }
                        }
                    }
                    dealInfos.add(dInfo);
                }
            }
            return dealInfos;
        } else {
            Dom.XmlNode message = returnResult.getChildElement('message', null);
            DealInfo custInfo = new DealInfo();
            custInfo.error = CDKSoapConstants.PROCESS.DEAL_INSERT + ' error: ' + message.getText();
            dealInfos.add(custInfo);
            return dealInfos;
        }
    }

    public class DealInfo {
        public String dealId;
        public String checksum;
        public String error;
        public String firstName;
        public String fullname;
        public String county;
        public String lastName;
        public String nameType;
        public String addressLines;
        public String city;
        public String middleName;
        public String primaryEmail;
        public String homeEmail;
        public String workEmail;
        public String primaryPhone;
        public String workPhone;
        public String homePhone;
        public String postalCode;
        public String stateOrProvince;
        public String cobuyer;
    }
}