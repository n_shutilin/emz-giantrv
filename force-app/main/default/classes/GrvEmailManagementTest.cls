@IsTest
public class GrvEmailManagementTest {
    private static final GrvEmailManagement.FilterWrapper FILTER;

    static {
        FILTER = new GrvEmailManagement.FilterWrapper();
        FILTER.address = UserInfo.getUserEmail();
        FILTER.emailOption = 'incoming';
        FILTER.startDate = Datetime.now().addDays(-7).format('YYYY-MM-dd');
        FILTER.endDate =  Datetime.now().format('YYYY-MM-dd');
    }

    @TestSetup
    public static void makeData(){
        RecordType rt = [SELECT Id, Name FROM RecordType WHERE SobjectType='Account' AND IsActive = true LIMIT 1];

        Account acc = new Account(Name = 'TestAcc', RecordTypeId = rt.Id);
        insert acc;

        EmailMessage message = new EmailMessage(
            Subject = 'Test',
            HtmlBody = 'Test',
            RelatedToId = acc.Id,
            ToAddress = UserInfo.getUserEmail()
        );
        insert message;

        insert new EmailMessageRelation(
            EmailMessageId = message.Id,
            RelationAddress = UserInfo.getUserEmail(),
            RelationType = 'ToAddress'
        );
    }

    @IsTest
    public static void testGetUserOptions() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u1 = new User(
            Alias = 'newUser1', 
            Email = 'newuser1@testgrv.com',
            EmailEncodingKey = 'UTF-8', 
            LastName = 'Testing1', 
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US', 
            ProfileId = p.Id,
            TimeZoneSidKey = 'America/Los_Angeles', 
            UserName = 'newuser1@testgrv.com',
            Track_Email__c = true
        );
        User u2 = new User(
            Alias = 'newUser2', 
            Email = 'newuser2@testgrv.com',
            EmailEncodingKey = 'UTF-8', 
            LastName = 'Testing2', 
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US', 
            ProfileId = p.Id,
            TimeZoneSidKey = 'America/Los_Angeles', 
            UserName = 'newuser2@testgrv.com',
            Track_Email__c = true
        );
        insert u1;
        insert u2;
        insert new Email_Tracking_Member__c(Member__c = u1.Id, Manager__c = u2.Id);
        Test.startTest();
        List<GrvEmailManagement.OptionsWrapper> options;
        System.runAs(u2) {
            options = GrvEmailManagement.getUserOptions();
        }
        Test.stopTest();
        System.assert(options.size() == 2);
    }

    @IsTest
    public static void testGetMessages() {
        Test.startTest();
        List<GrvEmailManagement.MessageWrapper> messages = GrvEmailManagement.getMessages(FILTER, 1);
        Test.stopTest();
        System.assert(messages.size() == 1);
    }
   
    @IsTest
    public static void testGetNumberMessagesPages() {
        Test.startTest();
        Double pagesCount = GrvEmailManagement.getNumberMessagesPages(FILTER);
        Test.stopTest();
        System.assert(pagesCount == 1);
    }
   
    @IsTest
    public static void testDeleteOpener() {
        EmailMessage ems = [SELECT Id FROM EmailMessage LIMIT 1];
        Test.startTest();
        GrvEmailManagement.deleteOpener(ems.Id);
        Test.stopTest();
    }

    @IsTest
    public static void testDeleteMessages() {
        EmailMessage ems = [SELECT Id FROM EmailMessage LIMIT 1];
        Test.startTest();
        GrvEmailManagement.deleteMessages(new List<String> {ems.Id});
        Test.stopTest();
        List<EmailMessage> messages = [SELECT Id FROM EmailMessage];
        System.assertEquals(0, messages.size());
    }

    @IsTest
    static void sendEmail_Test() {
        GrvEmailManagement.SingleEmailMessageWrapper semw = new GrvEmailManagement.SingleEmailMessageWrapper();
        semw.fromAddress = 'test@test.test';
        semw.toAddresses = new List<String> {'test@test.test'};
        semw.ccAddresses = new List<String> {'test@test.test'};
        semw.bccAddresses = new List<String> {'test@test.test'};
        semw.subject = 'test@test.test';
        semw.body = 'test@test.test'; 

        Test.startTest();
        GrvEmailManagement.sendEmail(semw);
        Test.stopTest();
    }
 }