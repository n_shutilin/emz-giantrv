public without sharing class CDKCreditAppInsertComponent {
    private static final CDK_Integration__c SETTINGS;
    private static ApexLogger LOGGER_WEB_CREDIT_INSERT;
    static {
        LOGGER_WEB_CREDIT_INSERT = ApexLogger.create(CDKSoapConstants.PROCESS.WEB_CREDIT_INSERT);
    }

    @AuraEnabled
    public static CreditAppResponseWrapper createDraftCreditApplication(String recordId) {
        Opportunity opp = [
            SELECT Id, AccountId, CDK_Deal_Number__c, CDK_Deal_Inserted_Date__c, Dealer__c, CDK_Customer_Number__c
            FROM Opportunity
            WHERE Id = :recordId
        ];
        if (opp == null || String.isBlank(opp.AccountId)) {
            return new CreditAppResponseWrapper(false, System.Label.No_Opportunity_Account);
        }
        String dealerId = CDKSoapConstants.getDealerIdByDealer(opp.Dealer__c);
        if (String.isBlank(dealerId)) {
            return new CreditAppResponseWrapper(false, System.Label.Dealer_Id_Not_Defined);
        }
        Account customer = [
            SELECT Id, FirstName, LastName, PersonMobilePhone, PersonEmail, PersonBirthdate,
                PersonHomePhone, PersonOtherPhone, Account.PersonMailingStreet,
                Account.PersonMailingCity, Account.PersonMailingCountry, Account.PersonMailingPostalCode,
                Account.PersonMailingState, Business_Company__c,
                CDK_Customer_Colton__c, CDK_Customer_Downey__c, CDK_Customer_Montclair__c, CDK_Customer_Murrieta__c
            FROM Account 
            WHERE Id = :opp.AccountId 
        ];

        List<Matching_Vehicles__c> cdkVehicles = [
            SELECT Id, Inventory__r.Stock_Number__c 
            FROM Matching_Vehicles__c
            WHERE Opportunity__c = :opp.Id
            AND Send_to_CDK__c = true
            LIMIT 1
        ];
        if (cdkVehicles.isEmpty()) {
            return new CreditAppResponseWrapper(false, System.Label.Vehicle_For_Sending_Not_Found);
        }

        Credit_Application__c creditApp = createCreditApplicationFromAccount(customer);
        creditApp.Opportunity__c = opp.Id;
        creditApp.Stock_Number__c = cdkVehicles.get(0).Inventory__r?.Stock_Number__c;
        return new CreditAppResponseWrapper(creditApp);
        // CDKCreditInsert.CreditInfo creditInfo = creditInsert.insertCreditApp(creditApp);
        // if (!creditInfo.success) {
        //     return new CreditAppResponseWrapper(false, creditInfo.message);
        // }
        // opp.CDK_Credit_Inserted_Date__c = Datetime.now();
        // creditApp.WC_URL__c = creditInfo.wcURL;
        // insert creditApp;
        // update opp;
        // LOGGER_WEB_CREDIT_INSERT.save();
        // //change label
        // return new CreditAppResponseWrapper(true, System.Label.Deal_Successfully_Created);
    }

    @AuraEnabled
    public static CreditAppResponseWrapper sendCreditApplication(Id creditAppId){
        CDKCreditInsert creditInsert = new CDKCreditInsert(LOGGER_WEB_CREDIT_INSERT);
        Credit_Application__c creditApp = [
            SELECT Id, Birthdate__c, City__c, Co_buyer_Birthday__c, Co_buyer_City__c,
                Co_buyer_Company__c, Co_buyer_County__c, Co_buyer_DL_State__c, 
                Co_buyer_DL__c, Co_buyer_Email__c, Co_buyer_First_Name__c, Co_buyer_Home_Phone__c,
                Co_buyer_Last_Name__c, Co_buyer_Mobile_Phone__c, Co_buyer_SS__c,
                Co_buyer_State__c, Co_buyer_Street__c, Co_buyer_Zip__c, Company__c,
                County__c, DL_State__c, DL__c, Email__c, First_Name__c, Home_Phone__c,
                Last_Name__c, Mobile_Phone__c, Opportunity__c, SS__c, State__c,
                Stock_Number__c, Street__c, Zip__c
            FROM Credit_Application__c
            WHERE Id = :creditAppId
        ];
        List<Opportunity> oppList = [
            SELECT Id, Dealer__c 
            FROM Opportunity 
            WHERE Id = :creditApp.Opportunity__c
        ];
        String dealerId = CDKSoapConstants.getDealerIdByDealer(oppList.get(0).Dealer__c);
        CDKCreditInsert.CreditInfo creditInfo = creditInsert.insertCreditApp(creditApp, dealerId);
        if (!creditInfo.success) {
            return new CreditAppResponseWrapper(false, creditInfo.message);
        }
        oppList.get(0).CDK_Credit_Inserted_Date__c = Datetime.now();
        creditApp.WC_URL__c = creditInfo.wcURL;
        update creditApp;
        update oppList;
        return new CreditAppResponseWrapper(true, creditApp.WC_URL__c);
    }

    private static Credit_Application__c createCreditApplicationFromAccount(Account acc) {
        return new Credit_Application__c(
            Birthdate__c = acc.PersonBirthdate,
            City__c = acc.PersonMailingCity,
            // Co_buyer_Birthday__c
            // Co_buyer_City__c
            // Co_buyer_Company__c
            // Co_buyer_County__c
            // Co_buyer_DL_State__c
            // Co_buyer_DL__c
            // Co_buyer_Email__c
            // Co_buyer_First_Name__c
            // Co_buyer_Home_Phone__c
            // Co_buyer_Last_Name__c
            // Co_buyer_Mobile_Phone__c
            // Co_buyer_SS__c
            // Co_buyer_State__c
            // Co_buyer_Street__c
            // Co_buyer_Zip__c
            Company__c = acc.Business_Company__c,
            County__c = acc.PersonMailingCountry,
            // DL_State__c
            // DL__c
            Email__c = acc.PersonEmail,
            First_Name__c = acc.FirstName,
            Home_Phone__c = acc.PersonHomePhone,
            Last_Name__c = acc.LastName,
            Mobile_Phone__c = acc.PersonMobilePhone,
            // Opportunity__c
            // SS__c
            State__c = acc.PersonMailingState,
            // Stock_Number__c = stockNumber, //pass
            Street__c = acc.PersonMailingStreet,
            Zip__c = acc.PersonMailingPostalCode
        );
    }

    public class CreditAppResponseWrapper {
        @AuraEnabled
        public Boolean success {get; set;}
        @AuraEnabled
        public String message {get; set;}
        @AuraEnabled
        public Credit_Application__c creditApp {get; set;}
        

        public CreditAppResponseWrapper() {}
        public CreditAppResponseWrapper(Boolean success, String message) {
            this.success = success;
            this.message = message;
        }
        public CreditAppResponseWrapper(Credit_Application__c creditApp) {
            this.success = true;
            this.creditApp = creditApp;
        }
    }
}
