public without sharing class CDKDealInsertComponent {
    private static final CDK_Integration__c SETTINGS;
    private static ApexLogger LOGGER_INSERT_CUSTOMER;
    private static ApexLogger LOGGER_INSERT_DEAL;
    static {
        LOGGER_INSERT_CUSTOMER = ApexLogger.create(CDKSoapConstants.PROCESS.CUSTOMER_INSERT);
        LOGGER_INSERT_DEAL = ApexLogger.create(CDKSoapConstants.PROCESS.DEAL_INSERT);
    }

    @AuraEnabled(cacheable=true)
    public static Opportunity retrieveDeal(String recordId) {
        Opportunity opp = [
            SELECT Id, CDK_Deal_Number__c, CDK_Deal_Inserted_Date__c, CDK_Credit_Inserted_Date__c, Account.PersonMailingStreet,
                Account.PersonMailingCity, Account.PersonMailingCountry, Account.PersonMailingPostalCode,
                Account.PersonMailingState, Dealer__c
            FROM Opportunity
            WHERE Id = :recordId
            LIMIT 1
        ];

        return opp;
    }

    @AuraEnabled
    public static DealInsertResponseWrapper sendDeal(String recordId) {
        CDKDealInsertUpdate dealInsertUpdate = new CDKDealInsertUpdate(LOGGER_INSERT_DEAL);
        CDKCustomerInsertUpdate customerInsertUpdate = new CDKCustomerInsertUpdate(LOGGER_INSERT_CUSTOMER);

        Opportunity opp = [
            SELECT Id, AccountId, CDK_Deal_Number__c, CDK_Deal_Inserted_Date__c, Dealer__c, CDK_Customer_Number__c
            FROM Opportunity
            WHERE Id = :recordId
        ];
        if (opp == null || String.isBlank(opp.AccountId)) {
            return new DealInsertResponseWrapper(false, System.Label.No_Opportunity_Account);
        }
        String dealerId = CDKSoapConstants.getDealerIdByDealer(opp.Dealer__c);
        if (String.isBlank(dealerId)) {
            return new DealInsertResponseWrapper(false, System.Label.Dealer_Id_Not_Defined);
        }
        Account customer = [
            SELECT Id, FirstName, LastName, PersonMobilePhone, 
                PersonHomePhone, PersonOtherPhone,  
                CDK_Customer_Colton__c, CDK_Customer_Downey__c, CDK_Customer_Montclair__c, CDK_Customer_Murrieta__c
            FROM Account 
            WHERE Id = :opp.AccountId 
        ];

        List<Matching_Vehicles__c> cdkVehicles = [
            SELECT Id, Inventory__r.Stock_Number__c 
            FROM Matching_Vehicles__c
            WHERE Opportunity__c = :opp.Id
            AND Send_to_CDK__c = true
            LIMIT 1
        ];
        if (cdkVehicles.isEmpty()) {
            return new DealInsertResponseWrapper(false, System.Label.Vehicle_For_Sending_Not_Found);
        }

        if (String.isBlank(opp.CDK_Customer_Number__c)) {
            String searchName = String.isNotBlank(customer.FirstName) 
                ? customer.FirstName + ',' + customer.LastName
                : customer.LastName;
            String searchPhone;
            if (String.isNotEmpty(customer.PersonMobilePhone)) {
                searchPhone = customer.PersonMobilePhone;
            } else if (String.isNotEmpty(customer.PersonHomePhone)) {
                searchPhone = customer.PersonHomePhone;
            } else if (String.isNotEmpty(customer.PersonOtherPhone)) {
                searchPhone = customer.PersonOtherPhone;
            }
            
            if (String.isBlank(searchName)) {
                return null;
            }
    
            List<String> searchResults = CDKCustomerSearch.executeSearchBulk(dealerId, searchName, searchPhone);
            if (searchResults != null && !searchResults.isEmpty()) {
                customer.put(
                    CDKSoapConstants.getAccountFieldNameByDealer(opp.Dealer__c), 
                    searchResults.get(0)
                );
            } else {
                CDKCustomerInsertUpdate.CustomerInfo custInfo = customerInsertUpdate.insertUpdateCustomer(dealerId, customer.Id);              
                if (custInfo != null && String.isNotBlank(custInfo.custId)) {
                    customer.put(
                        CDKSoapConstants.getAccountFieldNameByDealer(opp.Dealer__c), 
                        custInfo.custId.remove(CDKSoapConstants.PREFIX_ID)
                    );
                } else {
                    return new DealInsertResponseWrapper(false, custInfo.error);
                }
            }
        } 
        List<CDKDealInsertUpdate.DealInfo> deals = dealInsertUpdate.insertDeal(
            opp, 
            cdkVehicles.get(0), 
            (String)customer.get(CDKSoapConstants.getAccountFieldNameByDealer(opp.Dealer__c)),
            dealerId
        );
        if (deals != null && String.isNotBlank(deals.get(0).dealId)) {
            opp.CDK_Deal_Number__c = deals.get(0).dealId;
            opp.CDK_Deal_Inserted_Date__c = Datetime.now();
            update opp;
        } else {
            return new DealInsertResponseWrapper(false, deals.get(0).error);
        }
        update customer;
        
        LOGGER_INSERT_CUSTOMER.save();
        LOGGER_INSERT_DEAL.save();
        return new DealInsertResponseWrapper(true, System.Label.Deal_Successfully_Created);
    }

    public class DealInsertResponseWrapper {
        @AuraEnabled
        public Boolean success {get; set;}
        @AuraEnabled
        public String message {get; set;}

        public DealInsertResponseWrapper() {}
        public DealInsertResponseWrapper(Boolean success, String message) {
            this.success = success;
            this.message = message;
        }
    }
}