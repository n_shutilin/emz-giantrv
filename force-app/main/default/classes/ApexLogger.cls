public without sharing class ApexLogger {
    private static final String TIMESTAMP_FORMAT = 'MM-dd-yyyy HH:mm:ss';
    private static final Reflector REFLECTOR = new Reflector(ApexLogger.class.getName());
    private static final BlankException BLANK_EXCEPTION = new BlankException();
    private String context = '';
    private List<Apex_Log__c> logs = new List<Apex_Log__c>();
    private Map<Integer, List<Attachment>> logIndexToAttachments = new Map<Integer, List<Attachment>>();

    private ApexLogger() {}
    private ApexLogger(String context) {
        this.context = context + ': ';
    }

    public static ApexLogger create() {
        return new ApexLogger();
    }
   
    public static ApexLogger create(String context) {
        return new ApexLogger(context);
    }

    public void addLog(String message) {
        this.logs.add(this.getApexLog(message, BLANK_EXCEPTION));
    }

    public void addLog(String message, List<Attachment> attachments) {
        this.logIndexToAttachments.put(this.logs.size(), attachments);
        this.logs.add(this.getApexLog(message, BLANK_EXCEPTION));
    }

    public void addLog(Exception thrownException) {
        this.logs.add(this.getApexLog(null, thrownException));
    } 

    public void debug(String message) {
        Apex_Log__c log = this.getApexLog(message, BLANK_EXCEPTION);
        String debugMessage = this.buildSystemDebugMessage(log);
        System.debug(LoggingLevel.DEBUG, debugMessage);
    }

    public void log(String message) {
        this.log(message, BLANK_EXCEPTION, null);
    }

    public void log(Exception thrownException) {
        this.log(null, thrownException, null);
    }

    public void log(String message, List<Attachment> attachments) {
        this.log(message, BLANK_EXCEPTION, attachments);
    }

    public void save() {
        if (!logIndexToAttachments.isEmpty()) {
            insert this.logs;
            List<Attachment> attachmentsToInsert = new List<Attachment>();
            for (Integer i = 0; i<this.logs.size(); i++) {
                if (logIndexToAttachments.containsKey(i)) {
                    for (Attachment att : logIndexToAttachments.get(i)) {
                        att.ParentId = this.logs.get(i).Id;
                        attachmentsToInsert.add(att);
                    }
                }
            }
            if (!attachmentsToInsert.isEmpty()) insert attachmentsToInsert;
        } else if (!this.logs.isEmpty()) { 
            insert this.logs; 
            this.logs.clear();
        }
    }

    private void log(String message, Exception thrownException, List<Attachment> attachments) {
        Apex_Log__c log = this.getApexLog(message, thrownException);
        Id logId = this.save(log);
        if (attachments != null && !attachments.isEmpty()) {
            for (Attachment att : attachments) {
                att.ParentId = logId;
            }
            insert attachments;
        }
    }

    private Id save(Apex_Log__c log) {
        insert log;
        return log.Id;
	}

    private Apex_Log__c getApexLog(String message, Exception thrownException) {
        Caller caller = REFLECTOR.getCaller();
        Datetime now = System.now();
        String className = caller.className;
        String methodName = caller.methodName;

        return new Apex_Log__c(
			Class__c = className,
			Method__c = methodName,
			Message__c = this.buildLogMessage(message, thrownException),
            Limits_Info__c = this.buildLimitsMessage(),
            Exception__c = this.buildExceptionMessage(thrownException),
            Exception_Thrown__c = thrownException != BLANK_EXCEPTION,
            Timestamp__c = now,
            Time__c = now.format(TIMESTAMP_FORMAT)
		);
    }

    private String buildSystemDebugMessage(Apex_Log__c log) {
		String message = String.format(' {0}.{1}: {2}', new Object[] { log.Class__c, log.Method__c, log.Message__c });
		return message + '\n';
	}
	
    private String buildLogMessage(String message, Exception thrownException) {
		if (String.isBlank(message)) {
			message = 'Exception thrown: ' + thrownException.getTypeName();
		}
		return this.context + message;
	}

    private String buildExceptionMessage(Exception thrownException) {
		String message = '';
		while (thrownException != BLANK_EXCEPTION && thrownException != null) {
			message += thrownException.getTypeName() + ': ' + thrownException.getMessage() + '\n';
			message += thrownException.getStackTraceString() + '\n\n';
			thrownException = thrownException.getCause();
		}
		return message;
	}

    private String buildLimitsMessage() {
        return String.format(
            'Current context limits usage:\n' +
            'CPU Time: {0} / {1} ms\nDB Time: {2} / {3} ms\nHeap: {4} / {5} bytes\n' +
            'DML queries: {6} / {7}\nDML rows: {8} / {9}\n' +
            'SOQL queries: {10} / {11}\nSOSL queries: {12} / {13}',
            new Object[] {
                Limits.getCpuTime(), Limits.getLimitCpuTime(),
                Limits.getDatabaseTime(), Limits.getLimitDatabaseTime(),
                Limits.getHeapSize(), Limits.getLimitHeapSize(),
                Limits.getDmlStatements(), Limits.getLimitDmlStatements(),
                Limits.getDmlRows(), Limits.getLimitDmlRows(),
                Limits.getQueries(), Limits.getLimitQueries(),
                Limits.getSoslQueries(), Limits.getLimitSoslQueries()
            }
        );
    }

    private class BlankException extends Exception { }

    private class Reflector {
        private final Caller anonymousCaller = new Caller('AnonymousApex', 'AnonymousApex');
		private final Pattern callerPattern = Pattern.compile('\\.(.+):');
		private final String ignoredClass;
		
		public Reflector(String ignoredClass) {
			this.ignoredClass = ignoredClass;
		}

		public Caller getCaller() {
			final String stacktrace = new NullPointerException().getStackTraceString();
			final Matcher matcher = callerPattern.matcher(stacktrace);

			while (matcher.find()) {
				final List<String> haystack = matcher.group(1).split('\\.');
				if (haystack[0] == ignoredClass) {
					continue;
				}
				Caller caller = new Caller();
				caller.methodName = haystack.remove(haystack.size() - 1);
				caller.className = String.join(haystack, '.');
                return caller;
			}
            return anonymousCaller;
		}
	}
	
	private class Caller {
		public String className { get; set; }
		public String methodName { get; set; }
        public Caller() { }
        public Caller(String className, String methodName) {
            this.className = className;
            this.methodName = methodName;
        }
	}
}