public class GrvEmailMessageTriggerHelper {
    public static void insertEmailMessageOpener(Map<Id, EmailMessage> newMap) {
        List<EmailMessageRelation> relations = [
            SELECT EmailMessageId, RelationAddress, EmailMessage.Headers
            FROM EmailMessageRelation
            WHERE EmailMessageId IN :newMap.keySet()
            AND (
                RelationType = 'ToAddress'
                OR RelationType = 'CcAddress'
                OR RelationType = 'BccAddress'
            )
        ];

        if (!relations.isEmpty()) {
            List<Email_Message_Opener__c> openers = new List<Email_Message_Opener__c>();

            for (EmailMessageRelation rel : relations) {
                Email_Message_Opener__c opener = new Email_Message_Opener__c(
                    Email_Message_Id__c = rel.EmailMessageId,
                    Email__c = rel.RelationAddress,
                    Microsoft_Id__c = rel.EmailMessage.Headers
                );

                openers.add(opener);
            }

            insert openers;
        }
    }
}