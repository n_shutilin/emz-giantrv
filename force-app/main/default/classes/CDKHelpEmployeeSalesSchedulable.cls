global without sharing class CDKHelpEmployeeSalesSchedulable implements Schedulable {
    private static final ApexLogger LOGGER;
    private static final CDK_Integration__c SETTINGS;

    static {
        LOGGER = ApexLogger.create(CDKSoapConstants.PROCESS.HELP_EMPLOYEE_SALES_EXTRACT);
        SETTINGS = CDK_Integration__c.getOrgDefaults();
    }

    global void execute(SchedulableContext sc) {
        syncEmployees();
        this.rescheduleJob(sc);
    }
    //system.schedule('CDK Empl Sales', '0 35 * * * ?', new CDKHelpEmployeeSalesSchedulable());
    @future(callout=true)
    global static void syncEmployees() {
        List<CDK_Employee__c> employees = new List<CDK_Employee__c>();
        String deltaDate = Date.today().format();

        for (String dealerId : CDKSoapConstants.getListOfDealers()) {
            try {
                HttpResponse response = CDKApi.doPost(
                    SETTINGS.PIP_Help_Employee_Extract__c, 
                    new Map<String, String>{
                        CDKSoapConstants.PARAM.DEALER_ID => dealerId, 
                        CDKSoapConstants.PARAM.QUERY_ID => CDKSoapConstants.QUERY.HELP_EMPLOYEE_DELTA_SALES_EXTRACT, 
                        CDKSoapConstants.PARAM.DELTA_DATE => deltaDate
                    }
                );
        
                if (response.getStatusCode() != HTTPConstants.Status.OK) {
                    LOGGER.addLog(String.format(
                        CDKSoapConstants.LOGGER_MSG_TEMPLATE.RESPONDED_WITH_STATUS,
                        new List<String>{ response.getStatus(), deltaDate, dealerId }
                    ));
                    continue;
                }
                
                List<CDK_Employee__c> employeesToInsert = CDKHelpEmployeeParser.parse(response.getBodyDocument(), dealerId, CDKSoapConstants.EMPLOYEE_TYPE_SALES);
                if (employeesToInsert == null || employeesToInsert.isEmpty()) {
                    LOGGER.addLog(String.format(
                        CDKSoapConstants.LOGGER_MSG_TEMPLATE.LIST_UPSERTED_EMPTY,
                        new List<String>{ dealerId }
                    ));
                    continue;
                }
                employees.addAll(employeesToInsert);
            } catch (Exception e) {
                LOGGER.addLog(e);
            }
        }
        upsertEmployees(employees);
        LOGGER.save();
    }

    public static void upsertEmployees(List<CDK_Employee__c> employeesToInsert) {
        if (employeesToInsert == null || employeesToInsert.isEmpty()) return;

        Database.UpsertResult[] results = Database.upsert(employeesToInsert, CDK_Employee__c.fields.UniqueId__c, false);
        LOGGER.addLog(String.format(
            CDKSoapConstants.LOGGER_MSG_TEMPLATE.INSERTED_UPDATED, 
            CDKSoapConstants.countUpsertResults(results)
        ));
    }

    global void rescheduleJob(SchedulableContext sc) {
        Datetime nextScheduleTime = Datetime.newInstance(
            Date.today().addDays(1).year(), 
            Date.today().addDays(1).month(), 
            Date.today().addDays(1).day(), 
            22, 5, 0
        );
        
        String hour = String.valueOf(nextScheduleTime.hour());
        String minutes = String.valueOf(nextScheduleTime.minute());
        String day = String.valueOf(nextScheduleTime.day());
        String cronValue = '0 ' + minutes + ' ' + hour + ' * * ?';
        String jobName = CDKSoapConstants.PROCESS.HELP_EMPLOYEE_SALES_EXTRACT + ': ' + nextScheduleTime.format('hh:mm');

        CDKHelpEmployeeSalesSchedulable nextJob = new CDKHelpEmployeeSalesSchedulable();
        System.schedule(jobName, cronValue, nextJob);

        System.abortJob(sc.getTriggerId());
    }
}