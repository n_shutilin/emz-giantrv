public without sharing class CDKWeOweParser {
    private static final CDK_Integration__c SETTINGS;
    private static final String EXTRACT_WE_OWE_NAMESPACE;
    private static final String DEFAULT_NODE_CHILD = 'WeOwe';

    static {
        SETTINGS = CDK_Integration__c.getOrgDefaults();
        EXTRACT_WE_OWE_NAMESPACE = SETTINGS.API_URL__c + SETTINGS.PIP_We_Owe_Extract__c;
    }

    public static List<CDK_We_Owe__c> parse(Dom.Document doc, String dealerId) {
        Map<String, Schema.SObjectField> weOweFields = Schema.SObjectType.CDK_We_Owe__c.fields.getMap();

        List<CDK_We_Owe__c> weOwes = new List<CDK_We_Owe__c>();
        for (Dom.XmlNode child: doc.getRootElement().getChildElements()) {
            if (child.getNodeType() == DOM.XmlNodeType.ELEMENT && child.getName() == DEFAULT_NODE_CHILD) {
                CDK_We_Owe__c weOwe = new CDK_We_Owe__c();
                CDKParser.setFields(weOwe, weOweFields, WE_OWE_PARSE_MAP, child, EXTRACT_WE_OWE_NAMESPACE);
                weOwe.DealerId__c = dealerId;
                weOwe.UniqueId__c = dealerId + weOwe.HostItemID__c;
                weOwes.add(weOwe);
            }
        }

        if (weOwes.isEmpty()) {
            return null;
        }

        return weOwes;
    }

    public static final Map<String, String> WE_OWE_PARSE_MAP = new Map<String, String> {
        'CostAmount'            => 'CostAmount__c',
        'DealNo'                => 'DealNo__c',
        'ErrorLevel'            => 'ErrorLevel__c',
        'ErrorMessage'          => 'ErrorMessage__c',
        'FrontBackFlag'         => 'FrontBackFlag__c',
        'HardSoftFlag'          => 'HardSoftFlag__c',
        'HostItemID'            => 'HostItemID__c',
        'ResidualAmount'        => 'ResidualAmount__c',
        'ResidualTableAmount'   => 'ResidualTableAmount__c',
        'ResidualTableFlag'     => 'ResidualTableFlag__c',
        'SaleAmount'            => 'SaleAmount__c', 
        'SatisfiedBy'           => 'SatisfiedBy__c',
        'SatisfiedDate'         => 'SatisfiedDate__c',
        'WeOweCode'             => 'WeOweCode__c',
        'WeOweDesc'             => 'WeOweDesc__c'
    };
}