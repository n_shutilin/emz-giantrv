@IsTest
public without sharing class CDKCustomerSearchTest {
    @TestSetup
    public static void makeData(){
        CDKTestDataFactory.initSettings();
    }

    @IsTest
    public static void testExecuteSearchBulk() {
        Test.setMock(HttpCalloutMock.class, CDKTestDataFactory.getApiMock(CDKTestDataFactory.MOCK.CUSTOMER_SEARCH));
        Test.startTest();
        List<String> customerNumber = CDKCustomerSearch.executeSearchBulk(CDKTestDataFactory.getDealerId(), 'searchName', 'searchPhone');
        Test.stopTest();
        System.assertEquals(CDKTestDataFactory.CUSTOMER_NUMBER, customerNumber?.get(0));
    }

}