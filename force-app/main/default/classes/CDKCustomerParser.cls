public without sharing class CDKCustomerParser {
    private static final CDK_Integration__c SETTINGS;
    private static final String EXTRACT_CUSTOMER_NAMESPACE;
    private static final String DEFAULT_NODE_CHILD = 'Customer';

    static {
        SETTINGS = CDK_Integration__c.getOrgDefaults();
        EXTRACT_CUSTOMER_NAMESPACE = CDKSoapConstants.NAMESPACE.CUSTOMER;
    }

    public static List<CDK_Customer__c> parse(Dom.Document doc, String dealerId) {
        Map<String, Schema.SObjectField> customerFields = Schema.SObjectType.CDK_Customer__c.fields.getMap();

        List<CDK_Customer__c> customers = new List<CDK_Customer__c>();
        for (Dom.XmlNode child: doc.getRootElement().getChildElements()) {
            if (child.getNodeType() == DOM.XmlNodeType.ELEMENT && child.getName() == DEFAULT_NODE_CHILD) {
                CDK_Customer__c customer = new CDK_Customer__c();
                CDKParser.setFields(customer, customerFields, CUSTOMER_PARSE_MAP, child, EXTRACT_CUSTOMER_NAMESPACE);
                customer.DealerId__c = dealerId;
                customer.UniqueId__c = dealerId + customer.CustNo__c;
                customers.add(customer);
            }
        }

        if (customers.isEmpty()) {
            return null;
        }

        return customers;
    }

    public static final Map<String, String> CUSTOMER_PARSE_MAP = new Map<String, String>{
        'Address'                   => 'Address__c',
        'AddressSecondLine'         => 'AddressSecondLine__c',
        'Birthdate'                 => 'Birthdate__c',
        'BlockEMail'                => 'BlockEMail__c',
        'BlockMail'                 => 'BlockMail__c',
        'BlockPhone'                => 'BlockPhone__c',
        'BusinessPhone'             => 'BusinessPhone__c',
        'BusinessPhoneExt'          => 'BusinessPhoneExt__c',
        'Cellular'                  => 'Cellular__c',
        'City'                      => 'City__c',
        'Comment'                   => 'Comment__c',
        'CommentDate'               => 'CommentDate__c',
        'ContactMethod'             => 'ContactMethod__c',
        'Country'                   => 'Country__c',
        'County'                    => 'County__c',
        'CreditLimit'               => 'CreditLimit__c',
        'CurrentDue'                => 'CurrentDue__c',
        'CustNo'                    => 'CustNo__c',
        'DateAdded'                 => 'DateAdded__c',
        'DriverLicenseExpDate'      => 'DriverLicenseExpDate__c',
        'DriverLicenseStOrProv'     => 'DriverLicenseStOrProv__c',
        'Email'                     => 'Email__c',
        'Email2'                    => 'Email2__c',
        'Email3'                    => 'Email3__c',
        'EmailDesc'                 => 'EmailDesc__c',
        'EmailDesc2'                => 'EmailDesc2__c',
        'EmailDesc3'                => 'EmailDesc3__c',
        'Employer'                  => 'Employer__c',
        'ErrorLevel'                => 'ErrorLevel__c',
        'ErrorMessage'              => 'ErrorMessage__c',
        'FirstName'                 => 'FirstName__c',
        'Gender'                    => 'Gender__c',
        'HomeFax'                   => 'HomeFax__c',
        'HomePhone'                 => 'HomePhone__c',
        'HostItemID'                => 'HostItemID__c',
        'InsAgency'                 => 'InsAgency__c',
        'InsAgent'                  => 'InsAgent__c',
        'InsAgentAddress1'          => 'InsAgentAddress1__c',
        'InsAgentAddress2'          => 'InsAgentAddress2__c',
        'InsAgentCity'              => 'InsAgentCity__c',
        'InsAgentZipOrPostalCode'   => 'InsAgentZipOrPostalCode__c',
        'InsCompany'                => 'InsCompany__c',
        'InsCompanyAddress1'        => 'InsCompanyAddress1__c',
        'InsCompanyAddress2'        => 'InsCompanyAddress2__c',
        'InsCompanyCity'            => 'InsCompanyCity__c',
        'InsCompanyPhone'           => 'InsCompanyPhone__c',
        'InsCompanyState'           => 'InsCompanyState__c',
        'InsPolicyNo'               => 'InsPolicyNo__c',
        'InsVerifiedBy'             => 'InsVerifiedBy__c',
        'InsVerifiedDate'           => 'InsVerifiedDate__c',
        'Language'                  => 'Language__c',
        'LastName'                  => 'LastName__c',
        'LastUpdated'               => 'LastUpdated__c',
        'Mailability'               => 'Mailability__c',
        'MiddleName'                => 'MiddleName__c',
        'Name1'                     => 'Name1__c',
        'Name2'                     => 'Name2__c',
        'Name2Company'              => 'Name2Company__c',
        'Name2First'                => 'Name2First__c',
        'Name2Last'                 => 'Name2Last__c',
        'Name2Middle'               => 'Name2Middle__c',
        'Name2Suffix'               => 'Name2Suffix__c',
        'Name2Title'                => 'Name2Title__c',
        'NameBalances1'             => 'NameBalances1__c',
        'NameCode'                  => 'NameCode__c',
        'NameCompany'               => 'NameCompany__c',
        'NameSuffix'                => 'NameSuffix__c',
        'NameTitle'                 => 'NameTitle__c',
        'Over120Due'                => 'Over120Due__c',
        'Over30Due'                 => 'Over30Due__c',
        'Over60Due'                 => 'Over60Due__c',
        'Over90Due'                 => 'Over90Due__c',
        'Pager'                     => 'Pager__c',
        'PartsCounterCode'          => 'PartsCounterCode__c',
        'PartsFlag'                 => 'PartsFlag__c',
        'PartsType'                 => 'PartsType__c',
        'PreferredContactDay'       => 'PreferredContactDay__c',
        'PreferredContactMethod'    => 'PreferredContactMethod__c',
        'PreferredContactTime'      => 'PreferredContactTime__c',
        'PreferredLanguage'         => 'PreferredLanguage__c',
        'SaleType'                  => 'SaleType__c',
        'SecondaryHomePhone'        => 'SecondaryHomePhone__c',
        'ServiceCustomer'           => 'ServiceCustomer__c',
        'SpecInstructions'          => 'SpecInstructions__c',
        'SpecInstructions2'         => 'SpecInstructions2__c',
        'SpecInstructions3'         => 'SpecInstructions3__c',
        'SpecInstructions4'         => 'SpecInstructions4__c',
        'SpecInstructions5'         => 'SpecInstructions5__c',
        'State'                     => 'State__c',
        'TaxCode'                   => 'TaxCode__c',
        'Telephone'                 => 'Telephone__c',
        'TextMessageCarrier'        => 'TextMessageCarrier__c',
        'TextMessagePhone'          => 'TextMessagePhone__c',
        'Title1'                    => 'Title1__c',
        'ZipOrPostalCode'           => 'ZipOrPostalCode__c'
    };
}