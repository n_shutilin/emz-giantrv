@IsTest
public without sharing class CDKWeOweSchedulableTest {
    @TestSetup
    public static void makeData(){
        CDKTestDataFactory.initSettings();
        CDKTestDataFactory.abortJobs();
    }

    @IsTest
    public static void testSchedulable() {
        Test.setMock(HttpCalloutMock.class, CDKTestDataFactory.getApiMock(CDKTestDataFactory.MOCK.WE_OWE_EXTRACT));
        Test.startTest();
        System.schedule(CDKTestDataFactory.DUMMY_VALUE_TEST, CDKTestDataFactory.TEST_CRON, new CDKWeOweSchedulable());
        Test.stopTest();
    }
    
    @IsTest
    public static void testSchedulableServerError() {
        Test.setMock(HttpCalloutMock.class, CDKTestDataFactory.getApiMock(CDKTestDataFactory.MOCK.SERVER_ERROR));
        Test.startTest();
        System.schedule(CDKTestDataFactory.DUMMY_VALUE_TEST, CDKTestDataFactory.TEST_CRON, new CDKWeOweSchedulable());
        Test.stopTest();
    }
}
