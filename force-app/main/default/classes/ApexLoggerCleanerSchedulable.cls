global without sharing class ApexLoggerCleanerSchedulable implements Schedulable{
    global void execute(SchedulableContext sc) {
        cleanLogs();
    }

    global static void cleanLogs() {
        ApexLoggerCleanerBatch batch = new ApexLoggerCleanerBatch();
        Database.executeBatch(batch, 200);
    }
}
