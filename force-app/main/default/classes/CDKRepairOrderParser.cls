public without sharing class CDKRepairOrderParser {
    private static final CDK_Integration__c SETTINGS;
    private static final Map<String, Schema.SObjectField> RO_FIELDS;
    private static final Map<String, Schema.SObjectField> RO_DETAIL_FIELDS;
    private static final Map<String, Schema.SObjectField> RO_PART_FIELDS;

    static {
        SETTINGS = CDK_Integration__c.getOrgDefaults();
        RO_FIELDS =  Schema.SObjectType.CDK_Repair_Order__c.fields.getMap();
        RO_DETAIL_FIELDS =  Schema.SObjectType.CDK_Repair_Order_Detail__c.fields.getMap();
        RO_PART_FIELDS =  Schema.SObjectType.CDK_Repair_Order_Part__c.fields.getMap();
    }

    public static Map<String, List<SObject>> parse(Dom.Document doc, String dealerId, String namespace, String nodeTag) {
        List<CDK_Repair_Order__c> orders = new List<CDK_Repair_Order__c>();
        Map<String, List<CDK_Repair_Order_Detail__c>> orderToOrderDetails = new Map<String, List<CDK_Repair_Order_Detail__c>>();
        Map<String, List<CDK_Repair_Order_Part__c>> orderToOrderParts = new Map<String, List<CDK_Repair_Order_Part__c>>();
        for (Dom.XmlNode child: doc.getRootElement().getChildElements()) {
            if (child.getNodeType() == DOM.XmlNodeType.ELEMENT && child.getName() == nodeTag) {
                CDK_Repair_Order__c order = new CDK_Repair_Order__c();
                CDKParser.setFields(order, RO_FIELDS, RO_PARSE_MAP, child, namespace);
                order.UniqueId__c = dealerId + order.RONumber__c;
                orders.add(order);
                orderToOrderDetails.put(
                    order.UniqueId__c, 
                    CDKParser.setMultipleChildFields(
                        CDK_Repair_Order_Detail__c.class,
                        child, 
                        namespace, 
                        RO_DETAIL_FIELDS,
                        RO_DETAIL_PARSE_MAP
                    )
                );
                orderToOrderParts.put(
                    order.UniqueId__c,
                    CDKParser.setMultipleChildFields(
                        CDK_Repair_Order_Part__c.class,
                        child, 
                        namespace, 
                        RO_PART_FIELDS,
                        RO_PART_PARSE_MAP
                    )
                );
            }
        }

        List<CDK_Repair_Order_Detail__c> orderDetails = new List<CDK_Repair_Order_Detail__c>();
        List<CDK_Repair_Order_Part__c> orderParts = new List<CDK_Repair_Order_Part__c>();
        for (CDK_Repair_Order__c order : orders) {
            if (orderToOrderDetails.containsKey(order.UniqueId__c)) {
                for (CDK_Repair_Order_Detail__c orderDetail : orderToOrderDetails.get(order.UniqueId__c)) {
                    if (String.isBlank(orderDetail.linLineCode__c)) continue;
                    orderDetail.CDK_Repair_Order__r = new CDK_Repair_Order__c(UniqueId__c = order.UniqueId__c);
                    orderDetail.UniqueId__c = order.UniqueId__c + orderDetail.linLineCode__c;
                    orderDetails.add(orderDetail);
                }
            }
            if (orderToOrderParts.containsKey(order.UniqueId__c)) {
                for (CDK_Repair_Order_Part__c orderPart : orderToOrderParts.get(order.UniqueId__c)) {
                    if (String.isBlank(orderPart.prtPartNo__c)) continue;
                    orderPart.CDK_Repair_Order__r = new CDK_Repair_Order__c(UniqueId__c = order.UniqueId__c);
                    orderPart.UniqueId__c = order.UniqueId__c + orderPart.prtPartNo__c;
                    orderParts.add(orderPart);
                }
            }
        }

        if (orders.isEmpty()) {
            return null;
        }

        return new Map<String, List<SObject>> {
            'orders' => orders,
            'orderDetails' => orderDetails,
            'orderParts' => orderParts
        };
    }

    public static final Map<String, String> RO_PARSE_MAP = new Map<String, String>{
        'AddOnFlag'             => 'AddOnFlag__c',
        'Address'               => 'Address__c',
        'ApptDate'              => 'ApptDate__c',
        'ApptFlag'              => 'ApptFlag__c',
        'BookedDate'            => 'BookedDate__c',
        'BookerNo'              => 'BookerNo__c',
        'Cashier'               => 'Cashier__c',
        'CityStateZip'          => 'CityStateZip__c',
        'ClosedDate'            => 'ClosedDate__c',
        'ComebackFlag'          => 'ComebackFlag__c',
        'Comments'              => 'Comments__c',
        'ContactEmailAddress'   => 'ContactEmailAddress__c',
        'ContactPhoneNumber'    => 'ContactPhoneNumber__c',
        'CustNo'                => 'CustNo__c',
        'DeliveryDate'          => 'DeliveryDate__c',
        'ErrorLevel'            => 'ErrorLevel__c',
        'ErrorMessage'          => 'ErrorMessage__c',
        'EstComplDate'          => 'EstComplDate__c',
        'EstComplTime'          => 'EstComplTime__c',
        'HostItemID'            => 'HostItemID__c',
        'LastServiceDate'       => 'LastServiceDate__c',
        'Make'                  => 'Make__c',
        'MakeDesc'              => 'MakeDesc__c',
        'Mileage'               => 'Mileage__c',
        'MileageLastVisit'      => 'MileageLastVisit__c',
        'MileageOut'            => 'MileageOut__c',
        'Model'                 => 'Model__c',
        'Name1'                 => 'Name1__c',
        'Name2'                 => 'Name2__c',
        'OpenDate'              => 'OpenDate__c',
        'OrigPromisedDate'      => 'OrigPromisedDate__c',
        'OrigPromisedTime'      => 'OrigPromisedTime__c',
        'OrigWaiterFlag'        => 'OrigWaiterFlag__c',
        'PostedDate'            => 'PostedDate__c',
        'PriorityValue'         => 'PriorityValue__c',
        'PromisedDate'          => 'PromisedDate__c',
        'PromisedTime'          => 'PromisedTime__c',
        'PurchaseOrderNo'       => 'PurchaseOrderNo__c',
        'Remarks'               => 'Remarks__c',
        'RentalFlag'            => 'RentalFlag__c',
        'RONumber'              => 'RONumber__c',
        'ServiceAdvisor'        => 'ServiceAdvisor__c',
        'SoldByDealerFlag'      => 'SoldByDealerFlag__c',
        'SpecialCustFlag'       => 'SpecialCustFlag__c',
        'TagNo'                 => 'TagNo__c',
        'VehicleColor'          => 'VehicleColor__c',
        'VehID'                 => 'VehID__c',
        'VIN'                   => 'VIN__c',
        'VoidedDate'            => 'VoidedDate__c',
        'WaiterFlag'            => 'WaiterFlag__c'
    };

    public static final Map<String, String> RO_DETAIL_PARSE_MAP = new Map<String, String> {
        'linCampaignCode'       => 'linCampaignCode__c',
        'linCause'              => 'linCause__c',
        'linComebackFlag'       => 'linComebackFlag__c',
        'linComplaintCode'      => 'linComplaintCode__c',
        'linEstDuration'        => 'linEstDuration__c',
        'linLineCode'           => 'linLineCode__c',
        'linServiceRequest'     => 'linServiceRequest__c',
        'linStatusCode'         => 'linStatusCode__c',
        'linStatusCode'         => 'linStatusCode__c',
        'linStatusDesc'         => 'linStatusDesc__c',
        'linStoryEmployeeNo'    => 'linStoryEmployeeNo__c',
        'lbrOpCode'             => 'lbrOpCode__c',
        'lbrOpCodeDesc'         => 'lbrOpCodeDesc__c',
        'lbrActualHours'        => 'lbrActualHours__c',
        'disLopSeqNo'           => 'disLopSeqNo__c',
        'totPartsSale'          => 'totPartsSale__c',
        'linAddOnFlag'          => 'linAddOnFlag__c',
        'linActualWork'         => 'linActualWork__c',
        'linStorySequenceNo'    => 'linStorySequenceNo__c',
        'linBookerNo'           => 'linBookerNo__c',
        'linStoryText'          => 'linStoryText__c',
        'linDispatchCode'       => 'linDispatchCode__c'
    };

    public static final Map<String, String> RO_PART_PARSE_MAP = new Map<String, String>{
        'prtBin1' => 'prtBin1__c',
        'prtClass' => 'prtClass__c',
        'prtDesc' => 'prtDesc__c',
        'prtSource' => 'prtSource__c',
        'prtCoreSale' => 'prtCoreSale__c',
        'prtSpecialStatus' => 'prtSpecialStatus__c',
        'prtUnitServiceCharge' => 'prtUnitServiceCharge__c',
        'prtComp' => 'prtComp__c',
        'prtCoreCost' => 'prtCoreCost__c',
        'prtOutsideSalesmanNo' => 'prtOutsideSalesmanNo__c',
        'prtSale' => 'prtSale__c',
        'prtCompLineCode' => 'prtCompLineCode__c',
        'prtEmployeeNo' => 'prtEmployeeNo__c',
        'prtLaborType' => 'prtLaborType__c',
        'prtPartNo' => 'prtPartNo__c',
        'prtQtyOrdered' => 'prtQtyOrdered__c',
        'prtQtySold' => 'prtQtySold__c',
        'prtCost' => 'prtCost__c',
        'prtList' => 'prtList__c',
        'prtMcdPercentage' => 'prtMcdPercentage__c',
        'prtExtendedSale' => 'prtExtendedSale__c',
        'prtLaborSequenceNo' => 'prtLaborSequenceNo__c',
        'prtExtendedCost' => 'prtExtendedCost__c',
        'prtLineCode' => 'prtLineCode__c',
        'prtSequenceNo' => 'prtSequenceNo__c',
        'prtQtyOnHand' => 'prtQtyOnHand__c',
        'prtQtyBackordered' => 'prtQtyBackordered__c',
        'prtQtyFilled' => 'prtQtyFilled__c'
    };
}