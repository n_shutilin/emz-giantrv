public without sharing class CDKDealParser {
    private static final CDK_Integration__c SETTINGS;
    private static final String EXTRACT_DEAL_NAMESPACE;
    private static final String DEFAULT_NODE_CHILD = 'FISalesClosed';

    static {
        SETTINGS = CDK_Integration__c.getOrgDefaults();
        EXTRACT_DEAL_NAMESPACE = CDKSoapConstants.NAMESPACE.DEAL;
    }

    public static List<CDK_Deal__c> parse(Dom.Document doc, String dealerId) {
        Map<String, Schema.SObjectField> dealFields = Schema.SObjectType.CDK_Deal__c.fields.getMap();

        List<CDK_Deal__c> deals = new List<CDK_Deal__c>();
        for (Dom.XmlNode child: doc.getRootElement().getChildElements()) {
            if (child.getNodeType() == DOM.XmlNodeType.ELEMENT && child.getName() == DEFAULT_NODE_CHILD) {
                CDK_Deal__c deal = new CDK_Deal__c();
                CDKParser.setFields(deal, dealFields, DEAL_PARSE_MAP, child, EXTRACT_DEAL_NAMESPACE);
                deal.DealerId__c = dealerId;
                deal.UniqueId__c = dealerId + deal.DealNo__c;
                deal.Dealer__c = CDKSoapConstants.getDealerNameByDealerId(dealerId);
                deals.add(deal);
            }
        }

        if (deals.isEmpty()) {
            return null;
        }

        return deals;
    }

    public static final Map<String, String> DEAL_PARSE_MAP = new Map<String, String>{
        'BirthDate'                 => 'BirthDate__c', 
        'Branch'                    => 'Branch__c',
        'BusinessPhone'             => 'BusinessPhone__c',
        'City'                      => 'City__c', 
        'CoName1'                   => 'CoName1__c', 
        'County'                    => 'County__c',   
        'CRMSalesMgrId'             => 'CRMSalesMgrId__c', 
        'CRMSalesMgrName'           => 'CRMSalesMgrName__c', 
        'CRMSaleType'               => 'CRMSaleType__c',  
        'CRMSP1Id'                  => 'CRMSP1Id__c',   
        'CRMSP1Name'                => 'CRMSP1Name__c',  
        'CRMSP2Id'                  => 'CRMSP2Id__c',  
        'CRMSP2Name'                => 'CRMSP2Name__c',  
        'CustNo'                    => 'CustNo__c',  
        'CustOrCompanyCode'         => 'CustOrCompanyCode__c', 
        'DealerDefined4'            => 'DealerDefined4__c',
        'DealerDefined6'            => 'DealerDefined6__c',
        'DealerDefined8'            => 'DealerDefined8__c',
        'DealEvent1'                => 'DealEvent1__c',
        'DealEvent10'               => 'DealEvent10__c',  
        'DealEvent10Date'           => 'DealEvent10Date__c', 
        'DealEvent1Date'            => 'DealEvent1Date__c',
        'DealEvent2'                => 'DealEvent2__c',
        'DealEvent2Date'            => 'DealEvent2Date__c',
        'DealEvent3'                => 'DealEvent3__c', 
        'DealEvent3Date'            => 'DealEvent3Date__c',  
        'DealEvent4'                => 'DealEvent4__c',  
        'DealEvent4Date'            => 'DealEvent4Date__c',
        'DealEvent5'                => 'DealEvent5__c', 
        'DealEvent5Date'            => 'DealEvent5Date__c',
        'DealEvent6'                => 'DealEvent6__c',
        'DealEvent6Date'            => 'DealEvent6Date__c',
        'DealEvent7'                => 'DealEvent7__c',
        'DealEvent7Date'            => 'DealEvent7Date__c',
        'DealEvent8'                => 'DealEvent8__c',
        'DealEvent8Date'            => 'DealEvent8Date__c',
        'DealEvent9'                => 'DealEvent9__c',
        'DealEvent9Date'            => 'DealEvent9Date__c',
        'DealNo'                    => 'DealNo__c',
        'DealSource'                => 'DealSource__c',
        'Email1'                    => 'Email1__c',
        'Email1Desc'                => 'Email1Desc__c',
        'Email2'                    => 'Email2__c',
        'Email2Desc'                => 'Email2Desc__c',
        'Email3'                    => 'Email3__c',
        'Email3Desc'                => 'Email3Desc__c',
        'FiWipStatusCode'           => 'FiWipStatusCode__c',
        'HomePhone'                 => 'HomePhone__c',
        'Make'                      => 'Make__c',
        'MakeName'                  => 'MakeName__c',
        'Model'                     => 'Model__c',
        'ModelName'                 => 'ModelName__c',
        'ModelNo'                   => 'ModelNo__c',
        'ModelType'                 => 'ModelType__c',
        'MSRP'                      => 'MSRP__c',
        'Name'                      => 'Name__c',
        'Name1'                     => 'Name1__c',
        'NetTrade1'                 => 'NetTrade1__c',
        'NetTrade2'                 => 'NetTrade2__c',
        'SalesDate '                => 'SalesDate__c',
        'SalesMgr'                  => 'SalesMgr__c',
        'Salesperson1'              => 'Salesperson1__c',
        'Salesperson2'              => 'Salesperson2__c',
        'Salesperson3'              => 'Salesperson3__c',
        'SaleType'                  => 'SaleType__c',
        'State'                     => 'State__c',
        'StockNo'                   => 'StockNo__c',
        'Trade1Make'                => 'Trade1Make__c',
        'Trade1MakeName'            => 'Trade1MakeName__c',
        'Trade1Mileage'             => 'Trade1Mileage__c',
        'Trade1Model'               => 'Trade1Model__c',
        'Trade1ModelName'           => 'Trade1ModelName__c',
        'Trade1ModelNo'             => 'Trade1ModelNo__c',
        'Trade1ModelType'           => 'Trade1ModelType__c',
        'Trade1Stock'               => 'Trade1Stock__c',
        'Trade1VIN'                 => 'Trade1VIN__c',
        'Trade1Year'                => 'Trade1Year__c',
        'Trade2MakeName'            => 'Trade2MakeName__c',
        'Trade2Mileage'             => 'Trade2Mileage__c',
        'Trade2Model'               => 'Trade2Model__c',
        'Trade2ModelName'           => 'Trade2ModelName__c',
        'Trade2ModelNo'             => 'Trade2ModelNo__c',
        'Trade2ModelType'           => 'Trade2ModelType__c',
        'Trade2Stock'               => 'Trade2Stock__c',
        'Trade2VIN'                 => 'Trade2VIN__c',
        'Trade2Year'                => 'Trade2Year__c',
        'TradeDealerDefined1'       => 'TradeDealerDefined1__c',
        'TradeDealerDefined2'       => 'TradeDealerDefined2__c',
        'TradeDealerDefined3'       => 'TradeDealerDefined3__c',
        'TradeDealerDefined4'       => 'TradeDealerDefined4__c',
        'TradeDealerDefined5'       => 'TradeDealerDefined5__c',
        'TradeDealerDefined6'       => 'TradeDealerDefined6__c',
        'TradeDealerDefined7'       => 'TradeDealerDefined7__c',
        'TradeDealerDefined8'       => 'TradeDealerDefined8__c',
        'VehicleMileage'            => 'VehicleMileage__c',
        'VIN'                       => 'VIN__c',
        'WAQNumber'                 => 'WAQNumber__c',
        'Year'                      => 'Year__c',
        'ZipOrPostalCode'           => 'ZipOrPostalCode__c'
    };
}