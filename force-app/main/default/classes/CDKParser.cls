public without sharing class CDKParser {
    public static void setFields(SObject curObject, Map<String, Schema.SObjectField> objectFieldMap,
        Map<String, String> fieldMap, Dom.XmlNode child, String namescape) {
            for (String field : fieldMap.keySet()) {
                Dom.XmlNode childElement = child.getChildElement(field, namescape);
                if (childElement != null && String.isNotBlank(childElement.getText().trim()) && fieldMap.get(field) != null) {
                    Schema.DisplayType fieldDataType = objectFieldMap.get(fieldMap.get(field)).getDescribe().getType();
                    String elementValue =  childElement.getText().trim();
                    if (fieldDataType == Schema.DisplayType.STRING ||
                        fieldDataType == Schema.DisplayType.EMAIL ||
                        fieldDataType == Schema.DisplayType.LONG ||
                        fieldDataType == Schema.DisplayType.PICKLIST ||
                        fieldDataType == Schema.DisplayType.PHONE ||
                        fieldDataType == Schema.DisplayType.ADDRESS ||
                        fieldDataType == Schema.DisplayType.TEXTAREA) {
                        curObject.put(fieldMap.get(field), elementValue);
                    }
                    if (fieldDataType == Schema.DisplayType.DATE) {
                        curObject.put(fieldMap.get(field), Date.valueOf(elementValue));
                    }
                    if (fieldDataType == Schema.DisplayType.INTEGER && elementValue.isNumeric()) {
                        curObject.put(fieldMap.get(field), Integer.valueOf(elementValue));
                    }
                    if (fieldDataType == Schema.DisplayType.DOUBLE ||
                        fieldDataType == Schema.DisplayType.PERCENT) {
                        try {
                            curObject.put(fieldMap.get(field), Double.valueOf(elementValue));
                        } catch (TypeException e) { continue; }
                    }
                    if (fieldDataType == Schema.DisplayType.DATETIME) {
                        curObject.put(fieldMap.get(field), Datetime.valueOf(elementValue));
                    }
                    if (fieldDataType == Schema.DisplayType.CURRENCY) {
                        //curObject.put(fieldMap.get(field), Decimal.valueOf(elementValue));
                    }
                    if (fieldDataType == Schema.DisplayType.BOOLEAN) {
                        //curObject.put(fieldMap.get(field), (elementValue.toUpperCase() == 'Y' ? true : false));
                    }
                }
            }
    }

    public static List<SObject> setMultipleChildFields(System.Type classType, Dom.XmlNode child, String namespace, Map<String, Schema.SObjectField> objectFieldMap, Map<String, String> fieldMap) {
        Map<String, SObject> records = new Map<String, SObject>();
        for (String field : fieldMap.keySet()) {
            Dom.XmlNode childElement = child.getChildElement(field, namespace);
            if (childElement == null) continue;
            
            for (Dom.XmlNode childElementItem : childElement.getChildElements()) {
                Schema.DisplayType fieldDataType = objectFieldMap.get(fieldMap.get(field)).getDescribe().getType();
                if (records.get(childElementItem.getAttribute('Idx', null)) != null) {
                    if (fieldDataType == Schema.DisplayType.DOUBLE) {
                        records.get(childElementItem.getAttribute('Idx', null)).put(fieldMap.get(field), Double.valueOf(childElementItem.getText().trim()));
                    } else if (fieldDataType == Schema.DisplayType.STRING) {
                        records.get(childElementItem.getAttribute('Idx', null)).put(fieldMap.get(field), childElementItem.getText().trim());
                    } else if (fieldDataType == Schema.DisplayType.BOOLEAN) {
                        records.get(childElementItem.getAttribute('Idx', null)).put(fieldMap.get(field), (childElementItem.getText().trim().toUpperCase() == 'Y'));
                    }
                } else {
                    SObject record = (SObject) classType.newInstance();
                    if (fieldDataType == Schema.DisplayType.DOUBLE) {
                        record.put(fieldMap.get(field), Double.valueOf(childElementItem.getText().trim()));
                        records.put(childElementItem.getAttribute('Idx', null), record);
                    } else if (fieldDataType == Schema.DisplayType.STRING) {
                        record.put(fieldMap.get(field), childElementItem.getText().trim());
                        records.put(childElementItem.getAttribute('Idx', null), record);
                    } else if (fieldDataType == Schema.DisplayType.BOOLEAN) {
                        //record.put(fieldMap.get(field), (childElementItem.getText().trim().toUpperCase() == 'Y'));
                        //records.put(childElementItem.getAttribute('Idx', null), record);
                    }
                }
            }
        }
        return records.values();
    }
}