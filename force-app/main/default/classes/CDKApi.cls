public without sharing class CDKApi {
    private static final ApexLogger LOGGER;
    private static final CDK_Integration__c SETTINGS;

    static {
        LOGGER = ApexLogger.create();
        SETTINGS = CDK_Integration__c.getOrgDefaults();
    }

    /**
     * @description Method for generating Basic auth header
     * @return   return encoded String 
     */
    public static String getAuthHeader() {
        return String.format('Basic {0}', new List<String> {
            EncodingUtil.base64Encode(Blob.valueOf(SETTINGS.UserID__c + ':' + SETTINGS.Password__c))
        });
    }

    /**
     * @description Method for making a GET request to the CDK API
     * @param  path      URL path  
     * @param  paramsMap Map<String, String> of params to be added as body of the request 
     * @return           return HttpResponse
     */
    public static HttpResponse doGet(String path, Map<String, String> paramsMap) {
        HttpRequest req = new HttpRequest();
        req.setMethod(HTTPConstants.Method.GET);
        req.setTimeout(HTTPConstants.TIMEOUT_120000);
        req.setHeader(HTTPConstants.AUTHORIZATION, getAuthHeader());
        req.setHeader(HTTPConstants.CONTENT_TYPE, HTTPConstants.MediaType.APPLICATION_FORM_URLENCODED_VALUE);
        req.setEndpoint(SETTINGS.API_URL__c + path);
        req.setBody(getUrlParams(paramsMap));

        HttpResponse res = new Http().send(req);
        // LOGGER.debug(res.getBody());
        return res;
    }

    /**
     * @description Method for making a POST request to the CDK API
     * @param  path      URL path  
     * @param  paramsMap Map<String, String> of params to be added as body of the request 
     * @return           return HttpResponse
     */
    public static HttpResponse doPost(String path, Map<String, String> paramsMap) {
        HttpRequest req = new HttpRequest();
        req.setMethod(HTTPConstants.Method.POST);
        req.setTimeout(HTTPConstants.TIMEOUT_120000);
        req.setHeader(HTTPConstants.AUTHORIZATION, getAuthHeader());
        req.setHeader(HTTPConstants.CONTENT_TYPE, HTTPConstants.MediaType.APPLICATION_FORM_URLENCODED_VALUE);
        req.setEndpoint(SETTINGS.API_URL__c + path);
        req.setBody(getUrlParams(paramsMap));

        HttpResponse res = new Http().send(req);
        LOGGER.debug(res.getBody());
        return res;
    }

    /**
     * @description Method for making a POST request to the CDK API
     * @param  path URL path  
     * @param  body XML to be added as body of the request 
     * @return      return HttpResponse
     */ 
    public static HttpResponse doPost(String path, DOM.Document body) {
        HttpRequest req = new HttpRequest();
        req.setMethod(HTTPConstants.Method.POST);
        req.setTimeout(HTTPConstants.TIMEOUT_120000);
        req.setHeader(HTTPConstants.CONTENT_TYPE, HTTPConstants.MediaType.TEXT_XML_VALUE);
        req.setEndpoint(SETTINGS.API_URL__c + path);
        req.setBodyDocument(body);

        HttpResponse res = new Http().send(req);
        LOGGER.debug(res.getBody());
        return res;
    }

    /**
     * @description Method for making a POST request to the CDK API
     * @param  endpoint endpoint 
     * @param  path URL path  
     * @param  body XML to be added as body of the request 
     * @return      return HttpResponse
     */ 
    public static HttpResponse doPost(String endpoint, String path, DOM.Document body) {
        HttpRequest req = new HttpRequest();
        req.setMethod(HTTPConstants.Method.POST);
        req.setTimeout(HTTPConstants.TIMEOUT_120000);
        req.setHeader(HTTPConstants.CONTENT_TYPE, HTTPConstants.MediaType.TEXT_XML_VALUE);
        req.setEndpoint(endpoint + path);
        req.setBodyDocument(body);

        HttpResponse res = new Http().send(req);
        LOGGER.debug(res.getBody());
        return res;
    }

    /**
     * @description Method for generating urlencoded form body from Map 
     * @param  paramsMap Map<String, String> of params to be added as body of the request
     * @return           return String for request body
     */
    private static String getUrlParams(Map<String, String> paramsMap) {
        if (paramsMap == null || paramsMap.isEmpty()) return '';

        List<String> params = new List<String>();
        for (String paramName : paramsMap.keySet()) {
            params.add(paramName + '=' + paramsMap.get(paramName));
        }

        return String.join(params, '&');
    }
}