//system.schedule('Customer', '0 9 * * * ?', new CDKCustomerSchedulable());
global without sharing class CDKCustomerSchedulable implements Schedulable {
    private static final ApexLogger LOGGER;
    private static final CDK_Integration__c SETTINGS;

    static {
        LOGGER = ApexLogger.create(CDKSoapConstants.PROCESS.CUSTOMER_EXTRACT);
        SETTINGS = CDK_Integration__c.getOrgDefaults();
    }

    global void execute(SchedulableContext sc) {
        syncCustomers();
        this.rescheduleJob(sc);
    }

    @future(callout=true)
    global static void syncCustomers() {
        String deltaDate = Date.today().format();
        List<CDK_Customer__c> customers = new List<CDK_Customer__c>();
        for (String dealerId : CDKSoapConstants.getListOfDealers()) {
            try {
                HttpResponse response = CDKApi.doGet(
                    SETTINGS.PIP_Customer_Extract__c, 
                    new Map<String, String>{
                        CDKSoapConstants.PARAM.DEALER_ID => dealerId, 
                        CDKSoapConstants.PARAM.QUERY_ID => CDKSoapConstants.QUERY.CUSTOMER_DELTA_EXTRACT, 
                        CDKSoapConstants.PARAM.DELTA_DATE => deltaDate
                    }
                );

                if (response.getStatusCode() != HTTPConstants.Status.OK) {
                    LOGGER.addLog(String.format(
                        CDKSoapConstants.LOGGER_MSG_TEMPLATE.RESPONDED_WITH_STATUS, 
                        new List<String>{ response.getStatus(), deltaDate, dealerId }
                    ));
                    continue;
                }
                
                List<CDK_Customer__c> customersToUpsert = CDKCustomerParser.parse(response.getBodyDocument(), dealerId);
                if (customersToUpsert == null || customersToUpsert.isEmpty()) {
                    LOGGER.addLog(String.format(
                        CDKSoapConstants.LOGGER_MSG_TEMPLATE.LIST_UPSERTED_EMPTY,
                        new List<String>{ dealerId }
                    ));
                    continue;
                }
                customers.addAll(customersToUpsert);
            } catch (Exception e) {
                LOGGER.addLog(e);
            }
        }
        upsertCustomers(customers);
        LOGGER.save();
    }
    
    public static void upsertCustomers(List<CDK_Customer__c> customersToUpsert) {
        if (customersToUpsert == null || customersToUpsert.isEmpty()) return;
        
        Database.UpsertResult[] results = Database.upsert(customersToUpsert, CDK_Customer__c.fields.UniqueId__c, false);
        LOGGER.addLog(String.format(
            CDKSoapConstants.LOGGER_MSG_TEMPLATE.INSERTED_UPDATED, 
            CDKSoapConstants.countUpsertResults(results)
        ));
    }

    global void rescheduleJob(SchedulableContext sc) {
        Datetime nextScheduleTime = System.now().addMinutes(15);
        String hour = nextScheduleTime.hour() <= 21 ? String.valueOf(nextScheduleTime.hour()) : '6';
        String minutes = String.valueOf(nextScheduleTime.minute());
        String cronValue = '0 ' + minutes + ' ' + hour + ' * * ?' ;
        String jobName = CDKSoapConstants.PROCESS.CUSTOMER_EXTRACT + ': ' + nextScheduleTime.format('hh:mm');

        CDKCustomerSchedulable nextJob = new CDKCustomerSchedulable();
        System.schedule(jobName, cronValue, nextJob);

        System.abortJob(sc.getTriggerId());
    }
}
//system.schedule('CDK Customer', '0 42 * * * ?', new CDKCustomerSchedulable());