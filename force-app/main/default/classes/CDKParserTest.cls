@IsTest
public without sharing class CDKParserTest {
    private static final CDK_Integration__c SETTINGS;
    private static final String EXTRACT_CUSTOMER_NAMESPACE;
    private static final String EXTRACT_REPAIR_ORDER_CLOSED_NAMESPACE;
    private static final String CUSTOMER_DEFAULT_NODE_CHILD = 'Customer';

    static {
        SETTINGS = CDK_Integration__c.getOrgDefaults();
        EXTRACT_CUSTOMER_NAMESPACE = 'http://www.dmotorworks.com/pip-extract-customer';
        EXTRACT_REPAIR_ORDER_CLOSED_NAMESPACE = 'http://www.dmotorworks.com/service-repair-order-closed';
    }

    @IsTest
    public static void testSetFields() {
        List<CDK_Customer__c> customers = new List<CDK_Customer__c>();
        Dom.Document doc = new Dom.Document();
        doc.load(CDKTestDataFactory.BODY_CUSTOMER_EXTRACT);
        Test.startTest();
        for (Dom.XmlNode child: doc.getRootElement().getChildElements()) {
            if (child.getNodeType() == DOM.XmlNodeType.ELEMENT && child.getName() == CUSTOMER_DEFAULT_NODE_CHILD) {
                CDK_Customer__c customer = new CDK_Customer__c();
                CDKParser.setFields(customer, Schema.SObjectType.CDK_Customer__c.fields.getMap(), CDKTestDataFactory.CUSTOMER_PARSE_MAP, child, EXTRACT_CUSTOMER_NAMESPACE);
                customers.add(customer);
            }
        } 
        Test.stopTest();
        System.assertEquals(1, customers.size());
    }

    @IsTest
    public static void testSetMultipleChildFields() {
        List<SObject> orderDetails = new List<SObject>();
        Dom.Document doc = new Dom.Document();
        doc.load(CDKTestDataFactory.BODY_REPAIR_ORDER_CLOSED_EXTRACT);
        Test.startTest();
        for (Dom.XmlNode child: doc.getRootElement().getChildElements()) {
            if (child.getNodeType() == DOM.XmlNodeType.ELEMENT && child.getName() == CDKSoapConstants.TAG.RO_CLOSED) {
                orderDetails.addAll(CDKParser.setMultipleChildFields(
                    CDK_Repair_Order_Detail__c.class,
                    child, 
                    EXTRACT_REPAIR_ORDER_CLOSED_NAMESPACE, 
                    Schema.SObjectType.CDK_Repair_Order_Detail__c.fields.getMap(),
                    CDKTestDataFactory.RO_DETAIL_PARSE_MAP
                ));
            }
        } 
        Test.stopTest();
        System.assertEquals(4, orderDetails.size());
    }
}