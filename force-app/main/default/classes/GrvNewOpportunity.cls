public with sharing class GrvNewOpportunity {
    @AuraEnabled
    public static List<Inventory__c> searchInventoryByStockNumber(String stockNumber) {
        List<Inventory__c> inventories = [
            SELECT Id, Make__c, Model__c, Stock_Number__c, Year__c,
                Status__c, Condition__c, Class__c
            FROM Inventory__c
            WHERE Stock_Number__c = :stockNumber
            LIMIT 1
        ];

        return inventories;
    }

    @AuraEnabled
    public static List<Inventory__c> searchInventoryByParams(String params){
        try {
            DataWrapper wrappedData = (DataWrapper) JSON.deserialize(params, DataWrapper.class);
            String query = 'SELECT Id, Make__c, Model__c, Stock_Number__c, Year__c, Status__c, Condition__c, Class__c FROM Inventory__c WHERE ';
            List<String> rvTypes;
            List<String> rvMakes;
            List<String> rvModels;
            List<String> rvFloorplans;
            if (String.isNotBlank(wrappedData.condition) && !wrappedData.condition.equals('New and Used')) {
                query += adjustCondition(query, 'Condition__c = \'' +  wrappedData.condition + '\'');
            }
            if (String.isNotBlank(wrappedData.rvFuelType) && !wrappedData.rvFuelType.equals('Gas or Diesel')) {
                query += adjustCondition(query, 'Fuel_Type__c = \'' +  wrappedData.rvFuelType + '\'');
            }
            if (wrappedData.rvPriceMin != null) {
                query += adjustCondition(query, 'List_Price__c >= ' + wrappedData.rvPriceMin);
            }
            if (wrappedData.rvPriceMax != null) {
                query += adjustCondition(query, 'List_Price__c <= ' + wrappedData.rvPriceMax);
            }
            if (String.isNotBlank(wrappedData.rvType)) {
                rvTypes = wrappedData.rvType.split(';');
                query += adjustCondition(query, 'Class__c IN :rvTypes');
            }
            if (String.isNotBlank(wrappedData.rvMake)) {
                rvMakes = wrappedData.rvMake.split(';');
                query += adjustCondition(query, 'Make__c IN :rvMakes');
            }
            if (String.isNotBlank(wrappedData.rvMakeCustom)) {
                query += adjustCondition(query, 'Make_Custom__c = \'' + wrappedData.rvMakeCustom + '\'');
            }
            if (String.isNotBlank(wrappedData.rvModel)) {
                rvModels = wrappedData.rvModel.split(';');
                query += adjustCondition(query, 'Model__c IN :rvModels');
            }
            if (String.isNotBlank(wrappedData.rvModelCustom)) {
                query += adjustCondition(query, 'Model_Custom__c = \'' + wrappedData.rvModelCustom + '\'');
            }
            if (String.isNotBlank(wrappedData.rvFloorplan)) {
                rvFloorplans = wrappedData.rvFloorplan.split(';');
                query += adjustCondition(query, 'Floorplan__c IN :rvFloorplans');
            }
            // if (String.isNotBlank(wrappedData.rvDryWeightMin)) {

            // }
            // if (String.isNotBlank(wrappedData.rvDryWeightMin) || String.isNotBlank(wrappedData.rvDryWeightMax)) {
            //     if ()
            //     query += adjustCondition(query, 'GVWR__c <= ' + Integer.valueOf(wrappedData.rvDryWeight) );
            // }
            //rvGarage
            // if (String.isNotBlank(wrappedData.rvPhysicalLength)) {
            //     query += adjustCondition(query, 'Length__c <= \'' + wrappedData.rvPhysicalLength + '\'');
            // }
            query = query.removeEnd(' WHERE ');
            query += ' LIMIT 20';
            System.debug(query);
            List<Inventory__c> inventories = Database.query(query);
            return inventories;

        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    private static String adjustCondition(String query, String condition) {
        return query.trim().endsWith('WHERE') ? condition : ' AND ' + condition;
    }

    @AuraEnabled
    public static String createRecords(String data) {
        try {
            DataWrapper wrappedData = (DataWrapper) JSON.deserialize(data, DataWrapper.class);
            Account accountToInsert = wrappedData.getAccount();
            String query = 'SELECT Id FROM Account WHERE FirstName LIKE \'' + accountToInsert.FirstName + '\'' + ' AND LastName LIKE \'' + accountToInsert.LastName + '\''; 
            if (String.isNotBlank(accountToInsert.PersonEmail)) {
                query += adjustCondition(query, 'PersonEmail = \'' + accountToInsert.PersonEmail + '\'');
            }
            if (String.isNotBlank(accountToInsert.Phone)) {
                query += adjustCondition(query, 'Phone = \'' + accountToInsert.Phone + '\'');
            }
            
            List<Account> existingAccounts = Database.query(query);
            
            if (existingAccounts.isEmpty()) {
                insert accountToInsert;
            }
    
            Opportunity opportunityToInsert = wrappedData.getOpportunity();
            opportunityToInsert.AccountId = existingAccounts.isEmpty() ? accountToInsert.Id : existingAccounts.get(0).Id;
            insert opportunityToInsert;
            return opportunityToInsert.Id;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static String updateRecords(String data, Id opportunityId){
        try {
            DataWrapper wrappedData = (DataWrapper) JSON.deserialize(data, DataWrapper.class);
            Opportunity opportunityToUpdate = wrappedData.getOpportunity();
            opportunityToUpdate.Id = opportunityId;
            update opportunityToUpdate;
            return opportunityToUpdate.Id;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static void createOpportunityInventories(Id opportunityId, List<String> inventoriesIds){
        try {
            List<Matching_Vehicles__c> existingInventories = [
                SELECT Id 
                FROM Matching_Vehicles__c
                WHERE Opportunity__c = :opportunityId
            ];

            if (!existingInventories.isEmpty()) {
                delete existingInventories;
            }

            if (inventoriesIds.isEmpty()) return;
            List<Matching_Vehicles__c> oppInventories = new List<Matching_Vehicles__c>();
            for (String inventoryId : inventoriesIds) {
                oppInventories.add(new Matching_Vehicles__c(
                    Inventory__c = inventoryId,
                    Opportunity__c = opportunityId
                ));
            }

            insert oppInventories;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static DataWrapper getInfoByOpportunityId(Id opportunityId){
        try {
            List<Opportunity> opportunities = [
                SELECT AccountId, Stock_Number__c, Condition__c, Price_Min__c, Price_Max__c, 
                    RV_Type__c, Make__c, Make_Custom__c, Model__c, Model_Custom__c, 
                    Floorplan_Type__c, Dry_Weight_Min__c, Dry_Weight_Max__c, 
                    Fuel_Type__c, Garage_Length_Min__c, Garage_Length_Max__c, 
                    Physical_Length_Min__c, Physical_Length_Max__c
                FROM Opportunity
                WHERE Id = :opportunityId
            ];

            if (opportunities.isEmpty() || String.isBlank(opportunities.get(0).AccountId)) {
                return null;
            }

            List<Account> accounts = [
                SELECT Id, FirstName, LastName, PersonEmail, Phone, Spouse_Name__c,
                    PersonMailingStreet, PersonMailingCity, PersonMailingCountry, 
                    PersonMailingState, PersonMailingPostalCode 
                FROM Account
                WHERE Id = :opportunities.get(0).AccountId                
            ];

            if (accounts.isEmpty()) {
                return null;
            }

            return new DataWrapper(accounts.get(0), opportunities.get(0));
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    public class AddressWrapper {
        @AuraEnabled 
        public String city {get; set;}
        @AuraEnabled 
        public String country {get; set;}
        @AuraEnabled 
        public String postalCode {get; set;}
        @AuraEnabled 
        public String province {get; set;}
        @AuraEnabled 
        public String street {get; set;}

        public AddressWrapper() {}

        public AddressWrapper(SObject sObj, String fieldPrefix) {
            this.city = (String) sObj.get(fieldPrefix + 'City');
            this.country = (String) sObj.get(fieldPrefix + 'Country');
            this.postalCode = (String) sObj.get(fieldPrefix + 'PostalCode');
            this.province = (String) sObj.get(fieldPrefix + 'State');
            this.street = (String) sObj.get(fieldPrefix + 'Street');
        }
    }

    public class DataWrapper {
        @AuraEnabled 
        public String firstName {get; set;}
        @AuraEnabled 
        public String lastName {get; set;}
        @AuraEnabled 
        public String email {get; set;}
        @AuraEnabled 
        public String phone {get; set;}
        @AuraEnabled
        public String spouseName {get; set;}
        @AuraEnabled
        public AddressWrapper personMailingAddress {get; set;}
        @AuraEnabled 
        public String stockNumber {get; set;}
        @AuraEnabled 
        public String condition {get; set;}
        @AuraEnabled 
        public Decimal rvPriceMin {get; set;}
        @AuraEnabled 
        public Decimal rvPriceMax {get; set;}
        @AuraEnabled 
        public String rvType {get; set;}
        @AuraEnabled 
        public transient String rvTypeValue {get; set;}
        @AuraEnabled 
        public String rvMake {get; set;}
        @AuraEnabled 
        public transient String rvMakeValue {get; set;}
        @AuraEnabled 
        public String rvMakeCustom {get; set;}
        @AuraEnabled 
        public String rvModel {get; set;}
        @AuraEnabled 
        public transient String rvModelValue {get; set;}
        @AuraEnabled 
        public String rvModelCustom {get; set;}
        @AuraEnabled 
        public String rvFloorplan {get; set;}
        @AuraEnabled 
        public transient String rvFloorplanValue {get; set;}
        @AuraEnabled 
        public String rvDryWeightMin {get; set;}
        @AuraEnabled 
        public String rvDryWeightMax {get; set;}
        @AuraEnabled 
        public String rvFuelType {get; set;}
        @AuraEnabled 
        public String rvGarageLengthMin {get; set;}
        @AuraEnabled 
        public String rvGarageLengthMax {get; set;}
        @AuraEnabled 
        public String rvPhysicalLengthMin {get; set;}
        @AuraEnabled 
        public String rvPhysicalLengthMax {get; set;}

        public DataWrapper() {
            this.personMailingAddress = new AddressWrapper();
        }

        public DataWrapper(Account acc, Opportunity opp) {
            this.firstName = acc.FirstName;
            this.lastName = acc.LastName;
            this.email = acc.PersonEmail;
            this.phone = acc.Phone;
            this.spouseName = acc.Spouse_Name__c;
            this.personMailingAddress = new AddressWrapper(acc, 'PersonMailing');

            this.stockNumber = opp.Stock_Number__c;
            this.condition = opp.Condition__c;
            this.rvPriceMin = opp.Price_Min__c;
            this.rvPriceMax = opp.Price_Max__c;
            this.rvTypeValue = opp.RV_Type__c;
            this.rvMakeValue = opp.Make__c;
            this.rvMakeCustom = opp.Make_Custom__c;
            this.rvModelValue = opp.Model__c;
            this.rvModelCustom = opp.Model_Custom__c;
            this.rvFloorplanValue = opp.Floorplan_Type__c;
            this.rvDryWeightMin = opp.Dry_Weight_Min__c;
            this.rvDryWeightMax = opp.Dry_Weight_Max__c;
            this.rvFuelType = opp.Fuel_Type__c;
            this.rvGarageLengthMin = opp.Garage_Length_Min__c;
            this.rvGarageLengthMax = opp.Garage_Length_Max__c;
            this.rvPhysicalLengthMin = opp.Physical_Length_Min__c;
            this.rvPhysicalLengthMax = opp.Physical_Length_Max__c;
        }

        public Account getAccount() {
            return new Account(
                FirstName = this.firstName,
                LastName = this.lastName,
                PersonEmail = this.email,
                Phone = this.phone,
                Spouse_Name__c = this.spouseName,
                PersonMailingStreet = this.personMailingAddress.street,
                PersonMailingCity = this.personMailingAddress.city,
                PersonMailingCountry= this.personMailingAddress.country,
                PersonMailingState = this.personMailingAddress.province,
                PersonMailingPostalCode = this.personMailingAddress.postalCode
            );
        }

        public Opportunity getOpportunity() {
            return new Opportunity(
                Name = 'Test',
                StageName = 'Qualification',
                CloseDate = Date.today(),
                Spouse_Name__c = this.spouseName,
                Stock_Number__c = this.stockNumber,
                Condition__c = this.condition,
                Price_Min__c = this.rvPriceMin,
                Price_Max__c = this.rvPriceMax,
                RV_Type__c = this.rvType,
                Make__c = this.rvMake,
                Make_Custom__c = this.rvMakeCustom,
                Model__c = this.rvModel,
                Model_Custom__c = this.rvModelCustom,
                Floorplan_Type__c = this.rvFloorplan,
                Dry_Weight_Min__c = this.rvDryWeightMin,
                Dry_Weight_Max__c = this.rvDryWeightMax,
                Fuel_Type__c = this.rvFuelType,
                Garage_Length_Min__c = this.rvGarageLengthMin,
                Garage_Length_Max__c = this.rvGarageLengthMax,
                Physical_Length_Min__c = this.rvPhysicalLengthMin,
                Physical_Length_Max__c = this.rvPhysicalLengthMax
            );
        }
    }
}