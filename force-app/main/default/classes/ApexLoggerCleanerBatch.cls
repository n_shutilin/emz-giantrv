global class ApexLoggerCleanerBatch implements Database.Batchable<SObject> {

    global Database.QueryLocator start(Database.BatchableContext BC) {         
        return Database.getQueryLocator([SELECT Id FROM Apex_Log__c WHERE CreatedDate <= LAST_N_DAYS:14]);
    }
     
    global void execute(Database.BatchableContext BC, List<Apex_Log__c> logs) {
        delete logs; 
    }   
     
    global void finish(Database.BatchableContext BC) {}
}
