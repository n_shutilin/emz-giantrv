@IsTest
public without sharing class ApexLoggerCleanerSchedulableTest {
    @IsTest
    public static void testSchedulable() {
        Apex_Log__c log = new Apex_Log__c();
        insert log;
        Test.setCreatedDate(log.id, DateTime.newInstance(2012,12,12));
        Test.startTest();
        System.schedule(CDKTestDataFactory.DUMMY_VALUE_TEST, CDKTestDataFactory.TEST_CRON, new ApexLoggerCleanerSchedulable());
        Test.stopTest();
    }
}
