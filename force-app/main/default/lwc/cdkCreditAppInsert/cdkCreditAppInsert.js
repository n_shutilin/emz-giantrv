import { LightningElement, api, wire, track } from 'lwc';
import retrieveDeal from '@salesforce/apex/CDKDealInsertComponent.retrieveDeal';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import sendCreditApplication from '@salesforce/apex/CDKCreditAppInsertComponent.sendCreditApplication';
const DEFAULT_INSERT_RESULT = { success: true, message: '' };

export default class CdkCreditAppInsert extends LightningElement {
  @api recordId;
  dealInfo = {};
  isLoading = false;
  validationMessage = '';
  insertResult = DEFAULT_INSERT_RESULT;

  @wire(retrieveDeal, { recordId: '$recordId' })
  getDealInfo(value) {
    this.wiredDealInfo = value;
    const { data, error } = value;
    if (data) {
      this.dealInfo = data;
      if (!this.dealInfo.CDK_Deal_Inserted_Date__c) {
        // this._buildValidationMessage()
        this.validationMessage = 'Deal was not inserted';
      }
      console.log(this.dealInfo)
    } else if (error) {
      console.error(error);
    }
  }
  openModal = false;
  loadingModal = false;
  handleOpenModal() {
    this.openModal = true;
  }

  handleModalCancel() {
    this.openModal = false;
  }

  handleCreditAppInsert(event) {
    this.insertResult = DEFAULT_INSERT_RESULT;
    this.isLoading = true;
    this.loadingModal = true;
    const creditAppId = event.detail.id
    sendCreditApplication({ creditAppId })
      .then((result) => {
        console.log('res ', result)
        this.insertResult = result;
        if (!this.insertResult.success) {
          this.loadingModal = false;
          this._showToast({
            title: 'Error',
            variant: 'error',
            message: this.insertResult.message
          });
        }
        if (this.insertResult.success) {
          this._showToast({
            title: 'Success',
            variant: 'success',
            message: this.insertResult.message
          });
          this.openModal = false;
          refreshApex(this.wiredDealInfo);
        }
        this.isLoading = false;
      })
      .catch(error => {
        console.error(error)
        this.isLoading = false;
      })
  }

  _showToast(info) {
    const event = new ShowToastEvent(info);
    this.dispatchEvent(event);
  }

  get isDisabled() {
    return this.validationMessage !== '';
  }
}