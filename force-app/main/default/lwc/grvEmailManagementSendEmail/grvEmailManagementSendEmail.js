import { LightningElement, api, track, wire } from 'lwc';
import { loadStyle } from 'lightning/platformResourceLoader';
import TIME_ZONE from '@salesforce/i18n/timeZone';
import LOCALE from '@salesforce/i18n/locale';
import { getRecord } from 'lightning/uiRecordApi';
import { setOriginalBody, DEFAULT_ORIGINAL_MESSAGE } from './grvEmailManagementSendEmailUtility';
import sendEmail from '@salesforce/apex/GrvEmailManagement.sendEmail';
import EMAIL_FIELD from '@salesforce/schema/User.Email';
import USER_ID from '@salesforce/user/Id';
const DEFAULT_COMPOSER_CLASS = 'slds-docked-composer slds-grid slds-grid_vertical ';
import GrvEmailManagementCSS from '@salesforce/resourceUrl/GrvEmailManagementCSS';


export default class GrvEmailManagementSendEmail extends LightningElement {
  constructor() {
    super();
    loadStyle(this, GrvEmailManagementCSS);
  }

  @api recordId = '';
  @api recordName = '';
  sfdcBaseURL;
  _tz = { locale: LOCALE, timeZone: TIME_ZONE };
  _isExpanded = true;
  _isLoading = false;
  @track uploadedFiles = [];

  @track _originalMessage = {...DEFAULT_ORIGINAL_MESSAGE}

  @api context = 'reply';
  @api 
  get originalMessage() { return this._originalMessage }
  set originalMessage(value) {
    this._resetOriginalMessage();
    this._originalMessage = setOriginalBody(this.context, value, this._tz);
  }

  _scrollToFiles = false;
  renderedCallback() {
    this.sfdcBaseURL = window.location.origin;
    if (this._scrollToFiles) {
      this.template.querySelector('.file-pill').scrollIntoView({behavior: 'smooth'});
      this._scrollToFiles = false;
    }
  }
    
  @api setTemplate(template) {
    this._originalMessage.templateId = template.id;
    this._originalMessage.body = template.htmlValue;
  }

  @wire(getRecord, { recordId: USER_ID, fields: [EMAIL_FIELD]}) 
  wireduser({ error, data }) {
    if (error) {
      console.log(error);
    } else if (data) {
        this._originalMessage.fromAddress = data.fields.Email.value;
    }
  }
  
  _resetOriginalMessage() {
    this._originalMessage = {
      ...DEFAULT_ORIGINAL_MESSAGE
    }
  }
  
  handlePanelExpandCollapse() {
    this._isExpanded = !this._isExpanded;
  }
  
  handlePanelClose() {
    this.dispatchEvent(new CustomEvent('action', { detail: {action: 'close'} }))
  }
  
  handlePillAdd(event) {
    const context = event.detail.context;
    const value = event.detail.value;
    this._originalMessage[context].push({ value: value, label: value });
  }
  
  handlePillRemove(event) {
    const context = event.detail.context;
    const index = event.detail.index;    
    this._originalMessage[context].splice(index, 1);
  }
  
  handleInputChange(event) {
    console.log(event.target.name);
    const inputName = event.target.name;
    const value = event.target.value;
    if (inputName === 'toAddress') {
      this.toAddresses.push({ name: value, label: value })
      this.template.querySelector('input[name=toAddress]').value = null;    
    } else {
      this._originalMessage[inputName] = value;  
    }
  }
  
  handleEditorAction(event) {
    this.dispatchEvent(new CustomEvent('action', { detail: {action: event.detail.name} }))
  }
  
  handleHtmlBodyChange(event) {
    this._originalMessage.body = event.detail;
  }
  
  handleSendClick() {
    console.log('orginal message ', this._originalMessage);
    this._isLoading = true;
    const messageToSend = {...this._originalMessage};
    messageToSend.toAddresses = messageToSend.toAddresses.map(item => item.value); 
    messageToSend.ccAddresses = messageToSend.ccAddresses.map(item => item.value); 
    messageToSend.bccAddresses = messageToSend.bccAddresses.map(item => item.value); 
    messageToSend.whatId = this.recordId;
    messageToSend.documentIds = this.uploadedFiles.map(file => file.contentVersionId);
    console.log('msg ', messageToSend)
    this._sendEmail(messageToSend);
  }
  
  handleUploadFinished(event) {
    const files = event.detail.files;
    this._scrollToFiles = true;    
    console.log('files ', files)
    this.uploadedFiles = [...this.uploadedFiles, ...files];
  }

  handleFileRemove(event) {
    const documentId = event.target.dataset.document;
    const fileIndex = this.uploadedFiles.findIndex(file => file.documentId === documentId);
    this.uploadedFiles.splice(fileIndex, 1);
  }
  
  _sendEmail(message) {
    sendEmail({ message })
    .then(() => {
      this._isLoading = false; 
      this.handlePanelClose() 
    })
    .catch((error) => console.error(error)) 
  }
  
  get opportunityName() {
    return this.recordName;
  }

  get opportunityUrl() {
    return `${this.sfdcBaseURL}/lightning/r/Opportunity/${this.recordId}/view`;
  }

  get classPanel() {
    return this._isExpanded 
    ? DEFAULT_COMPOSER_CLASS + 'slds-is-open'
    : DEFAULT_COMPOSER_CLASS
  }
}