export const CONTEXT = {
  REPLY: 'reply',
  REPLYALL: 'replyall',
  FORWARD: 'forward'
}

export const DEFAULT_ORIGINAL_MESSAGE = {
  fromAddress: '',
  toAddresses: [], 
  ccAddresses: [], 
  bccAddresses: [], 
  subject: '',
  body: '',
  whatId: '',
  templateId: '',
  documentIds: []
};

function _getEmailFormattedDate(tz, date) {
  return new Date(date).toLocaleString(tz.locale,{
    timeZone: tz.timeZone,
    month:'long', 
    year:'numeric', 
    day:'numeric',
    hour: 'numeric', 
    minute: 'numeric', 
    hour12: true
  })
}

export function buildReplyMessage(tz, message) {
  return `<br>
    <br>
    --------------- Original Message ---------------
    <br>
    From: <b>${message.senderName} </b> &#60;${message.senderEmail}&#62;
    <br>
    Date: ${_getEmailFormattedDate(tz, message.sendDate)}
    <blockquote style="border-left:1px #ccc solid; font-style: normal; font-family: inherit; margin: 2px; padding-left: 20px;">
      ${message.messageBody}
    </blockquote>`;
}

export function buildForwardMessage(tz, message) {
  return `<br>
    <br>
    --------------- Forwarded Message ---------------
    <br>
    From: <b>${message.senderName} </b> &#60;${message.senderEmail}&#62;
    <br>
    Date: ${_getEmailFormattedDate(tz, message.sendDate)}
    <br>
    Subject: ${message.subject}
    <br>
    To: ${message.recipientName} &#60;${message.recipientEmail}&#62;
    <br>
    Cc: ${message.ccEmail}
    <br>
    <br>
    <div>
      ${message.messageBody}
    </div>`;
}

export function setOriginalBody(context, value, tz) {
  let originalMessage = {...DEFAULT_ORIGINAL_MESSAGE};
  const subject = value.Subject ? value.Subject : '';
    if (context === CONTEXT.REPLY) {
      originalMessage.toAddresses = _getSeparatedAddresses(value.FromAddress);
      originalMessage.subject = 'Re: ' + subject;
      originalMessage.body = value 
        ? buildReplyMessage(tz, {
          subject: value.Subject, 
          messageBody: value.HtmlBody, 
          senderName: value.FromName, 
          senderEmail: value.FromAddress, 
          sendDate: value.MessageDate
        }) 
        : ''; 
    } 
    if (context === CONTEXT.REPLYALL) {
      originalMessage.toAddresses = _getSeparatedAddresses(value.FromAddress);
      originalMessage.ccAddresses = _getSeparatedAddresses(value.CcAddress);
      originalMessage.bccAddresses = _getSeparatedAddresses(value.BccAddress);
      originalMessage.subject = 'Re: ' + subject;
      originalMessage.body = value 
        ? buildReplyMessage(tz, {
          subject: value.Subject, 
          messageBody: value.HtmlBody, 
          senderName: value.FromName, 
          senderEmail: value.FromAddress, 
          sendDate: value.MessageDate
        }) 
        : ''; 
    }
    if (context === CONTEXT.FORWARD) {
      originalMessage.subject = 'Fw: ' + subject;
      originalMessage.body = value 
        ? buildForwardMessage(tz, {
          subject: value.Subject, 
          messageBody: value.HtmlBody, 
          recipientName: '', 
          recipientEmail: value.ToAddress, 
          ccEmail: value.CcAddress, 
          senderName: value.FromName, 
          senderEmail: value.FromAddress, 
          sendDate: value.MessageDate
        }) 
        : ''; 
    }
    return originalMessage;
}

function _getSeparatedAddresses(addressesString) {
  return addressesString 
    ? addressesString.split('; ').map(email => { return { value: email, label: email }})
    : [];
}

