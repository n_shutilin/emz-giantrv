import { LightningElement, api, track } from 'lwc';
import deleteOpener from '@salesforce/apex/GrvEmailManagement.deleteOpener';

export default class GrvEmailManagementDataTableLink extends LightningElement {
  @api recId;
  @api url;
  @api label;

  deleteOpener() {
    console.log('delete ', this.recId)
    deleteOpener({ recId: this.recId })
      .then(result => {})
      .catch(error => {})
  }
}