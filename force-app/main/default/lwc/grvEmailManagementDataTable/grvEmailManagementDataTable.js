import LightningDatatable from 'lightning/datatable';
import referenceTemplate from './grvEmailManagementDataTableLink.html';
import checkboxTemplate from './grvEmailManagementDataTableCheckbox.html';
import clickableTemplate from './grvEmailManagementDataTableClickable.html';

export default class GrvEmailManagementDataTable extends LightningDatatable {
  static customTypes = {
    reference: {
      template: referenceTemplate,
      typeAttributes: ['id', 'link', 'label']
    },
    checkbox: {
      template: checkboxTemplate,
      typeAttributes: ['id', 'display']
    },
    clickable: {
      template: clickableTemplate,
      typeAttributes: ['id', 'label']
    }
  }
}