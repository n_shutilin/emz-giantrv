import { LightningElement, api } from 'lwc';

export default class GrvEmailManagementDataTableClickable extends LightningElement {
  @api recId;
  @api label;

  handleClick() {
    const _event = CustomEvent('recordclick', {
      composed: true,
      bubbles: true,
      cancelable: true,
      detail: { value: { recId: this.recId }}
    });
    this.dispatchEvent(_event);
  }
}