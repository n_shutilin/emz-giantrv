import { LightningElement, wire, track, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent'
import createRecords from '@salesforce/apex/GrvNewOpportunity.createRecords';
import updateRecords from '@salesforce/apex/GrvNewOpportunity.updateRecords';
import searchInventoryByStockNumber from '@salesforce/apex/GrvNewOpportunity.searchInventoryByStockNumber';
import searchInventoryByParams from '@salesforce/apex/GrvNewOpportunity.searchInventoryByParams';
import createOpportunityInventories from '@salesforce/apex/GrvNewOpportunity.createOpportunityInventories';
import getInfoByOpportunityId from '@salesforce/apex/GrvNewOpportunity.getInfoByOpportunityId';
import { getPicklistValues } from 'lightning/uiObjectInfoApi';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import OPPORTUNITY_OBJECT from '@salesforce/schema/Opportunity';
import CONDITION_FIELD from '@salesforce/schema/Opportunity.Condition__c'
import RV_TYPE_FIELD from '@salesforce/schema/Opportunity.RV_Type__c'
import RV_MAKE_FIELD from '@salesforce/schema/Opportunity.Make__c'
import RV_MODEL_FIELD from '@salesforce/schema/Opportunity.Model__c'
import RV_FLOORPLAN_FIELD from '@salesforce/schema/Opportunity.Floorplan_Type__c'
import RV_FUEL_TYPE_FIELD from '@salesforce/schema/Opportunity.Fuel_Type__c'
import { NavigationMixin } from 'lightning/navigation';
import { DEFAULT_DATA, ADDRESS_DATA, MSG, CONSTANT } from 'c/grvNewOpportunityUtility';

export default class GrvNewOpportunity extends NavigationMixin(LightningElement) {
  @api recordId;
  _opporunityRecordTypeId;
  _stockNumber = '';
  _opportunityId;
  showMatch = false;
  @track matches = [];
  isLoading = false;
  selectedForSave = {};
  @track data =  {
    firstName: null,
    lastName: null,
    email: null,
    phone: null,
    spouseName: null,
    personMailingAddress: { ...ADDRESS_DATA },
    ...DEFAULT_DATA
  };

  _picklistValuesCondtion;
  _picklistValuesRvType;
  _picklistValuesRvMake;
  _picklistValuesRvModel;
  _picklistValuesRvFloorplan;
  _picklistValuesRvFuelType;

  _isMakeEmpty = true;
  _isModelEmpty = true;

  @wire(getObjectInfo, { objectApiName: OPPORTUNITY_OBJECT })
  objectInfo({ error, data }) {
    if (data) {
      console.log()
      this._opporunityRecordTypeId = data.defaultRecordTypeId
    } else if (error) {
      console.log(error)
    }
  }

  @wire(getPicklistValues, { recordTypeId: '$_opporunityRecordTypeId', fieldApiName: CONDITION_FIELD})
  conditionPicklistValues({ error, data }) {
    if (data) {
      this._picklistValuesCondtion = data.values;
    } else if (error) {
      console.log(error)
    }
  }

  @wire(getPicklistValues, { recordTypeId: '$_opporunityRecordTypeId', fieldApiName: RV_TYPE_FIELD})
  rvTypePicklistValues({ error, data }) {
    if (data) {
      this._picklistValuesRvType = data.values;
    } else if (error) {
      console.log(error)
    }
  }

  @wire(getPicklistValues, { recordTypeId: '$_opporunityRecordTypeId', fieldApiName: RV_MAKE_FIELD})
  rvMakePicklistValues({ error, data }) {
    if (data) {
      this._picklistValuesRvMake = data.values;
    } else if (error) {
      console.log(error)
    }
  }

  @wire(getPicklistValues, { recordTypeId: '$_opporunityRecordTypeId', fieldApiName: RV_MODEL_FIELD})
  rvModelPicklistValues({ error, data }) {
    if (data) {
      this._picklistValuesRvModel = data.values;
    } else if (error) {
      console.log(error)
    }
  }

  @wire(getPicklistValues, { recordTypeId: '$_opporunityRecordTypeId', fieldApiName: RV_FLOORPLAN_FIELD})
  rvFloorplanPicklistValues({ error, data }) {
    if (data) {
      this._picklistValuesRvFloorplan = data.values;
    } else if (error) {
      console.log(error)
    }
  }

  @wire(getPicklistValues, { recordTypeId: '$_opporunityRecordTypeId', fieldApiName: RV_FUEL_TYPE_FIELD})
  rvFuelTypePicklistValues({ error, data }) {
    if (data) {
      this._picklistValuesRvFuelType = data.values;
    } else if (error) {
      console.log(error)
    }
  }

  connectedCallback() {
    if (this.recordId) {
      this._getInfoByOpportunityId(this.recordId);
    }
  }

  handleCancel() {
    this.dispatchEvent(new CustomEvent('cancel'));
  }

  handleFieldChange(event) {
    const name = event.target.name;
    const value = event.target.value;
    
    if (name === 'personMailingAddress') {
      this.data[name].street = event.target.street;
      this.data[name].city = event.target.city;
      this.data[name].province = event.target.province;
      this.data[name].country = event.target.country;
      this.data[name].postalCode = event.target.postalCode;
      return;
    }

    this.data[name] = value === '' ? null : value;
    if (name === CONSTANT.PROP_NAME_RVMAKEVALUE) {
      if (value.length <= 0) {
        this._isMakeEmpty = true;
      } else {
        this._isMakeEmpty = false;
        this.data.rvMakeCustom = null;
      }
    }
    if (name === CONSTANT.PROP_NAME_RVMODELVALUE) {
      if (value.length <= 0) {
        this._isModelEmpty = true;
      } else {
        this._isModelEmpty = false;
        this.data.rvModelCustom = null;
      }
    }
  }

  handleSearch() {
    const areInputsFilled = [...this.template.querySelectorAll('.requiredInput')]
      .reduce((validSoFar, inputField) => {
        inputField.reportValidity();
        return validSoFar && inputField.checkValidity();
      }, true)
      
      if (!areInputsFilled) { return; }

      this.data.rvFloorplan = this.data.rvFloorplanValue != null ? this.data.rvFloorplanValue.join(';') : null;
      this.data.rvMake = this.data.rvMakeValue != null ? this.data.rvMakeValue.join(';') : null;
      this.data.rvModel = this.data.rvModelValue != null ? this.data.rvModelValue.join(';') : null;
      this.data.rvType = this.data.rvTypeValue != null ? this.data.rvTypeValue.join(';') : null;
      
      // return console.log('data ', this.data)
      if (this.data.stockNumber) {
        this._searchByStockNumber(this.data.stockNumber);
      } else {
        this._searchByParams(JSON.stringify(this.data));
      }
  }

  handleSaveMatches() {
    console.log(this.selectedForSave)
    this.isLoading = true;
    const listOfInvetoriesToSave = Object.entries(this.selectedForSave)
      .filter(([key, value]) => value)
      .map(([key, value]) =>  key);
      
    console.log(listOfInvetoriesToSave)
    if (this.isEditMode) {
      this._updateRecords(listOfInvetoriesToSave, this.recordId);
    } else {
      this._createRecords(listOfInvetoriesToSave);
    }
  }

  handleMatchSelect(event) {
    const detail = event.detail;
    this.selectedForSave[detail.id] = detail.checked;
  }

  handleCDKSelect(event) {
    const detail = event.detail;
    this.matches.forEach(match => {
      match.checked = detail.id === match.Id; 
    })
    console.log('this.matches ', this.matches)
  }

  _searchByStockNumber(stockNumber) {
    this.isLoading = true;
    searchInventoryByStockNumber({ stockNumber })
      .then((result) => {
        this._checkSearchResult(result);
      })
  }

  _searchByParams(params) {
    this.isLoading = true;
    searchInventoryByParams({ params })
      .then((result) => {
        this._checkSearchResult(result);
      })
  }

  _createRecords(listOfInvetoriesToSave) {
    createRecords({ data: JSON.stringify(this.data) })
      .then((result) => {
        console.log('Opportunity created ', result)
        this._opportunityId = result;
        if (listOfInvetoriesToSave.length > 0) {
          this._createOpportunityInventories(this._opportunityId, listOfInvetoriesToSave);
        } else {
          this.isLoading = false;
          this._navigateToOpportunityViewPage(this._opportunityId);
        }
      })
  }

  _updateRecords(listOfInvetoriesToSave, opportunityId) {
    updateRecords({ data: JSON.stringify(this.data), opportunityId })
      .then((result) => {
        console.log('Opportunity created ', result)
        this._opportunityId = result;
        this._createOpportunityInventories(this._opportunityId, listOfInvetoriesToSave);
      })
  }

  _createOpportunityInventories(opportunityId, inventoriesIds) {
    createOpportunityInventories({ opportunityId, inventoriesIds })
      .then((result) => {
        this.isLoading = false;
        this._showToast(MSG.SUCCESSFULLY_SAVED);   
        this._navigateToOpportunityViewPage(this._opportunityId);     
      })
  }

  _getInfoByOpportunityId(opportunityId) {
    this.isLoading = true;
    getInfoByOpportunityId({opportunityId})
      .then((result) => {
        if (!result) {
          this._showToast(MSG.ERROR);
          return;  
        }
        console.log('result  ', result)
        
        result.rvFloorplanValue = result.rvFloorplanValue != null ? result.rvFloorplanValue.split(';') : null;
        result.rvMakeValue = result.rvMakeValue != null ? result.rvMakeValue.split(';') : null;
        result.rvModelValue = result.rvModelValue != null ? result.rvModelValue.split(';') : null;
        result.rvTypeValue = result.rvTypeValue != null ? result.rvTypeValue.split(';') : null;
        Object.assign(this.data, result)
        this.isLoading = false;
        console.log('this data ', this.data)
      }) 
  }

  _showToast(message) {
    const event = new ShowToastEvent({
      message: message
    })
    this.dispatchEvent(event);
  }

  _showMatches() {
    this.dispatchEvent(new CustomEvent('headerchange', { 
      detail: {value: CONSTANT.MATCHING_VEHICLES} 
    }));
    this.showMatch = true;
    this.isLoading = false;
  }

  _checkSearchResult(result) {
    console.log(result)
    if (result.length <= 0) {
      this._showToast(MSG.NO_MATCHES)
      this.isLoading = false;
      return;
    }          
    this.matches = [...result];
    this._showMatches()
    // this._createRecords();
  }

  _navigateToOpportunityViewPage(oppId) {
    this[NavigationMixin.Navigate]({
      type: 'standard__recordPage',
      attributes: {
        recordId: oppId,
        actionName: 'view'
      }
    });
  }

  expandAll() {
    this.template.querySelectorAll('c-grv-new-opportunity-section')
      .forEach(section => section.expand())
  }

  collapseAll() {
    this.template.querySelectorAll('c-grv-new-opportunity-section')
      .forEach(section => section.collapse())
  }

  clearAll() {
    console.log('clear')
    // Object.assign(this.data, DEFAULT_DATA);
    const data = this.data
    this.data = { ...data, ...DEFAULT_DATA}
    console.log(this.data)
  }

  get isMakeEmpty() {
    return this._isMakeEmpty;
  }

  get isModelEmpty() {
    return this._isModelEmpty;
  }

  get conditionOptions() {
    return this._picklistValuesCondtion;
  }

  get rvTypeOptions() {
    return this._picklistValuesRvType;
  }

  get rvMakeOptions() {
    return this._picklistValuesRvMake;
  }

  get rvModelOptions() {
    return this._picklistValuesRvModel;
  }

  get rvFloorplanOptions() {
    return this._picklistValuesRvFloorplan;
  }

  get rvFuelTypeOptions() {
    return this._picklistValuesRvFuelType;
  }

  get showRvPriceIndicator() {
    return this.data.rvPriceMin || this.data.rvPriceMax;
  }

  get showRvTypeIndicator() {
    return this.data.rvTypeValue && this.data.rvTypeValue.length > 0;
  }

  get showModelMakeIndicator() {
    return (this.data.rvMakeValue && this.data.rvMakeValue.length > 0) || 
      (this.data.rvModelValue && this.data.rvModelValue.length > 0) || 
      this.data.rvMakeCustom || this.data.rvModelCustom;
  }

  get showDryWeightIndicator() {
    return this.data.rvDryWeightMin || this.data.rvDryWeightMax
  }

  get showGarageLength() {
    return this.data.rvGarageLengthMin || this.data.rvGarageLengthMax
  }

  get showPhysicalLength() {
    return this.data.rvPhysicalLengthMin || this.data.rvPhysicalLengthMax
  }

  get isEditMode() {
    return this.recordId
  }

  get CONSTANT() {
    return CONSTANT;
  }

  get isLastNameEmpty() {
    return !this.data.lastName;
  }

  get isFirstNameEmpty() {
    return !this.data.firstName;
  }
}