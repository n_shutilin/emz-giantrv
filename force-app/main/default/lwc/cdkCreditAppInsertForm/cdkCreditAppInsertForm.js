import { LightningElement, api, track } from 'lwc';
import createDraftCreditApplication from '@salesforce/apex/CDKCreditAppInsertComponent.createDraftCreditApplication'

export default class CdkCreditAppInsertForm extends LightningElement {
  @api recordId;
  @api isLoading = false
  uiRecordEdit = {}
  activeSections = [];
  _draft = {};
  errorMessage = '';
  connectedCallback() {
    this._createDraftCreditApplication();
  }

  _createDraftCreditApplication() {
    createDraftCreditApplication({recordId: this.recordId})
    .then((result) => {
      console.log(result)
      if (result.success) {
        this._draft = result.creditApp;
      } else {
        this.errorMessage = result.message;
      }
    })
  }

  handleLoad(event) {
    const layout = JSON.parse(JSON.stringify(event.detail.layout));
    layout.sections.forEach(section => {
      this.activeSections.push(section.id)
      section.layoutRows.forEach(layoutRow => {
        layoutRow.layoutItems.forEach(layoutItem => {
          layoutItem.layoutComponents.forEach(layoutComponent => {
            if (this._draft[layoutComponent.apiName]) {
              layoutComponent.value = this._draft[layoutComponent.apiName];
            }
          })
        })
      })
    })
    this.uiRecordEdit = layout
  }

  handleCancel() {
    this.dispatchEvent(new CustomEvent('cancel'));
  }

  handleSuccess(event) {
    console.log(event.detail.id)
    this.dispatchEvent(new CustomEvent('insert', { detail: { id: event.detail.id }}));
  }

  get isDraftLoaded() {
    return this._draft.Opportunity__c;
  }
}