import { LightningElement, api } from 'lwc';

export default class GrvEmailManagementSendEmailEditor extends LightningElement {
  _body = '';
  @api 
  get body() { return this._body }
  set body(value) {
    this._body = value;
    if (this._isRendered) {
      console.log('aa')
      this.modalBody.innerHTML = value;
    }
  }
  _lastSelection;
  _isRendered = false;
  renderedCallback() {
    this.modalBody.innerHTML = this.body;
    this._isRendered = true;
  }

  handleEditorChangeBody(event) {
    event.preventDefault()
    const selection = document.getSelection()
    this._lastSelection = selection.rangeCount === 0 ? null : selection.getRangeAt(0);
    // this.dispatchEvent(new CustomEvent('bodychange', { detail: this.modalBody.innerHTML }));
  }
  
  handleEditorChange(event) {
    event.preventDefault()
    const selection = document.getSelection()
    this._lastSelection = selection.rangeCount === 0 ? null : selection.getRangeAt(0);
    this.dispatchEvent(new CustomEvent('bodychange', { detail: this.modalBody.innerHTML }));
  }

  execCommand = event => {
    event.preventDefault();
    const command = event.currentTarget.dataset.command;
    document.execCommand(command, false);
  }

  handleMenuSelect(event) {
    event.preventDefault();
    event.stopPropagation();
    this.modalBody.focus();
    const command = event.currentTarget.dataset.command;
    const value = event.detail.value;
    
    let sel = document.getSelection();
    sel.collapseToStart();
    sel.removeAllRanges();
    sel.addRange(this._lastSelection);
    if (sel.rangeCount) {
      let e = document.createElement('span');
      if (command === 'fontName') {
        e.style.fontFamily = value; 
      } 
      if (command === 'fontSize') {
        e.style.fontSize = value; 
      }
      e.innerHTML = sel.toString();
  
      let range = sel.getRangeAt(0);
      range.deleteContents();
      range.insertNode(e);
    }    
  }

  handleInsertTemplate() {
    this.dispatchEvent(new CustomEvent('action', { detail: {name: 'template'} }))
  }

  get modalBody() {
    return this.template.querySelector('.modal-body')
  }

  _defaultFont = 'Salesforce Sans';
  _defaultSize = '12px';

  fonts = [
    {label: 'Salesforce Sans', value: 'Salesforce Sans'},
    {label: 'Arial', value: 'Arial'},
    {label: 'Courier', value: 'Courier'},
    {label: 'Verdana', value: 'Verdana'},
    {label: 'Tahoma', value: 'Tahoma'},
    {label: 'Garamond', value: 'Garamond'},
    {label: 'Times New Roman', value: 'Times New Roman'}
  ];

  fontSizes = [
    {label: '8', value: '8px'},
    {label: '9', value: '9px'},
    {label: '10', value: '10px'},
    {label: '11', value: '11px'},
    {label: '12', value: '12px'},
    {label: '14', value: '14px'},
    {label: '16', value: '16px'},
    {label: '18', value: '18px'},
    {label: '20', value: '20px'},
    {label: '22', value: '22px'},
    {label: '24', value: '24px'},
    {label: '26', value: '26px'},
    {label: '28', value: '28px'},
    {label: '36', value: '36px'},
    {label: '48', value: '48px'},
    {label: '72', value: '72px'}
  ] 
}