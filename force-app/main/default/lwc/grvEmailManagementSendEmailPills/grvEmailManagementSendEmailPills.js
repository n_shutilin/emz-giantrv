import { LightningElement, api } from 'lwc';

export default class GrvEmailManagementSendEmailPills extends LightningElement {
  @api items = [];
  @api context = '';
  isInputExpanded = false;
  handleInputChange(event) {
    this.dispatchEvent(new CustomEvent('add', { detail: { context: this.context, value: event.target.value }}))
    this.isInputExpanded = false;
  }

  handlePillRemove(event) {
    const index = event.detail.index;
    this.dispatchEvent(new CustomEvent('remove', { detail: { context: this.context, index: index}}))
  }

  expandCollapseInput() {
    this.isInputExpanded = !this.isInputExpanded;
  }
}