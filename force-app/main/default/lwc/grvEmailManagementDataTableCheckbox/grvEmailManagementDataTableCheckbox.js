import { LightningElement, api } from 'lwc';

export default class GrvEmailManagementDataTableCheckbox extends LightningElement {
  @api recId;

  handleCheckboxChange(event) {
    const _event = CustomEvent('recordselect', {
      composed: true,
      bubbles: true,
      cancelable: true,
      detail: { value: { recId: this.recId, state: event.target.checked }}
    });
    this.dispatchEvent(_event);
  }
}