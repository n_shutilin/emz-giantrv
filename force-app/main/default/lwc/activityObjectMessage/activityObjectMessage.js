import { LightningElement, api, wire } from 'lwc';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';

import ACCOUNT_EMAIL_OPT_OUT from '@salesforce/schema/Opportunity.Account.PersonHasOptedOutOfEmail';
import ACCOUNT_NAME from '@salesforce/schema/Opportunity.Account.Name';
const fields = [ACCOUNT_EMAIL_OPT_OUT, ACCOUNT_NAME];


export default class ActivityObjectMessage extends LightningElement {
  @api recordId;
  
  @wire(getRecord, { recordId: '$recordId', fields })
  opportunity;

  get emailOptOut() {
    return getFieldValue(this.opportunity.data, ACCOUNT_EMAIL_OPT_OUT);
  }

  get message() {
    return `${getFieldValue(this.opportunity.data, ACCOUNT_NAME)} opted out of email.`;
  }
}