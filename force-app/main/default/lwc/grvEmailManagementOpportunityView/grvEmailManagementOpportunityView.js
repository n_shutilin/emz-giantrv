import { LightningElement, api } from 'lwc';

export default class GrvEmailManagementOpportunityView extends LightningElement {
  @api oppFields = [];
  @api recordId = '';
  @api recordName = '';
  sfdcBaseURL;

  renderedCallback() {
    this.sfdcBaseURL = window.location.origin;
  }

  get opportunityName() {
    return this.recordName;
  }

  get opportunityUrl() {
    return `${this.sfdcBaseURL}/lightning/r/Opportunity/${this.recordId}/view`;
  }
}