import { LightningElement, api, wire } from 'lwc';
import { refreshApex } from '@salesforce/apex';
import sendDeal from '@salesforce/apex/CDKDealInsertComponent.sendDeal'
import retrieveDeal from '@salesforce/apex/CDKDealInsertComponent.retrieveDeal'
const DEFAULT_INSERT_RESULT = { success: true, message: '' };

export default class CdkDealInsert extends LightningElement {
  @api recordId;
  dealInfo = {};
  isLoading = false;
  insertResult = DEFAULT_INSERT_RESULT;
  validationMessage = '';

  @wire(retrieveDeal, { recordId: '$recordId' })
  getDealInfo(value) {
    this.wiredDealInfo = value;
    const { data, error } = value;
    if (data) {
      this.dealInfo = data;
      if (!this.dealInfo.CDK_Deal_Inserted_Date__c) {
        this._buildValidationMessage();
      }
      console.log(this.dealInfo)
    } else if (error) {
      console.error(error);
    }
  }

  handleDealInsert() {
    this.insertResult = DEFAULT_INSERT_RESULT;
    this.isLoading = true;
    sendDeal({recordId: this.recordId})
      .then((result) => {
        console.log('res ', result)
        this.insertResult = result;
        if (this.insertResult.success) {
          refreshApex(this.wiredDealInfo);
        } 
        this.isLoading = false;
      })
      .catch(error => {
        console.error(error)
        this.isLoading = false;
      })
  }

  _buildValidationMessage() {
    if (!this.dealInfo.Dealer__c) {
      this.validationMessage += 'Opportunity Dealer is not populated\n';
    }
    if (!this.dealInfo.Account) {
      this.validationMessage += 'Account is not linked to the Opportunity\n';
    }
    if (!this.dealInfo.Account.PersonMailingStreet) {
      this.validationMessage += 'Account Address Mailing Street is not populated\n';
    }
    if (!this.dealInfo.Account.PersonMailingCity) {
      this.validationMessage += 'Account Address Mailing City is not populated\n';
    }
    if (!this.dealInfo.Account.PersonMailingCountry) {
      this.validationMessage += 'Account Address Mailing Country is not populated\n';
    }
    if (!this.dealInfo.Account.PersonMailingPostalCode) {
      this.validationMessage += 'Account Address Mailing Postal Code is not populated\n';
    }
    if (!this.dealInfo.Account.PersonMailingState) {
      this.validationMessage += 'Account Address Mailing State is not populated\n';
    }
  }

  get isDisabled() {
    return this.validationMessage !== '';
  }
}