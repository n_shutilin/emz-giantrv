export const DEFAULT_DATA = {
  stockNumber: null,
  condition: null,
  rvType: null,
  rvTypeValue: null,
  rvMake: null,
  rvMakeValue: null,
  rvMakeCustom: null,
  rvModelCustom: null,
  rvModel: null,
  rvModelValue: null,
  rvFloorplan: null,
  rvFloorplanValue: null,
  rvDryWeightMin: null,
  rvDryWeightMax: null,
  rvFuelType: null,
  rvGarageLengthMin: null,
  rvGarageLengthMax: null,
  rvPhysicalLengthMin: null,
  rvPhysicalLengthMax: null,
  rvPriceMin: null,
  rvPriceMax: null
}

export const ADDRESS_DATA = {
  street: null,
  city: null,
  province: null,
  country: null,
  postalCode: null
}

export const MSG = {
  SUCCESSFULLY_SAVED: 'Successfully saved.',
  ERROR: 'Error.',
  NO_MATCHES: 'No matches found, please change search criteria.'
}

export const CONSTANT = {
  MATCHING_VEHICLES: 'Matching Vehicles',
  PROP_NAME_RVMAKEVALUE: 'rvMakeValue',
  PROP_NAME_RVMODELVALUE: 'rvModelValue',
  SECTION_CUSTOMER_INFORMATION: 'Customer Information',
  SECTION_RV_INFORMATION: 'RV Information',
  INPUT_STOCK_PLACEHOLDER: 'Enter a Stock # and press Search button (Only one Stock # is allowed for search)',
  INPUT_MAKE_CUSTOM_PLACEHOLDER: 'Enter the Make if it\'s not listed',
  INPUT_MODEL_CUSTOM_PLACEHOLDER: 'Enter the Model if it\'s not listed',
  INPUT_NAME_MISSING_TEXT: 'Complete the First Name or Last Name'
}