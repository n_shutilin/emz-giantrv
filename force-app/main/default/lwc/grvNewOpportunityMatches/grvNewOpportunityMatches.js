import { LightningElement, api } from 'lwc';

export default class GrvNewOpportunityMatches extends LightningElement {
  _matches = [];
  @api get matches() {
    return this._matches;
  }

  set matches(value) {
    this._matches = value;
  }

  handleSelectForSave(event) {
    const detail = {
      id: event.currentTarget.name,
      checked: event.currentTarget.checked
    };
    this.dispatchEvent(new CustomEvent('select', { detail }));
  }

  handleSelectForSendToCDK(event) {
    const detail = {
      id: event.currentTarget.name,
      checked: event.currentTarget.checked
    };
    this.dispatchEvent(new CustomEvent('selectcdk', { detail }));
  }
}