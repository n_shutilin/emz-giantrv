import { LightningElement, track } from 'lwc';
import getEmailTemplates from '@salesforce/apex/GrvEmailManagement.getEmailTemplates'

const TEMPLATE_FILTERS = [
{ label: 'All Lightning Templates', value: 'SFX'},
{ label: 'All Classic Templates', value: 'Aloha'},
  { label: 'Sample Lightning Templates', value: 'SFX_Sample'}
];
const COLUMNS = [
  { label: '', initialWidth: 46, type: 'button-icon', typeAttributes: { iconName: 'utility:insert_template', name: 'insert', variant: 'bare', title: 'Select this template' }},
  { label: 'Name', fieldName: 'Name' },
  { label: 'Description', fieldName: 'Description' },
  { label: 'Template Folders', fieldName: 'FolderName' },
];

export default class GrvEmailManagementEmailTemplateModal extends LightningElement {
  _selectedTemplateFilter = 'SFX';
  templateFilters = TEMPLATE_FILTERS;
  columns = COLUMNS;
  isLoading = false;
  @track _templates = [];

  connectedCallback() {
    this._getEmailTemplates(this._selectedTemplateFilter);
  }

  handleFilterChange(event) {
    const value = event.detail.value;
    this._getEmailTemplates(value);
  } 

  handleRowAction(event) {
    if (event.detail.action.name === 'insert') {
      console.log(JSON.stringify(event.detail.row))
      const row = event.detail.row;
      this.dispatchEvent(new CustomEvent('select', { detail: { templateId: row.Id, templateHtml: row.HtmlValue ? row.HtmlValue : row.Body }}))
    }
  }

  closeModal() {
    this.dispatchEvent(new CustomEvent('close'));
  }

  _getEmailTemplates(uiType) {
    this.isLoading = true;
    getEmailTemplates({uiType})
      .then(result => {
        console.log(result);
        this._templates = result;
        this.isLoading = false;
      })
      .catch((error) => console.error(error))
  }
}