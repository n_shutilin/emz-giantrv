import { LightningElement, wire, api } from 'lwc';
import getUserOptions from '@salesforce/apex/GrvEmailManagement.getUserOptions';

export default class GrvEmailManagementSidebar extends LightningElement {
  @api selectedRecords = []; 
  userOptions = [];
  filter = {
    address: null,
    emailOption: 'incoming',
    startDate: new Date(new Date().setDate(new Date().getDate() - 7)).toISOString(),
    endDate: new Date().toISOString()
  }

  @wire(getUserOptions)
  getPicklistValues({error, data}) {
    if (data && data.length > 0) {
      this.userOptions = data;
      this.filter.address = data[0].value;
      this.dispatchEvent(new CustomEvent('filterchange', { detail: this.filter }));

    } else if (error) {
      console.log(JSON.stringify(error));
    }
  }

  handleRefreshClick(event) {
    this.dispatchEvent(new CustomEvent('refresh'));
  }

  handleFilterChange(event) {
    const filterProperty = event.currentTarget.name;
    this.filter[filterProperty] = event.detail.value;
    this.dispatchEvent(new CustomEvent('filterchange', { detail: this.filter }));
  }

  handleDelete() {
    this.dispatchEvent(new CustomEvent('delete'));
  }

  get emailTypeOptions() {
    return [
      { label: 'Unread', value: 'unread' },
      { label: 'Inbox', value: 'incoming' },
      { label: 'Sent', value: 'outgoing' }
    ];
  }
}