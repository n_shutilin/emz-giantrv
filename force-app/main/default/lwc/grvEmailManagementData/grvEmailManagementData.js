import { LightningElement, api, track } from 'lwc';

export default class GrvEmailManagementData extends LightningElement {
  @api rows = [];
  @api isLoading;

  columns = [
    {label: '', fixedWidth: 24, hideDefaultActions: true, type: 'checkbox', typeAttributes: { id: {fieldName: 'id'}, display: {fieldName: 'recordLink'} }},
    {label: '', fieldName: 'opened', fixedWidth: 32, hideDefaultActions: true, cellAttributes: {iconName: {fieldName: 'icon'}}},
    {label: 'From', fieldName: 'fromAddress', type: 'text', cellAttributes: { class: {fieldName: 'customClass' } }},
    {label: 'To', fieldName: 'toAddress', type: 'text', cellAttributes: { class: {fieldName: 'customClass' } } },
    // {label: 'Subject', fieldName: 'subject', type: 'text', cellAttributes: { class: {fieldName: 'customClass' } }  },
    {label: 'Subject', fieldName: 'subject', type: 'clickable', typeAttributes: { id: {fieldName: 'id'}, label: {fieldName: 'subject'} }, cellAttributes: { class: {fieldName: 'customClass' } }  },
    {label: 'Related Records', fieldName: 'recordLink', type: 'reference', typeAttributes: { id: {fieldName: 'id'}, link: {fieldName: 'recordLink'}, label: {fieldName: 'recordName'} }, cellAttributes: { class: {fieldName: 'customClass' } }},
    {label: 'Date/Time', fieldName: 'sendingDateTime', initialWidth: 142, type: 'text', cellAttributes: { class: {fieldName: 'customClass' } }},
  ];
}