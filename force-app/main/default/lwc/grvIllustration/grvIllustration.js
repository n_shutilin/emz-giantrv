import { LightningElement, api } from 'lwc';

export default class GrvIllustration extends LightningElement {
  @api name = '';
  @api text = '';

  get isNoContent() {
    return this.name === 'No Content';
  }
}