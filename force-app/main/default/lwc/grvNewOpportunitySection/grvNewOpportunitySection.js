import { LightningElement, api } from 'lwc';

export default class GrvNewOpportunitySection extends LightningElement {
  @api title = '';
  @api showIndicator = false;
  isExpanded = false;

  toggleSection() {
    this.isExpanded = !this.isExpanded;
  }

  @api expand() { this.isExpanded = true; }
  @api collapse() { this.isExpanded = false; }

  get iconClass() {
    return this.isExpanded ? 'transition rotate90' : 'transition rotate0';
  }
}