import { LightningElement, api } from 'lwc';
import TIME_ZONE from '@salesforce/i18n/timeZone';
import LOCALE from '@salesforce/i18n/locale';
import { NavigationMixin } from 'lightning/navigation';


export default class GrvEmailManagementMessage extends NavigationMixin(LightningElement) {
  _tz = { locale: LOCALE, timeZone: TIME_ZONE };
  @api message = {}; 
  @api loading = false;
  sfdcBaseURL;
  
  renderedCallback() {
    this.sfdcBaseURL = window.location.origin;
  }
  
  handleActionClick(event) {
    this.dispatchEvent(new CustomEvent('reply', { detail: event.target.name }))
  }

  get formattedMessageDate() {
    return this.message.MessageDate 
      ? new Date(this.message.MessageDate).toLocaleString(this._tz.locale,{
        timeZone: this._tz.timeZone,
        month:'short', 
        day:'numeric',
        hour: 'numeric', 
        minute: 'numeric', 
        hour12: true
      }) 
      : '';
  }

  get opportunityName() {
    return this.recordName;
  }

  get opportunityUrl() {
    return `${this.sfdcBaseURL}/lightning/r/Opportunity/${this.recordId}/view`;
  }

  get creatorUserLink() {
    return this.message.CreatedBy 
      ? `${this.sfdcBaseURL}/lightning/r/User/${this.message.CreatedBy.Id}/view`
      : '';
  }
}