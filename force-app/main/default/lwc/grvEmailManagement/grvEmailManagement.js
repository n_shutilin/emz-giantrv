import { LightningElement, track } from 'lwc';
import { loadStyle } from 'lightning/platformResourceLoader';
import GrvEmailManagementCSS from '@salesforce/resourceUrl/GrvEmailManagementCSS';
import getMessages from '@salesforce/apex/GrvEmailManagement.getMessages';
import getNumberMessagesPages from '@salesforce/apex/GrvEmailManagement.getNumberMessagesPages';
import deleteMessages from '@salesforce/apex/GrvEmailManagement.deleteMessages';
import getEmailMessageById from '@salesforce/apex/GrvEmailManagement.getEmailMessageById';
import getOpportunityFieldSet from '@salesforce/apex/GrvEmailManagement.getOpportunityFieldSet';
import deleteOpener from '@salesforce/apex/GrvEmailManagement.deleteOpener';

export default class GrvEmailManagement extends LightningElement {
  isLoading = false;
  @track messages = [];
  @track filter = {};
  @track pagination = {
    totalPages: 1,
    currentPage: 1
  };
  @track selectedRecords = [];
  @track openedMessage = {};
  isMessageLoading = false;
  openedOpportunityId = ''; 
  openedOpportunityName = ''; 
  opportunityFields = [];
  showSendEmailPanel = false;
  actionContext = '';
  showEmailTemplateModal = false;

  constructor() {
    super();
    loadStyle(this, GrvEmailManagementCSS);
  }

  connectedCallback() {
    this._getOpportunityFieldSet();
  }
  
  handleRefresh() {
    this._getPagesCount();
    this._getMessages();
  }

  handleFilterChange(event) {
    this.filter = event.detail;
    this.pagination.totalPages = 1;
    this.pagination.currentPage = 1;
    
    this._getPagesCount();
    this._getMessages();
  }

  handleRecordsDelete() {
    this.isLoading = true;
    deleteMessages({messageIds: this.selectedRecords})
      .then(() => { 
        this.handleRefresh();
      })
      .catch((error) => {
        console.log(error);
        this.handleRefresh();
      })
  }

  handlePrevButtonClick() {
    if (this.pagination.currentPage > 1) {
      this.pagination.currentPage--;
      this._getMessages();
    }
  }

  handleRecordSelect(event) {
    event.stopPropagation();
    const recordId = event.detail.value.recId;
    const state = event.detail.value.state;
    if (state) {
      this.selectedRecords.push(recordId);
    } else {
      this.selectedRecords = this.selectedRecords.filter(record => record !== recordId);
    }
  }

  handleRecordClick(event) {
    event.stopPropagation();
    const recordId = event.detail.value.recId;
    if (!recordId) return;
    this._getSingleMessage(recordId);
    this._deleteOpener(recordId);
    this.openedOpportunityId = '';
    this.openedOpportunityName = '';
    const selectedMessage = this.messages.find(msg => msg.id === recordId);
    if (selectedMessage?.isOpportunityLinked) {
      this.openedOpportunityId = selectedMessage.recordId;
      this.openedOpportunityName = selectedMessage.recordName;
    }
  }

  _deleteOpener(recordId) {
    console.log('delete ', recordId)
    deleteOpener({ recId: recordId })
      .then(messageId => {
        console.log('result ', messageId)
        if (messageId) {
          const index = this.messages.findIndex(msg => msg.id === messageId);
          this.messages[index].openedEmail = 'utility:email_open';
          this.messages[index].customClass = '';
          this.messages[index].isOpened = true;
        }
        this.messages = [...this.messages];
      })
      .catch(error => {})
  }
  
  handleNextButtonClick() {
    if (this.pagination.currentPage < this.pagination.totalPages) {
      this.pagination.currentPage++;
      this._getMessages();
    }
  }

  handleMessageAction(event) {
    console.log('aaa ', event.detail);
    this.actionContext = event.detail;
    this.showSendEmailPanel = true;
  }

  handleComposerEvent(event) {
    const action = event.detail.action;
    if (action === 'close') {
      this.showSendEmailPanel = false;
    }
    if (action === 'template') {
      this.showEmailTemplateModal = true;
    }
  }

  handleTemplateSelect(event) {
    console.log('selected temlate id ', event.detail.templateId);
    console.log('selected temlate html ', event.detail.templateHtml);
    const id = event.detail.templateId;
    const htmlValue = event.detail.templateHtml;

    this.template.querySelector('c-grv-email-management-send-email').setTemplate({ id, htmlValue });
    this.showEmailTemplateModal = false;
    console.log('opened message ', this.openedMessage)
  }

  handleEmailTemplateModalClose() {
    this.showEmailTemplateModal = false;
  }

  _getMessages() {
    this.isLoading = true;
    this._clearTempData();
    getMessages({ filter: this.filter, pageNum: this.pagination.currentPage})
      .then((result) => {
        result.forEach(msg => {
          if (msg.isInbound) {
            msg.icon = msg.isOpened ? 'utility:email_open' : 'utility:email';
            msg.customClass = msg.isOpened ? '' : 'row-bold';
          }
        });
        this.messages = result;
        this.isLoading = false;
      })
  }

  _getPagesCount() {
    getNumberMessagesPages({ filter: this.filter }) 
      .then((result) => {
        this.pagination.totalPages = result || 1;
      })
  }

  _getSingleMessage(messageId) {
    this.isMessageLoading = true;
    getEmailMessageById({messageId})
      .then((result) => {
        console.log('res ', result)
        this.openedMessage = result;
        this.isMessageLoading = false;
      })
  }

  _getOpportunityFieldSet() {
    getOpportunityFieldSet()
      .then((result) => {
        console.log('oppp ', result);
        this.opportunityFields = result;
      })
  }

  _clearTempData() {
    this.selectedRecords = [];
  }

  get paginationPrevDisabled() {
    return this.pagination.currentPage === 1;
  }

  get paginationNextDisabled() {
    return this.pagination.currentPage === this.pagination.totalPages;
  }

  get editor() {
    return this.template.querySelector('c-grv-email-management-send-email');
  }
}