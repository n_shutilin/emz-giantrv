trigger GrvEmailMessageTrigger on EmailMessage (after insert) {
  if (Trigger.isAfter) {
    if (Trigger.isInsert) {
      GrvEmailMessageTriggerHelper.insertEmailMessageOpener(Trigger.newMap);
    }
  }
}