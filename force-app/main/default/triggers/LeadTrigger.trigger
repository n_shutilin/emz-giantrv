/**
 * Trigger for the Opportunity SObject type.
 *
 */
trigger LeadTrigger on Lead (After insert) {
    if(Trigger.isAfter && Trigger.isInsert){
        LeadConvertAccountOppCreate.AccountOpportunityCreate(Trigger.new);
    }
}