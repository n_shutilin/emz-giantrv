({
    handleCancel : function(component, event, helper) {
        var homeEvt = $A.get("e.force:navigateToObjectHome");
        homeEvt.setParams({
            "scope": "Opportunity"
        });
        homeEvt.fire();
    },

    handleHeaderChange: function(component, event, helper) {
        component.set('v.title', event.getParam('value'))
    }
})